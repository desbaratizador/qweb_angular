import { isMoment } from 'moment';

export function compareOption(option1: any, option2: any) {
    if (!option1 || !option2) { return false; }

    return option1.id === option2.id;
}

export function buildTimestamp(stringdate): any {
    if (isMoment(stringdate)) { return stringdate; }

    const splittedDateTime = stringdate.split(' '),
        strdate = splittedDateTime[0].split('-'),
        newDate = strdate[1] + '/' + strdate[0] + '/' + strdate[2] + ' ' + splittedDateTime[1];

    return new Date(newDate).getTime();
}
