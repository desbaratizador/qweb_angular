const appConstants = {
    APP_USER: 'app_user',
    DEVICES_TREE: 'devicestree',

    GET_STRUCTURE: 'getStructure',
    GET_STRUCTURE_WEEk_PROCESS: 'getstructureweekprocess',
    NOMINAL_VOLTAGE_MIN_VALUE: 110,
    NODE_TYPE: {
        LOCATION: 1,
        DEVICE: 2,
        CUSTOMER: 3
    },
    DATASOURCE_TYPES: {
        IQ_PLUS: 2 /* id na base de dados do registo IQ+ - tabela DatasourceType */
    },
    NODE_ICON: {
        LOCATION: 'fa-compass',
        DEVICE: 'fa-podcast',
        DEVICE_OUT_OF_SERVICE: 'fa-times'
    },
    CONTEXT_MENU_ICON: {
        DOCUMENTS: 'fa fa-file-text-o',
        SHEET: 'fa fa-id-card-o',
        ADD: 'fa fa-plus',
        REMOVE: 'fa fa-times',
        RENAME: 'fa fa-i-cursor'
    },
    DESCRIPTION_LIST_CONTEXT: {
        BUSBAR_LINE_MEASURE: 'BUSBAR_LINE_MEASURE',
        BUSBAR_TYPE: 'BUSBAR_TYPE',
        EXCEL_EXPORT_TYPE: 'EXCEL_EXPORT_TYPE',
        GESTINC_FAULT_NATURE: 'GESTINC_FAULT_NATURE',
        GESTINC_FAULT_TYPE: 'GESTINC_FAULT_TYPE',
        IGNORE_DATA_APPLIES: 'IGNORE_DATA_APPLIES',
        MAINTENANCE_CAUSE: 'MAINTENANCE_CAUSE',
        MEASURE_LOC_TYPE: 'MEASURE_LOC_TYPE',
        TRANSFORMER_TYPE: 'TRANSFORMER_TYPE',
        REPORT_TYPE: 'REPORT_TYPE',
    },
    WS_CONTROLLER: {
        AGGR_EVENTS_REFDATA: 'aggreventsrefdata',
        ALARMS: 'alarms',
        APPLICATION_CONFIGS: 'appconfigs',
        CALC_OPTIONS: 'calcoptions',
        CALC_OPTIONS_USER: 'calcoptionsforuser',
        DATASOURCES: 'datasources',
        DATASOURCE_TYPES: 'datasourcetypes',
        DESCRIPTION_LIST: 'descriptionlists',
        DEVICE: 'devices',
        DOCUMENTS: 'documents',
        EQUIPMENT: 'equipments',
        IGNORE_DATA_CAUSES: 'ignoredatacauses',
        LIMITS_DEFINITION: 'limitsdefinition',
        LOCATIONS: 'locations',
        PROFILES: 'profiles',
        CUSTOMERS: 'customers',
        CUSTOMER_INTERRUPTIONS: 'customerinterruptions',
        CUSTOMER_DISTURBANCES: 'customerdisturbances',
        TENSION_LEVELS: 'tensionlevels',
        USERS: 'users',
        VARIABLES: 'variables',
        REPORTS: 'reports'
    },
    WS_ACTION: {
        SAVE: 'save'
    },
    ROUTES: {
        NAME: {
            HOME: 'home',
            NEW_DEVICE: 'newDevice',
            DEVICE_SHEET: 'deviceSheet',
            LOCATION_SHEET: 'locationSheet',
            LOGIN: 'login',
            LOGOUT: 'logout',
            DOCUMENTS: 'documents',
            ADMINISTRATION: 'administration',
            ADMINISTRATION_CALC_OPTIONS: 'calcoptions',
            ADMINISTRATION_CALC_OPTIONS_SHEET: 'calcoptionsSheet',
            ADMINISTRATION_CONFIGS: 'administrationconfigs',
            ADMINISTRATION_LIMITS: 'administrationlimits',
            ADMINISTRATION_DATASOURCES: 'administrationdatasources',
            ADMINISTRATION_PROFILES: 'profiles',
            ADMINISTRATION_PROFILE_SHEET: 'profileSheet',
            ADMINISTRATION_REPORT_OPTIONS: 'reportoptions',
            ADMINISTRATION_REPORT_OPTIONS_SHEET: 'reportoptionsSheet',
            MANAGEMENT: 'management',
            ALARMS: 'alarms',
            ALARM_SHEET: 'alarmSheet',
            ALARM_DEFINITION_TYPES: 'alarmDefinitionTypes',
            WEEKLY_PROCESS: 'weeklyprocess',
            USERS: 'users',
            USER_SHEET: 'userSheet',
            LIMIT: 'limit',
            NEW_LIMIT: 'newLimit',
            NEW_CUSTOMER: 'newCustomer',
            CUSTOMER_SHEET: 'customerSheet',
            DOCUMENT_SEARCH: 'documentsearch',
            EVENT_PROCESSING_EXECUTION: 'eventProcessingExecute',
            EVENT_PROCESSING_REPORT: 'eventProcessingReport',
            REPORTS: 'reports'
        },
        ROUTE: {
            DEVICES: 'devices/sheet',
            DEVICE_SHEET: 'devices/sheet/:id',
            LOCATION_SHEET: 'location/sheet/:id',
            LOGIN: 'login',
            LOGOUT: 'logout',
            ADMINISTRATION: 'administration',
            ADMINISTRATION_CALC_OPTIONS: 'administration/calcoptions',
            ADMINISTRATION_CALC_OPTIONS_SHEET: 'administration/calcoptions/:id',
            ADMINISTRATION_CONFIGS: 'administration/configs',
            ADMINISTRATION_DATASOURCES: 'administration/datasources',
            ADMINISTRATION_DATASOURCES_SHEET: 'administration/datasources/:id',
            ADMINISTRATION_LIMITS: 'administration/limits',
            ADMINISTRATION_LIMITS_SHEET: 'administration/limits/:id',
            ADMINISTRATION_PROFILES: 'administration/profiles',
            ADMINISTRATION_PROFILE_SHEET: 'administration/profiles/:id',
            ADMINISTRATION_REPORT_OPTIONS: 'administration/reportoptions',
            ADMINISTRATION_REPORT_OPTIONS_SHEET: 'administration/reportoptions/:id',
            ADMINISTRATION_USERS: 'administration/users',
            ADMINISTRATION_USER_SHEET: 'administration/users/:id',
            NEW_DEVICE: 'device/add/:parenttype/:parentid',
            MANAGEMENT: 'management',
            ALARMS: 'management/alarms',
            ALARM_SHEET: 'management/alarm/:id',
            ALARM_DEFINITION_TYPES: 'management/alarmdefinitiontypes',
            DOCUMENT_SEARCH: 'management/documentsearch',
            EVENT_PROCESSING_EXECUTION: 'management/eventprocessing/execute',
            EVENT_PROCESSING_REPORT: 'management/eventprocessing/report',
            WEEKLY_PROCESS: 'management/weeklyprocess',

            
            LIMIT: 'administration/limit/:id',
            NEW_LIMIT: 'administration/limit/new/:id',

            NEW_CUSTOMER: 'customer/new/:parentid',
            CUSTOMER_SHEET: 'customer/:id',

            REPORTS: 'reports',
            USERS: 'users'
        },
        MODULE_ID: {
            DEVICE_EDIT: './modules/devices/edit',
            LOCATION_EDIT: './modules/locations/edit',
            LOGIN: './modules/auth/login',
            LOGOUT: './modules/auth/logout',
            ADMINISTRATION: './modules/administration/index',
            ADMINISTRATION_CALC_OPTIONS: './modules/administration/calcoptions/index',
            ADMINISTRATION_CALC_OPTIONS_SHEET: './modules/administration/calcoptions/edit',
            ADMINISTRATION_CONFIGS: './modules/administration/settings/settings',
            ADMINISTRATION_DATASOURCES: './modules/administration/datasources/index',
            ADMINISTRATION_LIMITS: './modules/administration/limits/index',
            ADMINISTRATION_USERS: './modules/administration/users/index',
            ADMINISTRATION_USER_SHEET: './modules/administration/users/edit',
            ADMINISTRATION_PROFILES: './modules/administration/profiles/index',
            ADMINISTRATION_PROFILE_SHEET: './modules/administration/profiles/edit',
            ADMINISTRATION_REPORT_OPTIONS: './modules/administration/reportoptions/index',
            ADMINISTRATION_REPORT_OPTIONS_SHEET: './modules/administration/reportoptions/edit',

            MANAGEMENT: './modules/management/index',
            ALARMS: './modules/management/alarms/index',
            ALARM_EDIT: './modules/management/alarms/edit',
            ALARM_DEFINITION_TYPES: './modules/management/alarm-definition-type/index',
            DOCUMENT_SEARCH: './modules/management/documentsearch/index',
            EVENT_PROCESSING_EXECUTION: './modules/management/eventprocessing/execute',
            EVENT_PROCESSING_REPORT: './modules/management/eventprocessing/report',
            WEEKLY_PROCESS: './modules/management/weekly-process/index',

            LIMIT: './modules/administration/limits/edit',

            CUSTOMER: './modules/customer/edit',
            REPORTS: './modules/reports/index',
        }
    },
    TOOLBAR_ITEMS: {
        /*  SAVE: 'save',
         ADD_NEW_RECORD: 'addNewRecord',
         GOTO_LIST: 'gotoList' */
        SAVE: {
            titleTranslationKey: 'save',
            //   messageType: messageType.TOOLBAR.SAVE,
            icon: 'fa-floppy-o'
        },
        ADD_NEW_RECORD: {
            titleTranslationKey: 'addNewRecord',
            // messageType: messageType.TOOLBAR.ADD_NEW,
            icon: 'fa-file-o'
        },
        GOTO_LIST: {
            titleTranslationKey: 'grid',
            //   messageType: messageType.TOOLBAR.GOTO_LIST,
            icon: 'fa-list-ol'
        },
        EXECUTE: {
            titleTranslationKey: 'execute',
            //   messageType: messageType.TOOLBAR.EXECUTE,
            icon: 'fa-bolt'
        },
        REPORT_REQUEST: {
            titleTranslationKey: 'process',
            //   messageType: messageType.REPORTS.PROCESS,
            icon: 'fa-file-text-o'
        },
        REPORT_LIST: {
            titleTranslationKey: 'reportListLabel',
            //   messageType: messageType.REPORTS.LIST,
            icon: 'fa-files-o'
        }
    },
    REPORT_TYPES: {
        BASE_REPORTS: 'BASE_REPORTS',
        ERSE_REPORTS: 'ERSE_REPORTS',
        ASSETS_REPORTS: 'ASSETS_REPORTS',
        EVENT_CONS_REPORTS: 'EVENT_CONS_REPORTS'
    }
};
export default appConstants;
