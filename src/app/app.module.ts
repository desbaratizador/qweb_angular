import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TreeModule } from 'angular-tree-component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { FlexLayoutModule } from '@angular/flex-layout';
import localePT from '@angular/common/locales/pt-PT';
import { TextMaskModule } from 'angular2-text-mask';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TreeService } from './services/tree.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BaseService } from './services/base.service';

import { TreeComponent } from './components/tree/tree.component';
/* import {TiGridModule} from '@ng-plus/grid'; */
import { AdministrationCalcOptionsModule } from './modules/administration/calc-options/administration-calc-options.module';
import { AdministrationCalcOptionsForUserModule } from './modules/administration/calc-options-for-user/administration-calc-options-for-user.module';
import { AdministrationDataSourcesModule } from './modules/administration/data-sources/administration-data-sources.module';
import { AdministrationLimitsModule } from './modules/administration/limits/administration-limits.module';
import { AdministrationProfilesModule } from './modules/administration/profiles/administration-profiles.module';
import { AdministrationUsersModule } from './modules/administration/users/administration-users.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppMaterialModule } from './material.module';
import { UserService } from './services/user.service';
import { registerLocaleData } from '@angular/common';
import { AppTranslationService } from './services/app-translation.service';
import { LOCALE_ID } from '@angular/core';
import { SafePipe } from './pipes/safe.pipe';
import { ProfileService } from './services/profile.service';
import { DeviceService } from './services/device.service';
import { LocationService } from './services/location.service';

import { DeviceModule } from './modules/devices/device.module';
import { DescriptionListService } from './services/description-list.service';
import { DeviceConfigModalComponent } from './modules/devices/device-config-modal/device-config-modal.component';
import { MatDatepickerIntl } from '@angular/material';
import { DateTimeEditorComponent } from './components/date/date-time-editor/date-time-editor.component';
import { TimeEditorComponent } from './components/date/date-time-editor/time-editor/time-editor.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DescriptionLists } from './modules/reference-data/description-lists';
import { DatasourceIdComponent } from './components/datasource-id/datasource-id.component';
import { DatasourceIdModalComponent } from './components/datasource-id/datasource-id-modal/datasource-id-modal.component';
import { DeviceMaintenanceModalComponent } from './modules/devices/device-maintenance/device-maintenance-modal/device-maintenance-modal.component';
import { DeviceIgnoreDataModalComponent } from './modules/devices/device-ignore-data/device-ignore-data-modal/device-ignore-data-modal.component';
import { DeviceIgnoreEventsModalComponent } from './modules/devices/device-ignore-events/device-ignore-events-modal/device-ignore-events-modal.component';
import { AppComponentsModule } from './components/app-components.module';
import { LoginComponent } from './modules/login/login.component';
import { HomeComponent } from './modules/home/home.component';
import { AdministrationComponent } from './modules/administration/administration/administration.component';
import { ManagementComponent } from './modules/management/management/management.component';
import { UsersMenuComponent } from './modules/users/users-menu/users-menu.component';
import { AppReportService } from './services/app-report.service';
import { ReportComponent } from './modules/reports/report/report.component';

import { ReportService } from './services/report.service';
import { ReportModule } from './modules/reports/report.module';






registerLocaleData(localePT);

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    TreeComponent,
    DeviceConfigModalComponent,
    DatasourceIdModalComponent,
    DateTimeEditorComponent,
    TimeEditorComponent,
    SafePipe,
    DatasourceIdComponent,
    DeviceMaintenanceModalComponent,
    DeviceIgnoreDataModalComponent,
    DeviceIgnoreEventsModalComponent,
    LoginComponent,
    HomeComponent,
    AdministrationComponent,
    ManagementComponent,
    UsersMenuComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot(),
    AppRoutingModule,
    TreeModule.forRoot(),
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    AdministrationCalcOptionsModule,
    AdministrationCalcOptionsForUserModule,
    AdministrationDataSourcesModule,
    AdministrationLimitsModule,
    AdministrationProfilesModule,
    AdministrationUsersModule,
    AppMaterialModule,
    DeviceModule,
    TextMaskModule,
    MatMomentDateModule,
    AppComponentsModule,
    ReportModule
  ],
  providers: [
    BaseService,
    TreeService,
    UserService,
    ProfileService,
    {
      provide: LOCALE_ID,
      useValue: 'pt'
    },
    AppTranslationService,
    DeviceService,
    LocationService,
    //    DescriptionLists,
    DescriptionListService,

    MatDatepickerIntl,
    AppReportService,
    ReportService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
