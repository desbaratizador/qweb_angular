import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { Observable } from 'rxjs';
import { IgnoreDataCause } from '../models/ignore-data-cause';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IgnoreDataCauseService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.IGNORE_DATA_CAUSES;
  }

  getIgnoreDataCauses(): Observable<IgnoreDataCause[]> {
    return this.getAll().pipe(map((v: any[]) => v.map(_ => new IgnoreDataCause(_[1]))));
  }
}
