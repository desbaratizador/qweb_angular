import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import appConstants from '../code/app-constants';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Variable } from '../models/variable';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VariableService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.VARIABLES;
  }

  getVariables(): Observable<Variable[]> {
    return this.getAll().pipe(map((v: any[]) => v.map(_ => new Variable(_[1]))));
  }
}
