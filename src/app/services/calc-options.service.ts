import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CalcOptions } from '../models/calc-options';


@Injectable({
  providedIn: 'root'
})
export class CalcOptionsService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.CALC_OPTIONS;
  }

  getList(): Observable<CalcOptions[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((u: any[]) => u.map(_ => new CalcOptions(_[1]))
      ));
  }

  get(id: string): Observable<CalcOptions> {
    return this.getData(this.getFullUrl(id)).pipe(map((calcOptions: CalcOptions) => new CalcOptions(calcOptions)));
  }

  getGlobal(): Observable<CalcOptions[]> {
    return this.getData(this.getFullUrl('global')).pipe(
      map((c: any) => c.map((_: any) => new CalcOptions(_[1])))
    );
  }
}
