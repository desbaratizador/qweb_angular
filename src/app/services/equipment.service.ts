import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { Observable } from 'rxjs';
import { Equipment } from '../models/equipment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EquipmentService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.EQUIPMENT;
  }

  getList(): Observable<Equipment[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((u: any[]) => u.map(_ => new Equipment(_[1]))
      ));
  }
}
