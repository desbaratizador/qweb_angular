import { TestBed } from '@angular/core/testing';

import { CalcOptionsForUserService } from './calc-options-for-user.service';

describe('CalcOptionsForUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalcOptionsForUserService = TestBed.get(CalcOptionsForUserService);
    expect(service).toBeTruthy();
  });
});
