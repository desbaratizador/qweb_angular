import { TestBed } from '@angular/core/testing';

import { IgnoreDataCauseService } from './ignore-data-cause.service';

describe('IgnoreDataCauseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IgnoreDataCauseService = TestBed.get(IgnoreDataCauseService);
    expect(service).toBeTruthy();
  });
});
