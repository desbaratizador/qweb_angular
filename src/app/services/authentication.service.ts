import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post('http://localhost:9090/qwebrest/api/users/login', {
      login: username,
      pwd: password
    }, httpOptions).pipe(map((authResult: any) => {
      if (authResult) {
        delete authResult.user.pwd;
        localStorage.setItem('currentUser', JSON.stringify(authResult.user));
        localStorage.setItem('access_token', authResult.access_token);
        this.currentUserSubject.next(authResult.user);
      }
      return authResult;
    }), catchError((error: any, caught: any) => {
      console.log(error);
      console.log(caught);
      return throwError(error);
    }));
  }
  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('access_token');
    this.currentUserSubject.next(null);
  }

}
