import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

import appConstants from '../code/app-constants';
import { HttpClient } from '@angular/common/http';
import { Report } from '../models/report';
import { Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DescriptionListService } from './description-list.service';
import { DescriptionList } from '../models/description-list';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends BaseService {
  private _erseReports: Report[] = [];
  private _baseReports: Report[] = [];
  private _assetReports: Report[] = [];
  private _eventConsReports: Report[] = [];

  private bsErseReports: BehaviorSubject<Report[]> = new BehaviorSubject<Report[]>(this._erseReports);
  private bsBaseReports: BehaviorSubject<Report[]> = new BehaviorSubject<Report[]>(this._baseReports);
  private bsAssetReports: BehaviorSubject<Report[]> = new BehaviorSubject<Report[]>(this._assetReports);
  private bsEventConsReports: BehaviorSubject<Report[]> = new BehaviorSubject<Report[]>(this._eventConsReports);

  constructor(http: HttpClient, private descriptionListService: DescriptionListService) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.REPORTS;
    const _this = this;
    forkJoin([this.getList(), descriptionListService.getReportTypes()]).subscribe((res: any) => {
      const reports = res[0],
        dlReports = res[1];

      _this._erseReports = this.filterReports(reports, dlReports, appConstants.REPORT_TYPES.ERSE_REPORTS);
      _this.bsErseReports.next(_this._erseReports);

      _this._baseReports = this.filterReports(reports, dlReports, appConstants.REPORT_TYPES.BASE_REPORTS);
      _this.bsBaseReports.next(_this._baseReports);

      _this._assetReports = this.filterReports(reports, dlReports, appConstants.REPORT_TYPES.ASSETS_REPORTS);
      _this.bsAssetReports.next(_this._assetReports);

      _this._eventConsReports = this.filterReports(reports, dlReports, appConstants.REPORT_TYPES.EVENT_CONS_REPORTS);
      _this.bsEventConsReports.next(_this._eventConsReports);

    }, (error: any) => {
      console.log(error);
    });
  }

  private filterReports(reports: Report[], dlReports: DescriptionList[], reportType: string) {
    const dlId: any = dlReports.find((_: DescriptionList) => _.code === reportType).id;

    return reports.filter((r: Report) => r.reportType.id === dlId);
  }

  private getList(): Observable<Report[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((res: any[]) => res.map(_ => new Report(_[1]))
      ));
  }

  public get erseReports(): Observable<Report[]> {
    return this.bsErseReports.asObservable();
  }

  public get baseReports(): Observable<Report[]> {
    return this.bsBaseReports.asObservable();
  }

  public get assetReports(): Observable<Report[]> {
    return this.bsAssetReports.asObservable();
  }

  public get eventConsReports(): Observable<Report[]> {
    return this.bsEventConsReports.asObservable();
  }

  public generateReports(reportRequest: any) {
    return this.postData(this.getFullUrl('generatereports'), reportRequest).pipe(map((resp: any) => {
      return JSON.parse(resp);
    }));
  }
}
