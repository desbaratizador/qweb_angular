import { Injectable, EventEmitter } from '@angular/core';
import { Device } from '../models/device';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class AppReportService {
    private _selectedDevices: Device[] = [];
    private bsSelectedDevices: BehaviorSubject<Device[]> = new BehaviorSubject<Device[]>(this._selectedDevices);
    public onDeviceRemoved: EventEmitter<any> = new EventEmitter<any>();
    public get selectedDevices(): Observable<Device[]> {
        return this.bsSelectedDevices.asObservable();
    }

    public addDevice(device: Device) {
        this._selectedDevices.push(device);
        this.bsSelectedDevices.next(this._selectedDevices);
    }

    public removeDevice(device: Device, source: any) {
        const idx = this._selectedDevices.indexOf(device);
        if (idx >= 0) {
            this._selectedDevices.splice(idx, 1);
            this.bsSelectedDevices.next(this._selectedDevices);
        }
        if (source !== 'tree') {
            this.onDeviceRemoved.emit({ device: device });
        }
    }
    constructor() { }
}
