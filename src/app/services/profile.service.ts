import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { map } from 'rxjs/operators';
import { Profile } from '../models/profile';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProfileService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.PROFILES;
  }

  getList(): Observable<Profile[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((u: any[]) => u.map(_ => new Profile(_[1])))
    );
  }

  get(id): Observable<Profile> {
    return this.getData(this.getFullUrl(id)).pipe(map((u: Profile) => new Profile(u)));
  }
}
