import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.USERS;
  }

  getList(): Observable<User[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((u: any[]) => u.map(_ => new User(_[1]))
      ));
  }

  get(id): Observable<User> {
    return this.getData(this.getFullUrl(id)).pipe(map((u: User) => new User(u)));
  }

  /* save(user: User) {
    return this._http.post(this.getFullUrl(), user.toJSON(), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  } */
}
