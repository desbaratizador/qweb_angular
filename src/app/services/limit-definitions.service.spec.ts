import { TestBed } from '@angular/core/testing';

import { LimitsDefinitionService } from './limits-definition.service';

describe('LimitDefinitionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LimitsDefinitionService = TestBed.get(LimitsDefinitionService);
    expect(service).toBeTruthy();
  });
});
