import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { Observable } from 'rxjs';
import { Location } from '../models/location';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class LocationService extends BaseService {
    constructor(http: HttpClient) {
        super(http);
        this.wsController = appConstants.WS_CONTROLLER.LOCATIONS;
    }

    getList(): Observable<Location[]> {
        return this.getData(this.getFullUrl()).pipe(
            map((l: any[]) => l.map(_ => new Location(_[1]))
            ));
    }

    get(id): Observable<Location> {
        return this.getData(this.getFullUrl(id)).pipe(map((l: Location) => new Location(l)));
    }
}
