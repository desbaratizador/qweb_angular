import { TestBed } from '@angular/core/testing';

import { AggrEventsRefDataService } from './aggr-events-refdata.service';

describe('AggrEventsRefDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AggrEventsRefDataService = TestBed.get(AggrEventsRefDataService);
    expect(service).toBeTruthy();
  });
});
