import { TestBed } from '@angular/core/testing';

import { CalcOptionsService } from './calc-options.service';

describe('CalcOptionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalcOptionsService = TestBed.get(CalcOptionsService);
    expect(service).toBeTruthy();
  });
});