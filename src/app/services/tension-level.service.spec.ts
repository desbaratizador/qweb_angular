import { TestBed } from '@angular/core/testing';

import { TensionLevelService } from './tension-level.service';

describe('TensionLevelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TensionLevelService = TestBed.get(TensionLevelService);
    expect(service).toBeTruthy();
  });
});
