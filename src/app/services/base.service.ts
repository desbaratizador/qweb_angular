import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModelInterface } from '../models/model.interface';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  private baseUrl = 'http://localhost:9090/qwebrest/api' + '/';
  protected wsController = '';
  protected _http: HttpClient;

  constructor(http: HttpClient) {
    this._http = http;
  }

  protected getAll(): any {
    return this.getData(this.getFullUrl());
  }

  protected getData(url: string): any {
    return this._http.get(url);
  }

  protected postData(url, params, httpOptions?) {
    const toPost = JSON.parse(JSON.stringify(params));
    return this._http.post(url, toPost, httpOptions);
  }

  protected getFullUrl(partialUrl?: string) {

    return partialUrl ? `${this.baseUrl}${this.wsController}/${partialUrl}` : `${this.baseUrl}${this.wsController}`;
  }

  public save(imodel: ModelInterface) {
    return this._http.post(this.getFullUrl(), imodel.toJSON(), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }
}
