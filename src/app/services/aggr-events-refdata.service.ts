import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AggrEventMethod } from '../models/aggr-event-method';
import { Description } from '../models/description';

@Injectable({
    providedIn: 'root'
})
export class AggrEventsRefDataService extends BaseService {

    constructor(http: HttpClient) {
        super(http);
        this.wsController = appConstants.WS_CONTROLLER.AGGR_EVENTS_REFDATA;
    }

    getMacroMethods(): Observable<Description[]> {
        return this.getData(this.getFullUrl('macromethods')).pipe(map((c: any) => c.map((_: any) => new Description(_[1]))));
    }

    getMethods(): Observable<AggrEventMethod[]> {
        return this.getData(this.getFullUrl('methods')).pipe(map((c: any) => c.map((_: any) => new AggrEventMethod(_[1]))));
    }

    getDurations(): Observable<Description[]> {
        return this.getData(this.getFullUrl('durations')).pipe(map((c: any) => c.map((_: any) => new Description(_[1]))));
    }

    getProfounds(): Observable<Description[]> {
        return this.getData(this.getFullUrl('profounds')).pipe(map((c: any) => c.map((_: any) => new Description(_[1]))));
    }
}
