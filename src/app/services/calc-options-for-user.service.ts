import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { map } from 'rxjs/operators';
import { CalcOptionsForUser } from '../models/calc-options-for-user';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CalcOptionsForUserService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.CALC_OPTIONS_USER;
  }

  getList(): Observable<CalcOptionsForUser[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((u: any[]) => u.map(_ => new CalcOptionsForUser(_[1]))
      ));
  }

  get(id): Observable<CalcOptionsForUser> {
    return this.getData(this.getFullUrl(id)).pipe(map((u: CalcOptionsForUser) => new CalcOptionsForUser(u)));
  }

  /* save(user: CalcOptionsForUser) {
    return this._http.post(this.getFullUrl(), user.toJSON(), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  } */
}
