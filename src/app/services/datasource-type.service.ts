import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import appConstants from '../code/app-constants';
import { DataSourceType } from '../models/data-source-type';

@Injectable({
    providedIn: 'root'
})

export class DatasourceTypeService extends BaseService {

    constructor(http: HttpClient) {
        super(http);
        this.wsController = appConstants.WS_CONTROLLER.DATASOURCE_TYPES;
    }

    getList(): Observable<DataSourceType[]> {
        return this.getData(this.getFullUrl()).pipe(map((u: any[]) => u.map(_ => new DataSourceType(_[1]))));
    }
    
    get(id): Observable<DataSourceType> {
        return this.getData(this.getFullUrl(id)).pipe(map((u: DataSourceType) => new DataSourceType(u)));
    }
}
