import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import appConstants from '../code/app-constants';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Description } from '../models/description';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TensionLevelService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.TENSION_LEVELS;
  }

  getList(): Observable<Description[]> {
    return this.getAll().pipe(map((v: any[]) => v.map(_ => new Description(_[1]))));
  }
}
