import { TestBed } from '@angular/core/testing';

import { DescriptionListService } from './description-list.service';

describe('DescriptionListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DescriptionListService = TestBed.get(DescriptionListService);
    expect(service).toBeTruthy();
  });
});
