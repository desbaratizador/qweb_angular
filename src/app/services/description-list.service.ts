import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { tap, filter, map, switchMap } from 'rxjs/operators';
import { DescriptionList } from '../models/description-list';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DescriptionListService extends BaseService {
  private contexts = {
    REPORT_TYPE: 'REPORT_TYPE',
    BUSBAR_LINE_MEASURE: 'BUSBAR_LINE_MEASURE',
    MAINTENANCE_CAUSE: 'MAINTENANCE_CAUSE',
    MEASURE_LOC_TYPE: 'MEASURE_LOC_TYPE',
    BUSBAR_TYPE: 'BUSBAR_TYPE',
    TRANSFORMER_TYPE: 'TRANSFORMER_TYPE',
    EXCEL_EXPORT_TYPE: 'EXCEL_EXPORT_TYPE',
    IGNORE_DATA_APPLIES: 'IGNORE_DATA_APPLIES'
  };

  private _descriptionList: any[] = [];
  private bsDescriptionList: BehaviorSubject<DescriptionList[]> = new BehaviorSubject<DescriptionList[]>(this._descriptionList);

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.DESCRIPTION_LIST;

  }

  getDataFromServer(): Observable<DescriptionList[]> {
    return this.getAll().pipe(map((res: any[]) => res.map((dl: any) => new DescriptionList(dl[1]))));
  }

  getMaintenanceCauses(): Observable<DescriptionList[]> {
    return this.getDataFromContext(this.contexts.MAINTENANCE_CAUSE);
  }

  getReportTypes(): Observable<DescriptionList[]> {
    return this.getDataFromContext(this.contexts.REPORT_TYPE);
  }

  private getDataFromContext(context: string): Observable<DescriptionList[]> {
    return this.getData(this.getFullUrl(`context/${context}`)).pipe(map((res: any[]) => res.map((dl: any) => new DescriptionList(dl[1]))));
  }
}
