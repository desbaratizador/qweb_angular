import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { Observable } from 'rxjs';
import { LimitsDefinition } from '../models/limits-definition';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LimitsDefinitionService extends BaseService {

  constructor(http: HttpClient) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.LIMITS_DEFINITION;
  }

  getList(): Observable<LimitsDefinition[]> {
    return this.getData(this.getFullUrl()).pipe(
      map((u: any[]) => u.map(_ => new LimitsDefinition(_[1]))
      ));
  }

  get(limitsDefintionId: any): Observable<LimitsDefinition> {
    return this.getData(this.getFullUrl(limitsDefintionId)).pipe(map((u: LimitsDefinition) => new LimitsDefinition(u)));
  }

  getGlobal(): Observable<LimitsDefinition[]> {
    return this.getData(this.getFullUrl('global')).pipe(
      map((c: any) => c.map((_: any) => new LimitsDefinition(_[1])))
    );
  }
}
