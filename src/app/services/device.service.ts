import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Device } from '../models/device';
import { AppTranslationService } from './app-translation.service';

@Injectable({
  providedIn: 'root'
})
export class DeviceService extends BaseService {

  constructor(http: HttpClient, private appTranslate: AppTranslationService) {
    super(http);
    this.wsController = appConstants.WS_CONTROLLER.DEVICE;
  }

  get(id): any {
    if (parseInt(id, 10) <= 0) {
      return this.appTranslate.translateMultiple([
        'newInstalation'
      ]).pipe(map((t: any) =>
        new Device({ name: `${t.newInstalation}` })
      ));

    } else {
      return this.getData(this.getFullUrl(id))
        .pipe(map((d: Device) => new Device(d)));
    }
  }
}
