import { TestBed } from '@angular/core/testing';

import { DatasourceTypeService } from './datasource-type.service';

describe('DatasourceTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatasourceTypeService = TestBed.get(DatasourceTypeService);
    expect(service).toBeTruthy();
  });
});
