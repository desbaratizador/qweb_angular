import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import appConstants from '../code/app-constants';
import { DataSource } from '../models/data-source';
import { Device } from '../models/device';
import { DataSourceType } from '../models/data-source-type';

@Injectable({
    providedIn: 'root'
})
export class DatasourceService extends BaseService {

    constructor(http: HttpClient) {
        super(http);
        this.wsController = appConstants.WS_CONTROLLER.DATASOURCES;
    }

    getList(): Observable<DataSource[]> {
        return this.getData(this.getFullUrl()).pipe(map((u: any[]) => u.map(_ => new DataSource(_[1]))));
    }
    
    get(id): Observable<DataSource> {
        return this.getData(this.getFullUrl(id)).pipe(map((u: DataSource) => new DataSource(u)));
    }

    findDevice(criteria: string, datasource: DataSource): Observable<Device[]> {
        return this.postData(this.getFullUrl('devices'), { dataSource: datasource, flag: criteria }).pipe(map((d: any) => d.map(_ => new Device(_[1]))));
    }

    test(datasource: DataSource) {
        this.postData(this.getFullUrl('test'), { dataSource: datasource});
    }
}
