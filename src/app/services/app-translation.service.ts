import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AppTranslationService {

  constructor(private translateService: TranslateService) {
    translateService.setDefaultLang('pt');
    translateService.use('pt');
  }

  translateMultiple(toTranslate: string[]): Observable<any> {
    const toTranslationService = toTranslate.map((s: string) => this.translateService.get(s));
    return forkJoin(toTranslationService).pipe(map((res: any) => {
      const toReturn = {};
      for (let i = 0, ii = toTranslate.length; i < ii; i++) {
        toReturn[toTranslate[i]] = res[i];
      }
      return toReturn;
    }));
  }

  translate(toTranslate: string) {
    return this.translateService.get(toTranslate);
  }
}
