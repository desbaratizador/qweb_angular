import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import appConstants from '../code/app-constants';
import { Document } from '../models/document';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class DocumentService extends BaseService {

    constructor(http: HttpClient) {
        super(http);
        this.wsController = appConstants.WS_CONTROLLER.DOCUMENTS;
    }

    search(searchParams: any): Observable<Document[]> {
        return this.postData(this.getFullUrl(), searchParams).pipe(map((d: any) => d.map(_ => new Document(_[1]))));
    }
}
