import { TestBed } from '@angular/core/testing';

import { AppReportService } from './app-report.service';

describe('AppReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppReportService = TestBed.get(AppReportService);
    expect(service).toBeTruthy();
  });
});
