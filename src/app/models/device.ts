import { BaseModel } from './base-model';
import { DescriptionList } from './description-list';
import { CalcOptions } from './calc-options';
import { LimitsDefinition } from './limits-definition';
import { DeviceConfig } from './device-config';
import { TreeComponentProperty } from './tree-component-property';
import { DeviceIgnoreData } from './device-ignore-data';
import { DeviceIgnoreEvent } from './device-ignore-event';
import { DeviceMaintenance } from './device-maintenance';
import { Customer } from './customer';
import { ModelInterface } from './model.interface';

export class Device extends BaseModel implements ModelInterface {
    private _locationId?: number;
    private _parentDeviceId?: number;
    private _code: string;
    private _name: string;
    private _description: string;
    private _nominalVoltage: number;
    private _measurePointDescription: string;
    private _deliveryPoint: string;
    private _weekProcess: boolean;
    private _weekProcessAutomatic: boolean;
    private _eventProcess: boolean;

    private _deviceZoneA: string;
    private _deviceZoneB: string;
    private _deviceZoneC: string;
    private _drc: string;
    private _ao: string;
    private _injector: string;
    private _substation: string;
    private _barring: string;
    private _output: string;
    private _county: string;
    private _instalation: string;
    private _district: string;
    private _customerMeasured: boolean;
    private _busbarType: DescriptionList;
    private _busbarLineMeasurement: DescriptionList;
    private _faultLevel: number;
    private _transformerTypeUpstream: DescriptionList;
    private _transformerTypeDownstream: DescriptionList;
    private _measureLocationType: DescriptionList;
    private _latitude: number;
    private _longitude: number;
    private _equipmentLocation: string;
    private _outOfServiceAux: boolean;

    private _childDevices: any;
    private _configs: any;
    private _documents: any;
    private _treeComponentProperties: any;
    private _ignoreData: any;
    private _ignoreEvent: any;
    private _maintenances: any;
    private _customers: any;
    private _forcedCalcOptions: CalcOptions;
    private _forcedLimitsDefinition: LimitsDefinition;

    constructor(protected obj?: any) {
        super(obj);
        this._outOfServiceAux = false;
    }

    get locationId(): number {
        return this._locationId ? this._locationId : (this._locationId = this.obj ? this.obj.locationId : null);
    }
    set locationId(value: number) {
        this._locationId = value;
    }

    get parentDeviceId(): number {
        return this._parentDeviceId ? this._parentDeviceId : (this._parentDeviceId = this.obj ? this.obj._parentDeviceId : null);
    }
    set parentDeviceId(value: number) {
        this._parentDeviceId = value;
    }

    get code(): string {
        return this._code ? this._code : (this._code = this.obj ? this.obj.code : '');
    }
    set code(value: string) {
        this._code = value;
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj ? this.obj.name : '');
    }
    set name(value: string) {
        this._name = value;
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj ? this.obj.description : '');
    }
    set description(value: string) {
        this._description = value;
    }

    get nominalVoltage(): number {
        return this._nominalVoltage ? this._nominalVoltage : (this._nominalVoltage = this.obj ? this.obj.nominalVoltage : 0);
    }
    set nominalVoltage(value: number) {
        this._nominalVoltage = value;
    }

    get measurePointDescription(): string {
        return this._measurePointDescription ? this._measurePointDescription : (this._measurePointDescription = this.obj ? this.obj.measurePointDescription : '');
    }
    set measurePointDescription(value: string) {
        this._measurePointDescription = value;
    }

    get deliveryPoint(): string {
        return this._deliveryPoint ? this._deliveryPoint : (this._deliveryPoint = this.obj ? this.obj.deliveryPoint : '');
    }
    set deliveryPoint(value: string) {
        this._deliveryPoint = value;
    }

    get weekProcess(): boolean {
        return this._weekProcess ? this._weekProcess : (this._weekProcess = this.obj ? this.obj.weekProcess : false);
    }
    set weekProcess(value: boolean) {
        this._weekProcess = value;
    }

    get weekProcessAutomatic(): boolean {
        return this._weekProcessAutomatic ? this._weekProcessAutomatic : (this._weekProcessAutomatic = this.obj ? this.obj.weekProcessAutomatic : false);
    }
    set weekProcessAutomatic(value: boolean) {
        this._weekProcessAutomatic = value;
    }

    get eventProcess(): boolean {
        return this._eventProcess ? this._eventProcess : (this._eventProcess = this.obj ? this.obj.eventProcess : false);
    }
    set eventProcess(value: boolean) {
        this._eventProcess = value;
    }

    get deviceZoneA(): string {
        return this._deviceZoneA ? this._deviceZoneA : (this._deviceZoneA = this.obj ? this.obj.deviceZoneA : '');
    }
    set deviceZoneA(value: string) {
        this._deviceZoneA = value;
    }

    get deviceZoneB(): string {
        return this._deviceZoneB ? this._deviceZoneB : (this._deviceZoneB = this.obj ? this.obj.deviceZoneB : '');
    }
    set deviceZoneB(value: string) {
        this._deviceZoneB = value;
    }

    get deviceZoneC(): string {
        return this._deviceZoneC ? this._deviceZoneC : (this._deviceZoneC = this.obj ? this.obj.deviceZoneC : '');
    }
    set deviceZoneC(value: string) {
        this._deviceZoneC = value;
    }

    get drc(): string {
        return this._drc ? this._drc : (this._drc = this.obj ? this.obj.drc : '');
    }
    set drc(value: string) {
        this._drc = value;
    }

    get ao(): string {
        return this._ao ? this._ao : (this._ao = this.obj ? this.obj.ao : '');
    }
    set ao(value: string) {
        this._ao = value;
    }

    get substation(): string {
        return this._substation ? this._substation : (this._substation = this.obj ? this.obj.substation : '');
    }
    set substation(value: string) {
        this._substation = value;
    }

    get injector(): string {
        return this._injector ? this._injector : (this._injector = this.obj ? this.obj.injector : '');
    }
    set injector(value: string) {
        this._injector = value;
    }
    get barring(): string {
        return this._barring ? this._barring : (this._barring = this.obj ? this.obj.barring : '');
    }
    set barring(value: string) {
        this._barring = value;
    }

    get output(): string {
        return this._output ? this._output : (this._output = this.obj ? this.obj.output : '');
    }
    set output(value: string) {
        this._output = value;
    }

    get county(): string {
        return this._county ? this._county : (this._county = this.obj ? this.obj.county : '');
    }
    set county(value: string) {
        this._county = value;
    }

    get instalation(): string {
        return this._instalation ? this._instalation : (this._instalation = this.obj ? this.obj.instalation : '');
    }
    set instalation(value: string) {
        this._instalation = value;
    }

    get district(): string {
        return this._district ? this._district : (this._district = this.obj ? this.obj.district : '');
    }
    set district(value: string) {
        this._district = value;
    }

    get customerMeasured(): boolean {
        return this._customerMeasured ? this._customerMeasured : (this._customerMeasured = this.obj ? this.obj.customerMeasured : false);
    }
    set customerMeasured(value: boolean) {
        this._customerMeasured = value;
    }

    get busbarType(): DescriptionList {
        return this._busbarType ? this._busbarType : (this._busbarType = this.obj ? this.obj.busbarType : 0);
    }
    set busbarType(value: DescriptionList) {
        this._busbarType = value;
    }

    get busbarLineMeasurement(): DescriptionList {
        return this._busbarLineMeasurement ? this._busbarLineMeasurement : (this._busbarLineMeasurement = this.obj ? this.obj.busbarLineMeasurement : 0);
    }
    set busbarLineMeasurement(value: DescriptionList) {
        this._busbarLineMeasurement = value;
    }

    get faultLevel(): number {
        return this._faultLevel ? this._faultLevel : (this._faultLevel = this.obj ? this.obj.faultLevel : 0);
    }
    set faultLevel(value: number) {
        this._faultLevel = value;
    }

    get transformerTypeUpstream(): DescriptionList {
        return this._transformerTypeUpstream ? this._transformerTypeUpstream : (this._transformerTypeUpstream = this.obj ? this.obj.transformerTypeUpstream : 0);
    }
    set transformerTypeUpstream(value: DescriptionList) {
        this._transformerTypeUpstream = value;
    }

    get transformerTypeDownstream(): DescriptionList {
        return this._transformerTypeDownstream ? this._transformerTypeDownstream : (this._transformerTypeDownstream = this.obj ? this.obj.transformerTypeDownstream : 0);
    }
    set transformerTypeDownstream(value: DescriptionList) {
        this._transformerTypeDownstream = value;
    }

    get measureLocationType(): DescriptionList {
        return this._measureLocationType ? this._measureLocationType : (this._measureLocationType = this.obj ? this.obj.measureLocationType : 0);
    }
    set measureLocationType(value: DescriptionList) {
        this._measureLocationType = value;
    }

    get latitude(): number {
        return this._latitude ? this._latitude : (this._latitude = this.obj ? this.obj.latitude : 0);
    }
    set latitude(value: number) {
        this._latitude = value;
    }

    get longitude(): number {
        return this._longitude ? this._longitude : (this._longitude = this.obj ? this.obj.longitude : 0);
    }
    set longitude(value: number) {
        this._longitude = value;
    }

    get equipmentLocation(): string {
        return this._equipmentLocation ? this._equipmentLocation : (this._equipmentLocation = this.obj ? this.obj.equipmentLocation : '');
    }
    set equipmentLocation(value: string) {
        this._equipmentLocation = value;
    }

    get outOfServiceAux(): boolean {
        return this._outOfServiceAux ? this._outOfServiceAux : (this._outOfServiceAux = this.obj ? this.obj.outOfServiceAux : false);
    }
    set outOfServiceAux(value: boolean) {
        this._outOfServiceAux = value;
    }

    get childDevices(): any {
        return this._childDevices ? this._childDevices : (this._childDevices = this.obj ? this.obj.childDevices : []);
    }
    set childDevices(value: any) {
        this._childDevices = value;
    }

    get configs(): any {
        return this._configs ? this._configs : (this._configs = this.obj ? this.obj.configs : []);
    }
    set configs(value: any) {
        this._configs = value;
    }

    get documents(): any {
        return this._documents ? this._documents : (this._documents = this.obj ? this.obj.documents : []);
    }
    set documents(value: any) {
        this._documents = value;
    }

    get treeComponentProperties(): any {
        return this._treeComponentProperties ? this._treeComponentProperties : (this._treeComponentProperties = this.obj ? this.obj.treeComponentProperties : []);
    }
    set treeComponentProperties(value: any) {
        this._treeComponentProperties = value;
    }

    get ignoreData(): any {
        return this._ignoreData ? this._ignoreData : (this._ignoreData = this.obj ? this.obj.ignoreData : []);
    }
    set ignoreData(value: any) {
        this._ignoreData = value;
    }

    get ignoreEvent(): any {
        return this._ignoreEvent ? this._ignoreEvent : (this._ignoreEvent = this.obj ? this.obj.ignoreEvent : []);
    }
    set ignoreEvent(value: any) {
        this._ignoreEvent = value;
    }

    get maintenances(): any {
        return this._maintenances ? this._maintenances : (this._maintenances = this.obj ? this.obj.maintenances : []);
    }
    set maintenances(value: any) {
        this._maintenances = value;
    }

    get customers(): any {
        return this._customers ? this._customers : (this._customers = this.obj ? this.obj.customers : []);
    }
    set customers(value: any) {
        this._customers = value;
    }

    get forcedCalcOptions(): CalcOptions {
        return this._forcedCalcOptions ? this._forcedCalcOptions : (this._forcedCalcOptions = this.obj ? this.obj.forcedCalcOptions : []);
    }
    set forcedCalcOptions(value: CalcOptions) {
        this._forcedCalcOptions = value;
    }

    get forcedLimitsDefinition(): LimitsDefinition {
        return this._forcedLimitsDefinition ? this._forcedLimitsDefinition : (this._forcedLimitsDefinition = this.obj ? this.obj.forcedLimitsDefinition : []);
    }
    set forcedLimitsDefinition(value: LimitsDefinition) {
        this._forcedLimitsDefinition = value;
    }



    public toJSON() {
        return {
            ao: this._ao,
            barring: this.barring,
            busbarLineMeasurement: this.busbarLineMeasurement,
            busbarType: this.busbarType,
            childDevices: this.childDevices,
            code: this.code,
            configs: this.configs,
            county: this.county,
            created: this.created,
            customerMeasured: this.customerMeasured,
            customers: this.customers,
            deliveryPoint: this.deliveryPoint,
            description: this.description,
            deviceZoneA: this.deviceZoneA,
            deviceZoneB: this.deviceZoneB,
            deviceZoneC: this.deviceZoneC,
            district: this.district,
            documents: this.documents,
            drc: this.drc,
            equipmentLocation: this.equipmentLocation,
            eventProcess: this.eventProcess,
            forcedCalcOptions: this.forcedCalcOptions,
            forcedLimitsDefinition: this.forcedLimitsDefinition,
            id: this.id,
            ignoreData: this.ignoreData,
            ignoreEvent: this.ignoreEvent,
            injector: this.injector,
            instalation: this.instalation,
            latitude: this.latitude,
            longitude: this.longitude,
            locationId: this.locationId,
            maintenances: this.maintenances,
            measureLocationType: this.measureLocationType,
            measurePointDescription: this.measurePointDescription,
            name: this.name,
            nominalVoltage: this.nominalVoltage,
            outOfServiceAux: this.outOfServiceAux,
            output: this.output,
            parentDeviceId: this.parentDeviceId,
            faultLevel: this.faultLevel,
            substation: this.substation,
            transformerTypeDownstream: this.transformerTypeDownstream,
            transformerTypeUpstream: this.transformerTypeUpstream,
            treeComponentProperties: this.treeComponentProperties,
            weekProcess: this.weekProcess,
            weekProcessAutomatic: this.weekProcessAutomatic
        };
    }
}
