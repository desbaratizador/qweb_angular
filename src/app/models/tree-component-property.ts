import { BaseModel } from './base-model';

export class TreeComponentProperty extends BaseModel {
    private _propertyName: string;
    private _propertyValue: string;
    private _parentTypeId: number;
    private _parentId: number;

    constructor(protected obj?: any) {
        super(obj);
    }

    get propertyName(): string {
        return this._propertyName ? this._propertyName : (this._propertyName = this.obj ? this.obj.propertyName : '');
    }
    set propertyName(value: string) {
        this._propertyName = value;
    }

    get propertyValue(): string {
        return this._propertyValue ? this._propertyValue : (this._propertyValue = this.obj ? this.obj.propertyValue : '');
    }
    set propertyValue(value: string) {
        this._propertyValue = value;
    }

    get parentTypeId(): number {
        return this._parentTypeId ? this._parentTypeId : (this._parentTypeId = this.obj ? this.obj.parentTypeId : 0);
    }
    set parentTypeId(value: number) {
        this._parentTypeId = value;
    }

    get parentId(): number {
        return this._parentId ? this._parentId : (this._parentId = this.obj ? this.obj.parentId : 0);
    }
    set parentId(value: number) {
        this._parentId = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            propertyName: this.propertyName,
            propertyValue: this.propertyValue,
            parentTypeId: this.parentTypeId,
            parentId: this.parentId
        };
    }
}
