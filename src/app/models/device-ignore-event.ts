import { DeviceIgnore } from './device-ignore';

export class DeviceIgnoreEvent extends DeviceIgnore {
        
    constructor(protected obj?: any) {
       super(obj);
    }    

    public toJSON() {
        return {
            id: this.id,
            created: this.created
        };
    }
}