import { BaseModel } from './base-model';
import { DeviceIgnore } from './device-ignore';

export class DeviceIgnoreData extends DeviceIgnore {
    private _variable: any;

    constructor(protected obj?: any) {
       super(obj);
    }

    get variable(): any {
        return this._variable ? this._variable : (this._variable = this.obj.variable || 0);
    }
    set variable(value: any) {
        this._variable = value;
    }
     
    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            variable: this.variable,
        };
    }
}