import { BaseModel } from './base-model';

export class WeekProcessLog extends BaseModel {
    private _startDate: Date;
    private _endDate: Date;
    private _result: boolean;
    private _message: string;

    constructor(protected obj?: any) {
       super(obj);
    }

    get startDate(): Date {
        return this._startDate ? this._startDate : (this._startDate = this.obj.startDate || '');
    }
    set startDate(value: Date) {
        this._startDate = value;
    } 

    get endDate(): Date {
        return this._endDate ? this._endDate : (this._endDate = this.obj.endDate || '');
    }
    set endDate(value: Date) {
        this._endDate = value;
    }

    get result(): boolean {
        return this._result ? this._result : (this._result = this.obj.result || '');
    }
    set result(value: boolean) {
        this._result = value;
    } 

    get message(): string {
        return this._message ? this._message : (this._message = this.obj.message || '');
    }
    set message(value: string) {
        this._message = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            startDate: this.startDate,
            endDate: this.endDate,
            result: this.result,
            message: this.message
        };
    }
}