import { BaseModel } from './base-model';

export class LimitsDefinition extends BaseModel {
    private _description: string;
    private _global: boolean;
    private _selected: boolean;

    private _limits: any;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }
    set description(value: string) {
        this._description = value;
    } 

    get global(): boolean {
        return this._global ? this._global : (this._global = this.obj.global || false);
    }
    set global(value: boolean) {
        this._global = value;
    }

    get selected(): boolean {
        return this._selected ? this._selected : (this._selected = this.obj.selected || false);
    }
    set selected(value: boolean) {
        this._selected = value;
    }

    get limits(): any {
        return this._limits ? this._limits : (this._limits = this.obj.limits || []);
    }
    set limits(value: any) {
        this._limits = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            description: this.description,
            global: this.global,
            selected: this.selected
        };
    }
}