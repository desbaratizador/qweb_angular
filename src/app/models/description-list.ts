import { BaseModel } from './base-model';

export class DescriptionList extends BaseModel {
    private _code: string;
    private _description: string;
    private _context: string;
    private _orderIndex: number;

    constructor(protected obj?: any) {
        super(obj);
    }

    get code(): string {
        return this._code ? this._code : (this._code = this.obj.code || '');
    }
    set code(value: string) {
        this._code = value;
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }
    set description(value: string) {
        this._description = value;
    }


    get context(): string {
        return this._context ? this._context : (this._context = this.obj.context || '');
    }
    set context(value: string) {
        this._context = value;
    }

    get orderIndex(): number {
        return this._orderIndex ? this._orderIndex : (this._orderIndex = this.obj.orderIndex || 0);
    }
    set orderIndex(value: number) {
        this._orderIndex = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            code: this.code,
            description: this.description,
            context: this.context,
            orderIndex: this.orderIndex,
        };
    }
}
