import { Profile } from './profile';
import { BaseModel } from './base-model';
import { ModelInterface } from './model.interface';

export class User extends BaseModel implements ModelInterface {
    private _login: string;
    private _pwd: string;
    private _firstname: string;
    private _lastname: string;
    private _email: string;
    private _department: string;
    private _language: string;
    private _notify: boolean;
    private _profile: Profile;
    private _calcOptions: any[];
    private _limitsDefinition: any[];

    constructor(protected obj?: any) {
        super(obj);
    }


    get login(): string {
        return this._login ? this._login : (this._login = this.obj ? this.obj.login : '');
    }
    set login(value: string) {
        this._login = value;
    }

    get pwd(): string {
        return this._pwd ? this._pwd : (this._pwd = this.obj ? this.obj.pwd : '');
    }
    set pwd(value: string) {
        this._pwd = value;
    }

    get firstname(): string {
        return this._firstname ? this._firstname : (this._firstname = this.obj ? this.obj.firstname : '');
    }
    set firstname(value: string) {
        this._firstname = value;
    }

    get lastname(): string {
        return this._lastname ? this._lastname : (this._lastname = this.obj ? this.obj.lastname : '');
    }
    set lastname(value: string) {
        this._lastname = value;
    }

    get fullname(): string {
        return `${this.firstname} ${this.lastname}`;
    }

    get email(): string {
        return this._email ? this._email : (this._email = this.obj ? this.obj.email : '');
    }
    set email(value: string) {
        this._email = value;
    }

    get department(): string {
        return this._department ? this._department : (this._department = this.obj ? this.obj.department : '');
    }
    set department(value: string) {
        this._department = value;
    }

    get language(): string {
        return this._language ? this._language : (this._language = this.obj ? this.obj.language : '');
    }
    set language(value: string) {
        this._language = value;
    }

    get notify(): boolean {
        return this._notify ? this._notify : (this._notify = this.obj ? this.obj.notify : false);
    }
    set notify(value: boolean) {
        this._notify = value;
    }

    get isAdmin(): boolean {
        return this.obj ? this.obj.isadmin : (this.obj.isadmin = false);
    }

    get profile(): Profile {
        return this._profile ? this._profile : (this._profile = this.obj ? new Profile(this.obj.profile) : new Profile());
    }
    set profile(value: Profile) {
        this._profile = value;
    }

    get calcOptions(): any[] {
        return this._calcOptions || (this._calcOptions = this.obj ? this.obj.calcOptions : []);
    }

    get limitsDefinition(): any[] {
        return this._limitsDefinition || (this._limitsDefinition = this.obj ? this.obj.limitsDefinition : []);
    }


    public toJSON() {
        return {
            id: this.id,
            login: this.login,
            pwd: this.pwd,
            firstname: this.firstname,
            lastname: this.lastname,
            email: this.email,
            department: this.department,
            language: this.language,
            notify: this.notify,
            created: this.created,
            profile: this.profile.toJSON()
        };
    }
}
