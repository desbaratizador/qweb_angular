import { BaseModel } from './base-model';
import { DataSourceType } from './data-source-type';
import { Variable } from './variable';

export class VariableDataSourceType extends BaseModel {

    private _variableId: number;
    private _dataSourceType: DataSourceType;
    private _externalId: number;
    
    constructor(protected obj?: any) {
       super(obj);
    }
    
    get variableId(): number {
        return this._variableId ? this._variableId : (this._variableId = this.obj.variableId || 0);
    }
    set variableId(value: number) {
        this._variableId = value;
    }

    get dataSourceType(): DataSourceType {
        return this._dataSourceType ? this._dataSourceType : (this._dataSourceType = this.obj.dataSourceType || new DataSourceType());
    }
    set dataSourceType(value: DataSourceType) {
        this._dataSourceType = value;
    } 

    get externalId(): number {
        return this._externalId ? this._externalId : (this._externalId = this.obj.externalId || 0);
    }
    set externalId(value: number) {
        this._externalId = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.variableId,
            dataSourceType: this.dataSourceType,
            externalId: this._externalId    
        };
    }
}