import { BaseModel } from './base-model';

export class DatetimeInterval extends BaseModel {
    private _startDate: Date;
    private _endDate: Date;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get startDate(): Date {
        return this._startDate ? this._startDate : (this._startDate = this.obj.startDate || new Date());
    }
    set startDate(value: Date) {
        this._startDate = value;
    } 

    get endDate(): Date {
        return this._endDate ? this._endDate : (this._endDate = this.obj.endDate || new Date());
    }
    set endDate(value: Date) {
        this._endDate = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            startDate: this.startDate,
            endDate: this.endDate
        };
    }
}