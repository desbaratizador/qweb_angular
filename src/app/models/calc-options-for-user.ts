import { BaseModel } from './base-model';
import { DescriptionList } from './description-list';
import { ModelInterface } from './model.interface';

export class CalcOptionsForUser extends BaseModel implements ModelInterface {
    private _description: string;
    private _language: string;
    private _harmonicsFirst: number;
    private _harmonicsSecond: number;
    private _harmonicsThird: number;
    private _excelExportType: DescriptionList;
    private _selMostPositiveWeek: boolean;
    private _selRepresentativeWeek: boolean;
    private _selMostNegativeWeek: boolean;

    constructor(protected obj?: any) {
        super(obj);
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj ? this.obj.description : '');
    }
    set description(value: string) {
        this._description = value;
    }

    get language(): string {
        return this._language ? this._language : (this._language = this.obj ? this.obj.language : '');
    }
    set language(value: string) {
        this._language = value;
    }

    get harmonicsFirst(): number {
        return this._harmonicsFirst ? this._harmonicsFirst : (this._harmonicsFirst = this.obj ? this.obj.harmonicsFirst : 0);
    }
    set harmonicsFirst(value: number) {
        this._harmonicsFirst = value;
    }

    get harmonicsSecond(): number {
        return this._harmonicsSecond ? this._harmonicsSecond : (this._harmonicsSecond = this.obj ? this.obj.harmonicsSecond : 0);
    }
    set harmonicsSecond(value: number) {
        this._harmonicsSecond = value;
    }

    get harmonicsThird(): number {
        return this._harmonicsThird ? this._harmonicsThird : (this._harmonicsThird = this.obj ? this.obj.harmonicsThird : 0);
    }
    set harmonicsThird(value: number) {
        this._harmonicsThird = value;
    }
    get excelExportType(): DescriptionList {
        return this._excelExportType ? this._excelExportType : (this._excelExportType = this.obj ? this.obj.excelExportType : 0);
    }
    set excelExportType(value: DescriptionList) {
        this._excelExportType = value;
    }

    get selMostPositiveWeek(): boolean {
        return this._selMostPositiveWeek ? this._selMostPositiveWeek : (this._selMostPositiveWeek = this.obj ? this.obj.selMostPositiveWeek : 0);
    }
    set selMostPositiveWeek(value: boolean) {
        this._selMostPositiveWeek = value;
    }

    get selRepresentativeWeek(): boolean {
        return this._selRepresentativeWeek ? this._selRepresentativeWeek : (this._selRepresentativeWeek = this.obj ? this.obj.selRepresentativeWeek : 0);
    }
    set selRepresentativeWeek(value: boolean) {
        this._selRepresentativeWeek = value;
    }

    get selMostNegativeWeek(): boolean {
        return this._selMostNegativeWeek ? this._selMostNegativeWeek : (this._selMostNegativeWeek = this.obj ? this.obj.selMostNegativeWeek : 0);
    }
    set selMostNegativeWeek(value: boolean) {
        this._selMostNegativeWeek = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            description: this.description,
            language: this.language,
            harmonicsFirst: this.harmonicsFirst,
            harmonicsSecond: this.harmonicsSecond,
            harmonicsThird: this._harmonicsThird,
            selMostPositiveWeek: this.selMostPositiveWeek,
            selRepresentativeWeek: this.selRepresentativeWeek,
            selMostNegativeWeek: this.selMostNegativeWeek
        };
    }
}
