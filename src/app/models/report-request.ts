import { BaseModel } from './base-model';
import { Device } from './device';
import { Variable } from './variable';
import { DatetimeInterval } from './datetime-interval';
import { Report } from './report';

export class ReportRequest extends BaseModel {
    private _report: Report;
    protected _device: Device;
    protected _variable: Variable;
    protected _datetimeInterval: DatetimeInterval;
    protected _applyDeviceConfig: boolean;
    protected _errorMessage: string;


    constructor(protected obj?: any) {
       super(obj);
       this._applyDeviceConfig = true;
    }

    get report(): Report {
        return this._report ? this._report : (this._report = this.obj.report || '');
    }
    set report(value: Report) {
        this._report = value;
    } 
    get device(): Device {
        return this._device ? this._device : (this._device = this.obj.device || new Device());
    }
    set device(value: Device) {
        this._device = value;
    } 

    get variable(): Variable {
        return this._variable ? this._variable : (this._variable = this.obj.variable || new Variable());
    }
    set variable(value: Variable) {
        this._variable = value;
    } 

    get datetimeInterval(): DatetimeInterval {
        return this._datetimeInterval ? this._datetimeInterval : (this._datetimeInterval = this.obj.datetimeInterval || new DatetimeInterval());
    }
    set datetimeInterval(value: DatetimeInterval) {
        this._datetimeInterval = value;
    }

    get applyDeviceConfig(): boolean {
        return this._applyDeviceConfig ? this._applyDeviceConfig : (this._applyDeviceConfig = this.obj.applyDeviceConfig || false);
    }
    set applyDeviceConfig(value: boolean) {
        this._applyDeviceConfig = value;
    }

    get errorMessage(): string {
        return this._errorMessage ? this._errorMessage : (this._errorMessage = this.obj.errorMessage || '');
    }
    set errorMessage(value: string) {
        this._errorMessage = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            report: this.report,
            device: this.device,
            variable: this.variable,
            datetimeInterval: this.datetimeInterval,
            applyDeviceConfig: this.applyDeviceConfig,
            errorMessage: this.errorMessage
        };
    }
}