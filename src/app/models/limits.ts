import { BaseModel } from './base-model';
import { Description } from './description';
import { Variable } from './variable';

export class Limits extends BaseModel {
    
    private _limitsDefinitionId: number;
    private _tensionLevel: Description;
    private _min100: number;
    private _max100: number;
    private _min95: number;
    private _max95: number;
    private _min99: number;
    private _max99: number;
    private _min995: number;
    private _max995: number;
    private _variable: Variable;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get limitsDefinitionId(): number {
        return this._limitsDefinitionId ? this._limitsDefinitionId : (this._limitsDefinitionId = this.obj.limitsDefinitionId || 0);
    }
    set imitsDefinitionId(value: number) {
        this._limitsDefinitionId = value;
    } 

    get tensionLevel(): Description {
        return this._tensionLevel ? this._tensionLevel : (this._tensionLevel = this.obj.tensionLevel || new Description());
    }
    set tensionLevel(value: Description) {
        this._tensionLevel = value;
    } 

    get min100(): number {
        return this._min100 ? this._min100 : (this._min100 = this.obj.min100 || 0);
    }
    set min100(value: number) {
        this._min100 = value;
    }

    get max100(): number {
        return this._max100 ? this._max100 : (this._max100 = this.obj.max100 || 0);
    }
    set max100(value: number) {
        this._max100 = value;
    }

    get min95(): number {
        return this._min95 ? this._min95 : (this._min95 = this.obj.min95 || 0);
    }
    set min95(value: number) {
        this._min95 = value;
    }

    get max95(): number {
        return this._max95 ? this._max95 : (this._max95 = this.obj.max95 || 0);
    }
    set max95(value: number) {
        this._max95 = value;
    }

    get min99(): number {
        return this._min99 ? this._min99 : (this._min99 = this.obj.min99 || 0);
    }
    set min99(value: number) {
        this._min99 = value;
    }

    get max99(): number {
        return this._max99 ? this._max99 : (this._max99 = this.obj.max99 || 0);
    }
    set max99(value: number) {
        this._max99 = value;
    }

    get min995(): number {
        return this._min995 ? this._min995 : (this._min995 = this.obj.min995 || 0);
    }
    set min995(value: number) {
        this._min995 = value;
    }

    get max995(): number {
        return this._max995 ? this._max995 : (this._max995 = this.obj.max995 || 0);
    }
    set max995(value: number) {
        this._max995 = value;
    }

    get variable(): Variable {
        return this._variable ? this._variable : (this._variable = this.obj.variable || new Variable());
    }
    set variable(value: Variable) {
        this._variable = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            limitsDefinitionId: this.limitsDefinitionId,
            tensionLevel: this.tensionLevel,
            min100: this.min100,
            max100: this.max100,
            min95: this.min95,
            max95: this.max95,
            min99: this.min99,
            max99: this.max99,
            min995: this.min995,
            max995: this.max995,
            variable: this.variable    
        };
    }
}