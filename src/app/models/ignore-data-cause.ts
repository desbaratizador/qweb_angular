import { BaseModel } from './base-model';
import { DescriptionList } from './description-list';

export class IgnoreDataCause extends BaseModel {
    private _code: string;
    private _description: string;
    private _appliesTo: DescriptionList; 
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get code(): string {
        return this._code ? this._code : (this._code = this.obj.code || '');
    }
    set code(value: string) {
        this._code = value;
    } 

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }
    set description(value: string) {
        this._description = value;
    } 

    get appliesTo(): DescriptionList {
        return this._appliesTo ? this._appliesTo : (this._appliesTo = this.obj.appliesTo || 0);
    }
    set appliesTo(value: DescriptionList) {
        this._appliesTo = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            code: this.code,
            description: this.description,
            parentTypeId: this.appliesTo
        };
    }
}