import { Variable } from './variable';
import { DescriptionList } from './description-list';
import { BaseModel } from './base-model';

export class Report extends BaseModel {
    private _name: string;
    private _code: string;
    private _generic: boolean;
    private _pdfOutput: boolean;
    private _excelOutput: boolean;
    private _excelOutputRecords: boolean;
    private _wordXOutput: boolean;
    private _reportType: DescriptionList;
    private _variable: Variable;
    private _resultURL: string;
    private _resultURLExcelRecords: string;

    private _reportParts: any;
    private _records: any;


    constructor(protected obj?: any) {
        super(obj);
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj.name || '');
    }
    set name(value: string) {
        this._name = value;
    }

    get code(): string {
        return this._code ? this._code : (this._code = this.obj.code || '');
    }
    set code(value: string) {
        this._code = value;
    }

    get generic(): boolean {
        return this._generic ? this._generic : (this._generic = this.obj.generic || false);
    }
    set generic(value: boolean) {
        this._generic = value;
    }

    get pdfOutput(): boolean {
        return this._pdfOutput ? this._pdfOutput : (this._pdfOutput = this.obj.pdfOutput || false);
    }
    set pdfOutput(value: boolean) {
        this._pdfOutput = value;
    }

    get excelOutput(): boolean {
        return this._excelOutput ? this._excelOutput : (this._excelOutput = this.obj.excelOutput || false);
    }
    set excelOutput(value: boolean) {
        this._excelOutput = value;
    }

    get excelOutputRecords(): boolean {
        return this._excelOutputRecords ? this._excelOutputRecords : (this._excelOutputRecords = this.obj.excelOutputRecords || false);
    }
    set excelOutputRecords(value: boolean) {
        this._excelOutputRecords = value;
    }

    get wordXOutput(): boolean {
        return this._wordXOutput ? this._wordXOutput : (this._wordXOutput = this.obj.wordXOutput || false);
    }
    set wordXOutput(value: boolean) {
        this._wordXOutput = value;
    }

    get reportType(): DescriptionList {
        return this._reportType ? this._reportType : (this._reportType = this.obj.reportType || new DescriptionList());
    }
    set reportType(value: DescriptionList) {
        this._reportType = value;
    }

    get variable(): Variable {
        return this._variable ? this._variable : (this._variable = this.obj.variable || new Variable());
    }
    set variable(value: Variable) {
        this._variable = value;
    }

    get resultURL(): string {
        return this._resultURL ? this._resultURL : (this._resultURL = this.obj.resultURL || '');
    }
    set resultURL(value: string) {
        this._resultURL = value;
    }

    get resultURLExcelRecords(): string {
        return this._resultURLExcelRecords ? this._resultURLExcelRecords : (this._resultURLExcelRecords = this.obj.resultURLExcelRecords || '');
    }
    set resultURLExcelRecords(value: string) {
        this._resultURLExcelRecords = value;
    }

    get reportParts(): any {
        return this._reportParts ? this._reportParts : (this._reportParts = this.obj.reportParts || []);
    }
    set reportParts(value: any) {
        this._reportParts = value;
    }

    get records(): any {
        return this._records ? this._records : (this._records = this.obj.records || []);
    }
    set records(value: any) {
        this._records = value;
    }
    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.name,
            generic: this.generic,
            pdfOutput: this.pdfOutput,
            excelOutput: this.excelOutput,
            excelOutputRecords: this.excelOutputRecords,
            wordXOutput: this.wordXOutput,
            reportType: this.reportType,
            variable: this.variable,
            resultURL: this.resultURL,
            resultURLExcelRecords: this.resultURLExcelRecords,
            reportParts: this.reportParts,
            records: this.records

        };
    }
}
