import { ReportRequest } from './report-request';

export class ReportRequestMultiDevices extends ReportRequest {
    
    private _devices: any;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get devices(): any {
        return this._devices ? this._devices : (this._devices = this.obj.devices || []);
    }
    set devices(value: any) {
        this._devices = value;
    } 
}