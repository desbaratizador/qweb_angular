import { BaseModel } from './base-model';
import { IgnoreDataCause } from './ignore-data-cause';
import { DeviceMaintenance } from './device-maintenance';
import { User } from './user';

export class DeviceIgnore extends BaseModel {
    private _deviceId: number;
    private _startDate: Date;
    private _endDate: Date;
    private _user: User;
    private _ignoreDataCause: IgnoreDataCause;
    private _obs: string;

    constructor(protected obj?: any) {
        super(obj);
    }

    get deviceId(): number {
        return this._deviceId ? this._deviceId : (this._deviceId = this.obj.deviceId || 0);
    }
    set deviceId(value: number) {
        this._deviceId = value;
    }

    get startDate(): Date {
        return this._startDate ? this._startDate : (this._startDate = this.obj.startDate || new Date());
    }
    set startDate(value: Date) {
        this._startDate = value;
    }

    get endDate(): Date {
        return this._endDate ? this._endDate : (this._endDate = this.obj.endDate || new Date());
    }
    set endDate(value: Date) {
        this._endDate = value;
    }

    get user(): User {
        return this._user ? this._user : (this._user = this.obj.user || new User());
    }
    set user(value: User) {
        this._user = value;
    }

    get ignoreDataCause(): IgnoreDataCause {
        return this._ignoreDataCause ? this._ignoreDataCause : (this._ignoreDataCause = this.obj.ignoreDataCause || new IgnoreDataCause());
    }
    set ignoreDataCause(value: IgnoreDataCause) {
        this._ignoreDataCause = value;
    }

    get obs(): string {
        return this._obs ? this._obs : (this._obs = this.obj.obs || '');
    }
    set obs(value: string) {
        this._obs = value;
    }
}
