import { BaseModel } from './base-model';
import { AggrEventMethod } from './aggr-event-method';
import { Description } from './description';

export class CalcOptions extends BaseModel {
    private _description: string;
    private _aggregationTime: number;
    private _aggrEventDuration: Description;
    private _aggrEventProfound: Description;
    private _aggrEventMethod: AggrEventMethod;
    private _aggrLimitMinimumDipTension: number;
    private _aggrLimitMaximumDipTension: number;
    private _aggrLimitMinimumDipTime: number;
    private _aggrLimitMaximumDipTime: number;
    private _aggrLimitMinimumSwellTension: number;
    private _aggrLimitMaximumSwellTension: number;
    private _aggrLimitMinimumSwellTime: number;
    private _aggrLimitMaximumSwellTime: number;
    private _aggrLimitMinimumInterruptionTension: number;
    private _aggrLimitMaximumInterruptionTension: number;
    private _aggrLimitMinimumInterruptionTime: number;
    private _aggrLimitMaximumInterruptionTime: number;
    private _voltageLimitToRMS: number;
    private _voltageLimitToOther: number;
    private _voltageLimitToRMSMax: number;
    private _voltageLimitToOtherMax: number;
    private _voltageLimitToRMSAtLeastOnePhase: boolean;
    private _voltageLimitToOtherAtLeastOnePhase: boolean;
    private _interruptionAllPhases: boolean;
    private _global: boolean;
    private _selected: boolean;

    constructor(protected obj?: any) {
       super(obj);
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }
    set description(value: string) {
        this._description = value;
    } 

    get aggregationTime(): number {
        return this._aggregationTime ? this._aggregationTime : (this._aggregationTime = this.obj.aggregationTime || 0);
    }
    set aggregationTime(value: number) {
        this._aggregationTime = value;
    }

    get aggrEventDuration(): Description {
        return this._aggrEventDuration ? this._aggrEventDuration : (this._aggrEventDuration = new Description(this.obj.aggrEventDuration) || new Description());
    }
    set aggrEventDuration(value: Description) {
        this._aggrEventDuration = value;
    }

    get aggrEventProfound(): Description {
        return this._aggrEventProfound ? this._aggrEventProfound : (this._aggrEventProfound = new Description(this.obj.aggrEventProfound) || new Description());
    }
    set aggrEventProfound(value: Description) {
        this._aggrEventProfound = value;
    }

    get aggrEventMethod(): AggrEventMethod {
        return this._aggrEventMethod ? this._aggrEventMethod : (this._aggrEventMethod = new AggrEventMethod(this.obj.aggrEventMethod) || new AggrEventMethod());
    }
    set aggrEventMethod(value: AggrEventMethod) {
        this._aggrEventMethod = value;
    }

    get aggrLimitMinimumDipTension(): number {
        return this._aggrLimitMinimumDipTension ? this._aggrLimitMinimumDipTension : (this._aggrLimitMinimumDipTension = this.obj.aggrLimitMinimumDipTension || 0);
    }
    set aggrLimitMinimumDipTension(value: number) {
        this._aggrLimitMinimumDipTension = value;
    }

    get aggrLimitMaximumDipTension(): number {
        return this._aggrLimitMaximumDipTension ? this._aggrLimitMaximumDipTension : (this._aggrLimitMaximumDipTension = this.obj.aggrLimitMaximumDipTension || 0);
    }
    set aggrLimitMaximumDipTension(value: number) {
        this._aggrLimitMaximumDipTension = value;
    }

    get aggrLimitMinimumDipTime(): number {
        return this._aggrLimitMinimumDipTime ? this._aggrLimitMinimumDipTime : (this._aggrLimitMinimumDipTime = this.obj.aggrLimitMinimumDipTime || 0);
    }
    set aggrLimitMinimumDipTime(value: number) {
        this._aggrLimitMinimumDipTime = value;
    }

    get aggrLimitMaximumDipTime(): number {
        return this._aggrLimitMaximumDipTime ? this._aggrLimitMaximumDipTime : (this._aggrLimitMaximumDipTime = this.obj.aggrLimitMaximumDipTime || 0);
    }
    set aggrLimitMaximumDipTime(value: number) {
        this._aggrLimitMaximumDipTime = value;
    }

    get aggrLimitMinimumSwellTension(): number {
        return this._aggrLimitMinimumSwellTension ? this._aggrLimitMinimumSwellTension : (this._aggrLimitMinimumSwellTension = this.obj.aggrLimitMinimumSwellTension || 0);
    }
    set aggrLimitMinimumSwellTension(value: number) {
        this._aggrLimitMinimumSwellTension = value;
    }

    get aggrLimitMaximumSwellTension(): number {
        return this._aggrLimitMaximumSwellTension ? this._aggrLimitMaximumSwellTension : (this._aggrLimitMaximumSwellTension = this.obj.aggrLimitMaximumSwellTension || 0);
    }
    set aggrLimitMaximumSwellTension(value: number) {
        this._aggrLimitMaximumSwellTension = value;
    }

    get aggrLimitMinimumSwellTime(): number {
        return this._aggrLimitMinimumSwellTime ? this._aggrLimitMinimumSwellTime : (this._aggrLimitMinimumSwellTime = this.obj.aggrLimitMinimumSwellTime || 0);
    }
    set aggrLimitMinimumSwellTime(value: number) {
        this._aggrLimitMinimumSwellTime = value;
    }

    get aggrLimitMaximumSwellTime(): number {
        return this._aggrLimitMaximumSwellTime ? this._aggrLimitMaximumSwellTime : (this._aggrLimitMaximumSwellTime = this.obj.aggrLimitMaximumSwellTime || 0);
    }
    set aggrLimitMaximumSwellTime(value: number) {
        this._aggrLimitMaximumSwellTime = value;
    }

    get aggrLimitMinimumInterruptionTension(): number {
        return this._aggrLimitMinimumInterruptionTension ? this._aggrLimitMinimumInterruptionTension : (this._aggrLimitMinimumInterruptionTension = this.obj.aggrLimitMinimumInterruptionTension || 0);
    }
    set aggrLimitMinimumInterruptionTension(value: number) {
        this._aggrLimitMinimumInterruptionTension = value;
    }

    get aggrLimitMaximumInterruptionTension(): number {
        return this._aggrLimitMaximumInterruptionTension ? this._aggrLimitMaximumInterruptionTension : (this._aggrLimitMaximumInterruptionTension = this.obj.aggrLimitMaximumInterruptionTension || 0);
    }
    set aggrLimitMaximumInterruptionTension(value: number) {
        this._aggrLimitMaximumInterruptionTension = value;
    }

    get aggrLimitMinimumInterruptionTime(): number {
        return this._aggrLimitMinimumInterruptionTime ? this._aggrLimitMinimumInterruptionTime : (this._aggrLimitMinimumInterruptionTime = this.obj.aggrLimitMinimumInterruptionTime || 0);
    }
    set aggrLimitMinimumInterruptionTime(value: number) {
        this._aggrLimitMinimumInterruptionTime = value;
    }

    get aggrLimitMaximumInterruptionTime(): number {
        return this._aggrLimitMaximumInterruptionTime ? this._aggrLimitMaximumInterruptionTime : (this._aggrLimitMaximumInterruptionTime = this.obj.aggrLimitMaximumInterruptionTime || 0);
    }
    set aggrLimitMaximumInterruptionTime(value: number) {
        this._aggrLimitMaximumInterruptionTime = value;
    }

    get voltageLimitToRMS(): number {
        return this._voltageLimitToRMS ? this._voltageLimitToRMS : (this._voltageLimitToRMS = this.obj.voltageLimitToRMS || 0);
    }
    set voltageLimitToRMS(value: number) {
        this._voltageLimitToRMS = value;
    }

    get voltageLimitToOther(): number {
        return this._voltageLimitToOther ? this._voltageLimitToOther : (this._voltageLimitToOther = this.obj.voltageLimitToOther || 0);
    }
    set voltageLimitToOther(value: number) {
        this._voltageLimitToOther = value;
    }

    get voltageLimitToRMSMax(): number {
        return this._voltageLimitToRMSMax ? this._voltageLimitToRMSMax : (this._voltageLimitToRMSMax = this.obj.voltageLimitToRMSMax || 0);
    }
    set voltageLimitToRMSMax(value: number) {
        this._voltageLimitToRMSMax = value;
    }

    get voltageLimitToOtherMax(): number {
        return this._voltageLimitToOtherMax ? this._voltageLimitToOtherMax : (this._voltageLimitToOtherMax = this.obj.voltageLimitToOtherMax || 0);
    }
    set voltageLimitToOtherMax(value: number) {
        this._voltageLimitToOtherMax = value;
    }

    get voltageLimitToRMSAtLeastOnePhase(): boolean {
        return this._voltageLimitToRMSAtLeastOnePhase ? this._voltageLimitToRMSAtLeastOnePhase : (this._voltageLimitToRMSAtLeastOnePhase = this.obj.voltageLimitToRMSAtLeastOnePhase || false);
    }
    set voltageLimitToRMSAtLeastOnePhase(value: boolean) {
        this._voltageLimitToRMSAtLeastOnePhase = value;
    }

    get voltageLimitToOtherAtLeastOnePhase(): boolean {
        return this._voltageLimitToOtherAtLeastOnePhase ? this._voltageLimitToOtherAtLeastOnePhase : (this._voltageLimitToOtherAtLeastOnePhase = this.obj.voltageLimitToOtherAtLeastOnePhase || false);
    }
    set voltageLimitToOtherAtLeastOnePhase(value: boolean) {
        this._voltageLimitToOtherAtLeastOnePhase = value;
    }

    get interruptionAllPhases(): boolean {
        return this._interruptionAllPhases ? this._interruptionAllPhases : (this._interruptionAllPhases = this.obj.interruptionAllPhases || false);
    }
    set interruptionAllPhases(value: boolean) {
        this._interruptionAllPhases = value;
    }

    get global(): boolean {
        return this._global ? this._global : (this._global = this.obj.global || false);
    }
    set global(value: boolean) {
        this._global = value;
    }

    get selected(): boolean {
        return this._selected ? this._selected : (this._selected = this.obj.selected || false);
    }
    set selected(value: boolean) {
        this._selected = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            description: this.description,
            aggregationTime: this.aggregationTime,
            aggrEventDuration: this.aggrEventDuration.toJSON(),
            aggrEventProfound: this.aggrEventProfound.toJSON(),
            aggrEventMethod: this.aggrEventMethod.toJSON(),
            aggrLimitMinimumDipTension: this.aggrLimitMinimumDipTension,
            aggrLimitMaximumDipTension: this.aggrLimitMaximumDipTension,
            aggrLimitMinimumDipTime: this.aggrLimitMinimumDipTime,
            aggrLimitMaximumDipTime: this.aggrLimitMaximumDipTime,
            aggrLimitMinimumSwellTension: this.aggrLimitMinimumSwellTension,
            aggrLimitMaximumSwellTension: this.aggrLimitMaximumSwellTension,
            aggrLimitMinimumSwellTime: this.aggrLimitMinimumSwellTime,
            aggrLimitMaximumSwellTime: this.aggrLimitMaximumSwellTime,
            aggrLimitMinimumInterruptionTension: this.aggrLimitMinimumInterruptionTension,
            aggrLimitMaximumInterruptionTension: this.aggrLimitMaximumInterruptionTension,
            aggrLimitMinimumInterruptionTime: this.aggrLimitMinimumInterruptionTime,
            aggrLimitMaximumInterruptionTime: this.aggrLimitMaximumInterruptionTime,
            voltageLimitToRMS: this.voltageLimitToRMS,
            voltageLimitToOther: this.voltageLimitToOther,
            voltageLimitToRMSMax: this.voltageLimitToRMSMax,
            voltageLimitToOtherMax: this.voltageLimitToOtherMax,
            voltageLimitToRMSAtLeastOnePhase: this.voltageLimitToRMSAtLeastOnePhase,
            voltageLimitToOtherAtLeastOnePhase: this.voltageLimitToOtherAtLeastOnePhase,
            interruptionAllPhases: this.interruptionAllPhases,
            global: this.global,
            selected: this.selected

        };
    }
}