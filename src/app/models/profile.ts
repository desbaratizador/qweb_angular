import { BaseModel } from './base-model';

export class Profile extends BaseModel {
    private _name: string;
    private _calcOptions: any;
    private _limitsDefinition: any;
    private _calcOptionsForUser: any;
    private _changeOptions: boolean;
    private _profileModule: any;
    private _profileReport: any;
    private _profileTreeAccess: any;
    constructor(obj?: any) {
        super(obj);
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj.name || '');
    }
    set name(value: string) {
        this._name = value;
    }

    get changeOptions(): boolean {
        return this._changeOptions ? this._changeOptions : (this._changeOptions = this.obj.changeOptions || false);
    }
    set changeOptions(value: boolean) {
        this._changeOptions = value;
    }

    get calcOptions(): any {
        return this._calcOptions ? this._calcOptions : (this._calcOptions = this.obj.calcOptions || null);
    }
    set calcOptions(value: any) {
        this._calcOptions = value;
    }

    get calcOptionsForUser(): any {
        return this._calcOptionsForUser ? this._calcOptionsForUser : (this._calcOptionsForUser = this.obj.calcOptionsForUser || null);
    }
    set calcOptionsForUser(value: any) {
        this._calcOptionsForUser = value;
    }

    get limitsDefinition(): any {
        return this._limitsDefinition ? this._limitsDefinition : (this._limitsDefinition = this.obj.limitsDefinition || null);
    }
    set limitsDefinition(value: any) {
        this._limitsDefinition = value;
    }

    get profileModule(): any {
        return this._profileModule ? this._profileModule : (this._profileModule = this.obj.profileModule || null);
    }
    set profileModule(value: any) {
        this._profileModule = value;
    }

    get profileReport(): any {
        return this._profileReport ? this._profileReport : (this._profileReport = this.obj.profileReport || null);
    }
    set profileReport(value: any) {
        this._profileReport = value;
    }

    get profileTreeAccess(): any {
        return this._profileTreeAccess ? this._profileTreeAccess : (this._profileTreeAccess = this.obj.profileTreeAccess || null);
    }
    set profileTreeAccess(value: any) {
        this._profileTreeAccess = value;
    }

    public toJSON() {
        return {
            id: this.id,
            name: this.name,
            notify: this._changeOptions,
            created: this.created,
            calcOptions: this.calcOptions,
            calcOptionsForUser: this.calcOptionsForUser,
            profileModule: this.profileModule,
            profileReport: this.profileReport,
            profileTreeAccess: this.profileTreeAccess
        };
    }
}
