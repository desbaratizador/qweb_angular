import { BaseModel } from './base-model';

export class DeviceMaintenance extends BaseModel {
    private _deviceId: number;
    private _startDate: Date;
    private _endDate: Date;
    private _userId: number;
    private _maintenanceCauseId: number;
    private _maintenanceCause: any;
    private _validity: Date;
    private _serviceOrder: string;
    private _document: string;
    private _obs: string;

    constructor(protected obj?: any) {
        super(obj);
    }

    get deviceId(): number {
        return this._deviceId ? this._deviceId : (this.obj ? this.obj.deviceId : 0);
    }
    set deviceId(value: number) {
        this._deviceId = value;
    }

    get startDate(): Date {
        return this._startDate ? this._startDate : (this.obj ? this.obj.startDate : null);
    }
    set startDate(value: Date) {
        this._startDate = value;
    }

    get endDate(): Date {
        return this._endDate ? this._endDate : (this.obj ? this.obj.endDate : null);
    }
    set endDate(value: Date) {
        this._endDate = value;
    }

    get userId(): number {
        return this._userId ? this._userId : (this.obj ? this.obj.userId : null);
    }
    set userId(value: number) {
        this._userId = value;
    }

    get maintenanceCauseId(): number {
        return this._maintenanceCauseId ? this._maintenanceCauseId : (this.obj ? this.obj.maintenanceCauseId : null);
    }
    set maintenanceCauseId(value: number) {
        this._maintenanceCauseId = value;
    }

    get maintenanceCause(): number {
        return this._maintenanceCause ? this._maintenanceCause : (this.obj ? this.obj.maintenanceCause : null);
    }
    set maintenanceCause(value: number) {
        this._maintenanceCause = value;
    }

    get validity(): Date {
        return this._validity ? this._validity : (this.obj ? this.obj.validity : null);
    }
    set validity(value: Date) {
        this._validity = value;
    }

    get serviceOrder(): string {
        return this._serviceOrder ? this._serviceOrder : (this.obj ? this.obj.serviceOrder : null);
    }
    set serviceOrder(value: string) {
        this._serviceOrder = value;
    }

    get document(): string {
        return this._document ? this._document : (this.obj ? this.obj.document : null);
    }
    set document(value: string) {
        this._document = value;
    }

    get obs(): string {
        return this._obs ? this._obs : (this.obj ? this.obj.obs : null);
    }
    set obs(value: string) {
        this._obs = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            deviceId: this.deviceId,
            startDate: this.startDate,
            endDate: this.endDate,
            userId: this.userId,
            maintenanceCauseId: this.maintenanceCauseId,
            maintenanceCause: this.maintenanceCause,
            validity: this.validity,
            serviceOrder: this.serviceOrder,
            document: this.document,
            obs: this.obs
        };
    }
}
