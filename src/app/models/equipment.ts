import { BaseModel } from './base-model';
import { Description } from './description';

export class Equipment extends BaseModel {
    private _name: string;
    private _qisCode: string;
    private _iqPlusCode: string;
    private _recordValidityInfo: boolean;
    private _equipmentManufacturer: Description;

    constructor(protected obj?: any) {
        super(obj);
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj ? this.obj.name : '');
    }
    set name(value: string) {
        this._name = value;
    }

    get qisCode(): string {
        return this._qisCode ? this._qisCode : (this._qisCode = this.obj ? this.obj.qisCode : '');
    }
    set qisCode(value: string) {
        this._qisCode = value;
    }

    get iqPlusCode(): string {
        return this._iqPlusCode ? this._iqPlusCode : (this._iqPlusCode = this.obj ? this.obj.iqPlusCode : '');
    }
    set iqPlusCode(value: string) {
        this._iqPlusCode = value;
    }

    get recordValidityInfo(): boolean {
        return this._recordValidityInfo ? this._recordValidityInfo : (this._recordValidityInfo = this.obj ? this.obj.recordValidityInfo : false);
    }
    set recordValidityInfo(value: boolean) {
        this._recordValidityInfo = value;
    }

    get equipmentManufacturer(): Description {
        return this._equipmentManufacturer ? this._equipmentManufacturer : (this._equipmentManufacturer = this.obj ? new Description(this.obj.equipmentManufacturer) : new Description());
    }
    set equipmentManufacturer(value: Description) {
        this._equipmentManufacturer = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.name,
            qisCode: this.qisCode,
            iqPlusCode: this.iqPlusCode,
            recordValidityInfo: this.recordValidityInfo,

            profequipmentManufacturerile: this.equipmentManufacturer.toJSON()
        };
    }
}
