import { BaseModel } from './base-model';

export class Customer extends BaseModel {
    private _deviceId: number;
    private _name: string;
    private _place: string;
    private _deliveryPointCode: string;
    private _substation: string;
    private _output: string;
    private _disturbanceTimeWindow: number;
    private _stopCost: number;
    private _obs: string;
    private _selected: boolean;    
    
    constructor(protected obj?: any) {
       super(obj);
       this._selected = false
    }

    get deviceId(): number {
        return this._deviceId ? this._deviceId : (this._deviceId = this.obj.deviceId || 0);
    }
    set deviceId(value: number) {
        this._deviceId = value;
    } 

    get name(): string {
        return this._name ? this._name : (this._name = this.obj.name || '');
    }
    set name(value: string) {
        this._name = value;
    } 

    get place(): string {
        return this._place ? this._place : (this._place = this.obj.place || '');
    }
    set place(value: string) {
        this._place = value;
    } 

    get deliveryPointCode(): string {
        return this._deliveryPointCode ? this._deliveryPointCode : (this._deliveryPointCode = this.obj.deliveryPointCode || '');
    }
    set deliveryPointCode(value: string) {
        this._deliveryPointCode = value;
    }

    get substation(): string {
        return this._substation ? this._substation : (this._substation = this.obj.substation || '');
    }
    set substation(value: string) {
        this._substation = value;
    }

    get output(): string {
        return this._output ? this._output : (this._output = this.obj.output || '');
    }
    set output(value: string) {
        this._output = value;
    }

    get disturbanceTimeWindow(): number {
        return this._disturbanceTimeWindow ? this._disturbanceTimeWindow : (this._disturbanceTimeWindow = this.obj.disturbanceTimeWindow || 0);
    }
    set disturbanceTimeWindow(value: number) {
        this._disturbanceTimeWindow = value;
    }

    get stopCost(): number {
        return this._stopCost ? this._stopCost : (this._stopCost = this.obj.stopCost || 0);
    }
    set stopCost(value: number) {
        this._stopCost = value;
    }

    get obs(): string {
        return this._obs ? this._obs : (this._obs = this.obj.obs || '');
    }
    set obs(value: string) {
        this._obs = value;
    }

    get selected(): boolean {
        return this._selected ? this._selected : (this._selected = this.obj.selected || false);
    }
    set selected(value: boolean) {
        this._selected = value;
    }   

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.name,
            place: this.place,
            deliveryPointCode: this.deliveryPointCode,
            substation: this.substation,
            output: this._output,
            disturbanceTimeWindow: this.disturbanceTimeWindow,
            stopCost: this.stopCost,
            obs: this.obs,
            selected: this.selected
        };
    }
}