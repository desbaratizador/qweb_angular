import { BaseModel } from './base-model';

export class Document extends BaseModel {
    private _name: string;
    private _description: string;
    private _parentTypeId: number;
    private _parentId: number;
    private _referenceDate: Date;

    constructor(protected obj?: any) {
        super(obj);
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj ? this.obj.name : '');
    }
    set name(value: string) {
        this._name = value;
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj ? this.obj.description : '');
    }
    set description(value: string) {
        this._description = value;
    }

    get parentTypeId(): number {
        return this._parentTypeId ? this._parentTypeId : (this._parentTypeId = this.obj ? this.obj.parentTypeId : 0);
    }
    set parentTypeId(value: number) {
        this._parentTypeId = value;
    }

    get parentId(): number {
        return this._parentId ? this._parentId : (this._parentId = this.obj ? this.obj.parentId : 0);
    }
    set parentId(value: number) {
        this._parentId = value;
    }

    get referenceDate(): Date {
        return this._referenceDate ? this._referenceDate : (this._referenceDate = this.obj ? this.obj.referenceDate : '');
    }
    set referenceDate(value: Date) {
        this._referenceDate = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.name,
            description: this.description,
            parentTypeId: this.parentTypeId,
            parentId: this.parentId,
            referenceDate: this.referenceDate
        };
    }
}
