import { BaseModel } from './base-model';

export class DataSourceType extends BaseModel {
    private _name: string;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj.name || '');
    }
    set name(value: string) {
        this._name = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.name
        };
    }
}