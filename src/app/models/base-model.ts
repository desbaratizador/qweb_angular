export class BaseModel {
    private _id: number;

    constructor(protected obj?: any) {
        if (!obj) {
            this.obj = {};
        }
    }

    get id(): number {
        return this._id ? this._id : (this._id = this.obj ? this.obj.id : 0);
    }

    set id(value: number) {
        this._id = value;
    }

    get created(): boolean {
        return this.id === 0;
    }
}
