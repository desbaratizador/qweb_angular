import { BaseModel } from './base-model';

export class AggrEventMethod extends BaseModel {
    private _description: string;
    private _aggrEventMacroMethodId: number;
    private _code: string;

    constructor(protected obj?: any) {
       super(obj);
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }

    set description(value: string) {
        this._description = value;
    } 

    get aggrEventMacroMethodId(): number {
        return this._aggrEventMacroMethodId ? this._aggrEventMacroMethodId : (this._aggrEventMacroMethodId = this.obj.aggrEventMacroMethodId || 0);
    }
    set aggrEventMacroMethodId(value: number) {
        this._aggrEventMacroMethodId = value;
    }

    get code(): string {
        return this._code ? this._code : (this._code = this.obj._code || '');
    }
    
    set code(value: string) {
        this._code = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            description: this.description
        };
    }
}