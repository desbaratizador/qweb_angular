import { BaseModel } from './base-model';
import { VariableDataSourceType } from './variable-data-source-type';

export class Variable extends BaseModel {
    private _name: string;
    private _useInLimits: boolean;

    private _variableDataSourceType: any;

    private _native: boolean;
    private _qisQuery: string;
    private _toIgnore: boolean;

    constructor(protected obj?: any) {
        super(obj);
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj ? this.obj.name : '');
    }
    set name(value: string) {
        this._name = value;
    }

    get useInLimits(): boolean {
        return this._useInLimits ? this._useInLimits : (this._useInLimits = this.obj ? this.obj.useInLimits : false);
    }
    set useInLimits(value: boolean) {
        this._useInLimits = value;
    }

    get variableDataSourceType(): any {
        return this._variableDataSourceType ? this._variableDataSourceType : (this._variableDataSourceType = this.obj ? this.obj.variableDataSourceType : []);
    }
    set variableDataSourceType(value: any) {
        this._variableDataSourceType = value;
    }

    get native(): boolean {
        return this._native ? this._native : (this._native = this.obj ? this.obj.native : false);
    }
    set native(value: boolean) {
        this._native = value;
    }

    get qisQuery(): string {
        return this._qisQuery ? this._qisQuery : (this._qisQuery = this.obj ? this.obj.qisQuery : '');
    }
    set qisQuery(value: string) {
        this._qisQuery = value;
    }

    get toIgnore(): boolean {
        return this._toIgnore ? this._toIgnore : (this._toIgnore = this.obj ? this.obj.toIgnore : false);
    }
    set toIgnore(value: boolean) {
        this._toIgnore = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            name: this.name,
            useInLimits: this.useInLimits,
            variableDataSourceType: this.variableDataSourceType,
            native: this.native,
            qisQuery: this.qisQuery,
            toIgnore: this.toIgnore
        };
    }
}
