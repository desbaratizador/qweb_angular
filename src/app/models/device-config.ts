import { BaseModel } from './base-model';
import { Equipment } from './equipment';
import { DataSource } from './data-source';
import { CalcOptions } from './calc-options';
import { Device } from './device';
import { LimitsDefinition } from './limits-definition';

export class DeviceConfig extends BaseModel {
    private _deviceId: number;
    private _startDate: Date;
    private _endDate: Date;
    private _dataSource: DataSource;
    private _deviceDataSourceId: string;
    private _equipment: Equipment;
    private _equipmentSerialNumber: string;
    private _simple: boolean;
    private _simpleCalc: boolean;
    private _calcOptions: CalcOptions;
    private _limitsDefinition: LimitsDefinition;
    private _declaredVoltage: number;
    private _ignoreRecordValidity: boolean;
    private _fixed: boolean;
    private _validateRecordsByEvents: boolean;
    private _validateRecordsByTensionExtremes: boolean;
    private _slidingDSI: boolean;
    private _indirectMeasuredAt: Device;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get deviceId(): number {
        return this._deviceId ? this._deviceId : (this._deviceId = this.obj.deviceId || 0);
    }
    set deviceId(value: number) {
        this._deviceId = value;
    } 

    get startDate(): Date {
        return this._startDate ? this._startDate : (this._startDate = this.obj.startDate || new Date());
    }
    set startDate(value: Date) {
        this._startDate = value;
    }

    get endDate(): Date {
        return this._endDate ? this._endDate : (this._endDate = this.obj.endDate || new Date());
    }
    set endDate(value: Date) {
        this._endDate = value;
    }

    get dataSource(): DataSource {
        return this._dataSource ? this._dataSource : (this._dataSource = this.obj.dataSource || new DataSource());
    }
    set dataSource(value: DataSource) {
        this._dataSource = value;
    }

    get deviceDataSourceId(): string {
        return this._deviceDataSourceId ? this._deviceDataSourceId : (this._deviceDataSourceId = this.obj.deviceDataSourceId || new DataSource());
    }
    set deviceDataSourceId(value: string) {
        this._deviceDataSourceId = value;
    }

    get equipment(): Equipment {
        return this._equipment ? this._equipment : (this._equipment = this.obj.equipment || new Equipment());
    }
    set equipment(value: Equipment) {
        this._equipment = value;
    }

    get equipmentSerialNumber(): string {
        return this._equipmentSerialNumber ? this._equipmentSerialNumber : (this._equipmentSerialNumber = this.obj.equipmentSerialNumber || '');
    }
    set equipmentSerialNumber(value: string) {
        this._equipmentSerialNumber = value;
    }

    get simple(): boolean {
        return this._simple ? this._simple : (this._simple = this.obj.simple || false);
    }
    set simple(value: boolean) {
        this._simple = value;
    }
    
    get simpleCalc(): boolean {
        return this._simpleCalc ? this._simpleCalc : (this._simpleCalc = this.obj.simpleCalc || false);
    }
    set simpleCalc(value: boolean) {
        this._simpleCalc = value;
    }

    get calcOptions(): CalcOptions {
        return this._calcOptions ? this._calcOptions : (this._calcOptions = this.obj.calcOptions || new CalcOptions());
    }
    set calcOptions(value: CalcOptions) {
        this._calcOptions = value;
    }

    get limitsDefinition(): LimitsDefinition {
        return this._limitsDefinition ? this._limitsDefinition : (this._limitsDefinition = this.obj.limitsDefinition || new LimitsDefinition());
    }
    set limitsDefinition(value: LimitsDefinition) {
        this._limitsDefinition = value;
    }

    get declaredVoltage(): number {
        return this._declaredVoltage ? this._declaredVoltage : (this._declaredVoltage = this.obj.declaredVoltage || 0);
    }
    set declaredVoltage(value: number) {
        this._declaredVoltage = value;
    }

    get ignoreRecordValidity(): boolean {
        return this._ignoreRecordValidity ? this._ignoreRecordValidity : (this._ignoreRecordValidity = this.obj.ignoreRecordValidity || false);
    }
    set ignoreRecordValidity(value: boolean) {
        this._ignoreRecordValidity = value;
    }
    
    get fixed(): boolean {
        return this._fixed ? this._fixed : (this._fixed = this.obj.fixed || false);
    }
    set fixed(value: boolean) {
        this._fixed = value;
    }

    get validateRecordsByEvents(): boolean {
        return this._validateRecordsByEvents ? this._validateRecordsByEvents : (this._validateRecordsByEvents = this.obj.validateRecordsByEvents || false);
    }
    set validateRecordsByEvents(value: boolean) {
        this._validateRecordsByEvents = value;
    }

    get validateRecordsByTensionExtremes(): boolean {
        return this._validateRecordsByTensionExtremes ? this._validateRecordsByTensionExtremes : (this._validateRecordsByTensionExtremes = this.obj.validateRecordsByTensionExtremes || false);
    }
    set validateRecordsByTensionExtremes(value: boolean) {
        this._validateRecordsByTensionExtremes = value;
    }

    get slidingDSI(): boolean {
        return this._slidingDSI ? this._slidingDSI : (this._slidingDSI = this.obj.slidingDSI || false);
    }
    set slidingDSI(value: boolean) {
        this._slidingDSI = value;
    }

    get indirectMeasuredAt(): Device {
        return this._indirectMeasuredAt ? this._indirectMeasuredAt : (this._indirectMeasuredAt = this.obj.indirectMeasuredAt || new Device());
    }
    set indirectMeasuredAt(value: Device) {
        this._indirectMeasuredAt = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            deviceId: this.deviceId,
            startDate: this.startDate,
            endDate: this.endDate,
            dataSource: this.dataSource,
            deviceDataSourceId: this.deviceDataSourceId,
            equipment: this.equipment,
            equipmentSerialNumber: this.equipmentSerialNumber,
            simple: this.simple,
            simpleCalc: this.simpleCalc,
            calcOptions: this.calcOptions,
            limitsDefinition: this.limitsDefinition,
            declaredVoltage: this.declaredVoltage,
            ignoreRecordValidity: this.ignoreRecordValidity,
            fixed: this.fixed,
            validateRecordsByEvents: this.validateRecordsByEvents,
            validateRecordsByTensionExtremes: this.validateRecordsByTensionExtremes,
            slidingDSI: this.slidingDSI,
            indirectMeasuredAt: this.indirectMeasuredAt
        };
    }
}
