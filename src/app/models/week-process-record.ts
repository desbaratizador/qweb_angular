import { BaseModel } from './base-model';
import { Device } from './device';

export class WeekProcessRecord extends BaseModel {
    private _startDate: Date;
    private _endDate: Date;
    private _weekNumber: string;
    private _device: Device;
    private _tensionDeclared: number;
    private _cablingMode: number;
    private _dPQI: number;
    private _lossPower: number;
    private _rMS: number;
    private _frequency: number;
    private _flicker: number;
    private _imbalance: number;
    private _tHD: number;
    private _harmonics: number;
    private _cPQI: number;
    private _harmonicIndex: number;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get startDate(): Date {
        return this._startDate ? this._startDate : (this._startDate = this.obj.startDate || '');
    }
    set startDate(value: Date) {
        this._startDate = value;
    } 

    get endDate(): Date {
        return this._endDate ? this._endDate : (this._endDate = this.obj.endDate || '');
    }
    set endDate(value: Date) {
        this._endDate = value;
    }

    get weekNumber(): string {
        return this._weekNumber ? this._weekNumber : (this._weekNumber = this.obj.weekNumber || '');
    }
    set weekNumber(value: string) {
        this._weekNumber = value;
    } 

    get device(): Device {
        return this._device ? this._device : (this._device = this.obj.device || 0);
    }
    set device(value: Device) {
        this._device = value;
    }

    get tensionDeclared(): number {
        return this._tensionDeclared ? this._tensionDeclared : (this._tensionDeclared = this.obj.tensionDeclared || 0);
    }
    set tensionDeclared(value: number) {
        this._tensionDeclared = value;
    }

    get cablingMode(): number {
        return this._cablingMode ? this._cablingMode : (this._cablingMode = this.obj.cablingMode || 0);
    }
    set cablingMode(value: number) {
        this._cablingMode = value;
    }

    get dPQI(): number {
        return this._dPQI ? this._dPQI : (this._dPQI = this.obj.dPQI || 0);
    }
    set dPQI(value: number) {
        this._dPQI = value;
    }

    get lossPower(): number {
        return this._lossPower ? this._lossPower : (this._lossPower = this.obj.lossPower || 0);
    }
    set lossPower(value: number) {
        this._lossPower = value;
    }

    get rMS(): number {
        return this._rMS ? this._rMS : (this._rMS = this.obj.rMS || 0);
    }
    set rMS(value: number) {
        this._rMS = value;
    }

    get frequency(): number {
        return this._frequency ? this._frequency : (this._frequency = this.obj.frequency || 0);
    }
    set frequency(value: number) {
        this._frequency = value;
    }

    get flicker(): number {
        return this._flicker ? this._flicker : (this._flicker = this.obj.flicker || 0);
    }
    set flicker(value: number) {
        this._flicker = value;
    }

    get imbalance(): number {
        return this._imbalance ? this._imbalance : (this._imbalance = this.obj.imbalance || 0);
    }
    set imbalance(value: number) {
        this._imbalance = value;
    }

    get tHD(): number {
        return this._tHD ? this._tHD : (this._tHD = this.obj.tHD || 0);
    }
    set tHD(value: number) {
        this._tHD = value;
    }

    get harmonics(): number {
        return this._harmonics ? this._harmonics : (this._harmonics = this.obj.harmonics || 0);
    }
    set harmonics(value: number) {
        this._harmonics = value;
    }
    
    get cPQI(): number {
        return this._cPQI ? this._cPQI : (this._cPQI = this.obj.cPQI || 0);
    }
    set cPQI(value: number) {
        this._cPQI = value;
    }

    get harmonicIndex(): number {
        return this._harmonicIndex ? this._harmonicIndex : (this._harmonicIndex = this.obj.harmonicIndex || 0);
    }
    set harmonicIndex(value: number) {
        this._harmonicIndex = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            startDate: this.startDate,
            endDate: this.endDate,
            weekNumber: this.weekNumber,
            device: this.device,
            tensionDeclared: this.tensionDeclared,
            cablingMode: this.cablingMode,
            dPQI: this.dPQI,
            lossPower: this.lossPower,
            rMS: this.rMS,
            frequency: this.frequency,
            flicker: this.flicker,
            imbalance: this.imbalance,
            tHD: this.tHD,
            harmonics: this.harmonics,
            cPQI: this.cPQI,
            harmonicIndex: this.harmonicIndex
        };
    }
}