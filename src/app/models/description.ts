import { BaseModel } from './base-model';

export class Description extends BaseModel {
    private _description: string;
    
    constructor(protected obj?: any) {
       super(obj);
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }
    set description(value: string) {
        this._description = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            description: this.description
        };
    }
}