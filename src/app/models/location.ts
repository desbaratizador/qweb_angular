import { BaseModel } from './base-model';
import { Device } from './device';
import { Document } from './document';
import { TreeComponentProperty } from './tree-component-property';

export class Location extends BaseModel {
    private _code: string;
    private _name: string;
    private _parentLocationId: number;

    private _locations: any;
    private _devices: any;
    private _documents: any;
    private _treeComponentProperty: any;

    constructor(protected obj?: any) {
        super(obj);
    }

    get code(): string {
        return this._code ? this._code : (this._code = this.obj ? this.obj.code : '');
    }
    set code(value: string) {
        this._code = value;
    }

    get name(): string {
        return this._name ? this._name : (this._name = this.obj ? this.obj.name : '');
    }
    set name(value: string) {
        this._name = value;
    }

    get parentLocationId(): number {
        return this._parentLocationId ? this._parentLocationId : (this._parentLocationId = this.obj ? this.obj.parentLocationId : 0);
    }
    set parentLocationId(value: number) {
        this._parentLocationId = value;
    }

    get locations(): any {
        return this._locations ? this._locations : (this._locations = this.obj ? this.obj.locations : []);
    }
    set locations(value: any) {
        this._locations = value;
    }

    get devices(): any {
        return this._devices ? this._devices : (this._devices = this.obj ? this.obj.devices : []);
    }
    set devices(value: any) {
        this._devices = value;
    }

    get documents(): any {
        return this._documents ? this._documents : (this._documents = this.obj ? this.obj.documents : []);
    }
    set documents(value: any) {
        this._documents = value;
    }

    get treeComponentProperty(): any {
        return this._treeComponentProperty ? this._treeComponentProperty : (this._treeComponentProperty = this.obj ? this.obj.treeComponentProperty : []);
    }
    set treeComponentProperty(value: any) {
        this._treeComponentProperty = value;
    }

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            code: this.code,
            name: this.name,
            parentLocationId: this.parentLocationId,
            Locations: this.locations,
            Devices: this.devices,
            documents: this.documents,
            TreeComponentProperty: this.treeComponentProperty
        };
    }
}
