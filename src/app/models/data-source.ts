import { BaseModel } from './base-model';
import { DataSourceType } from './data-source-type';

export class DataSource extends BaseModel {
    private _description: string;
    private _connectionString: string;
    private _userId: string;
    private _pwd: string;
    private _dataSourceType: DataSourceType;

    constructor(protected obj?: any) {
       super(obj);
    }

    get description(): string {
        return this._description ? this._description : (this._description = this.obj.description || '');
    }
    set description(value: string) {
        this._description = value;
    } 
    
    get connectionString(): string {
        return this._connectionString ? this._connectionString : (this._connectionString = this.obj.connectionString || '');
    }
    set connectionString(value: string) {
        this._connectionString = value;
    }

    get userId(): string {
        return this._userId ? this._userId : (this._userId = this.obj.userId || '');
    }

    set userId(value: string) {
        this._userId = value;
    }

    get pwd(): string {
        return this._pwd ? this._pwd : (this._pwd = this.obj.pwd || '');
    }
    set pwd(value: string) {
        this._pwd = value;
    }

    get dataSourceType(): DataSourceType {
        return this._dataSourceType ? this._dataSourceType : (this._dataSourceType = this.obj.dataSourceType || 0);
    }
    set dataSourceType(value: DataSourceType) {
        this._dataSourceType = value;
    } 

    public toJSON() {
        return {
            id: this.id,
            created: this.created,
            description: this.description,
            connectionString: this.connectionString,
            userId: this.userId,
            pwd: this.pwd,
            dataSourceType: this.dataSourceType
        };
    }
}