import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule, MatSidenavModule, MatInputModule, MatSelectModule, MatTabsModule, MatGridListModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatDatepickerIntl, MatChipsModule, MatExpansionModule } from '@angular/material';
import { MatToolbarModule, MatIconModule, MatCardModule, MatFormFieldModule } from '@angular/material';


@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatCheckboxModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTabsModule,
        MatGridListModule,
        MatDialogModule,
        MatDatepickerModule,
        MatChipsModule,
        MatExpansionModule

    ],
    exports: [
        MatButtonModule,
        MatCheckboxModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTabsModule,
        MatGridListModule,
        MatDialogModule,
        MatDatepickerModule,
        MatChipsModule,
        MatExpansionModule
    ]
})
export class AppMaterialModule { }
