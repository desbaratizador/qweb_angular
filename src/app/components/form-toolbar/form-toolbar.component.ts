import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-form-toolbar',
    templateUrl: 'form-toolbar.component.html',
    styleUrls: ['form-toolbar.component.scss']
})
export class FormToolbarComponent {
    @Output() backToList: EventEmitter<any> = new EventEmitter();
    @Output() add: EventEmitter<any> = new EventEmitter();
    @Output() save: EventEmitter<any> = new EventEmitter();

    backToListFromParent() {
        this.backToList.emit(null);
    }

    addFromParent() {
        this.add.emit(null);
    }

    saveFromParent() {
        this.save.emit(null);
    }
}
