import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TreeService } from '../../services/tree.service';
import { ITreeOptions, TreeNode, TreeModel, TREE_ACTIONS } from 'angular-tree-component';
import appConstants from 'src/app/code/app-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {
  contextMenu: { node: TreeNode, x: number, y: number } = null;
  private constants = appConstants;
  private nodeTypes = appConstants.NODE_TYPE;
  options: ITreeOptions = {
    useCheckbox: true,
    useVirtualScroll: true,
    actionMapping: {
      mouse: {
        contextMenu: (treeModel: TreeModel, treeNode: TreeNode, e: MouseEvent) => {
          e.preventDefault();
          if (this.contextMenu && treeNode === this.contextMenu.node) {
            return this.closeMenu();
          }
          this.contextMenu = {
            node: treeNode,
            x: e.pageX,
            y: e.pageY - 50
          };
        },
        click: (treeModel: TreeModel, treeNode: TreeNode, e: MouseEvent) => {
          this.closeMenu();
          TREE_ACTIONS.TOGGLE_ACTIVE(treeModel, treeNode, e);
        }
      }
    },
  };
  nodes = [];
  treeStructure = [];
  sourceNode: TreeNode = null;
  editNode: TreeNode = null;
  doCut = false;

  @Output() nodeToggle: EventEmitter<any> = new EventEmitter();
  constructor(
    private treeService: TreeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.treeService.getStructure().subscribe(_ => {
      this._buildtree(_);
    });
  }
  _buildtree(treeData: any) {
    this.nodes = treeData;
  }
  closeMenu = () => {
    this.contextMenu = null;
  }

  handleNodeSelection(event) {
    if (event.node.data.treeComponentType !== appConstants.NODE_TYPE.DEVICE) {
      return;
    }
    this.nodeToggle.emit({ device: event.node.data, selected: true });
  }

  handleNodeDeselection(event) {
    if (event.node.data.treeComponentType !== appConstants.NODE_TYPE.DEVICE) {
      return;
    }
    this.nodeToggle.emit({ device: event.node.data, selected: false });
  }

  /* ------ */
  canPaste = () => {
    if (!this.sourceNode) {
      return false;
    }
    return this.sourceNode.treeModel.canMoveNode(this.sourceNode, { parent: this.contextMenu.node, index: 0 });
  }
  edit = () => {
    this.editNode = this.contextMenu.node;
    this.closeMenu();
  }

  stopEdit = () => {
    this.editNode = null;
  }

  copy = () => {
    this.sourceNode = this.contextMenu.node;
    this.doCut = false;
    this.closeMenu();
  }

  cut = () => {
    this.sourceNode = this.contextMenu.node;
    this.doCut = true;
    this.closeMenu();
  }

  paste = () => {
    if (!this.canPaste()) {
      return;
    }
    this.doCut
      ? this.sourceNode.treeModel.moveNode(this.sourceNode, { parent: this.contextMenu.node, index: 0 })
      : this.sourceNode.treeModel.copyNode(this.sourceNode, { parent: this.contextMenu.node, index: 0 });

    this.sourceNode = null;
    this.closeMenu();
  }

  filterFn(value: string, treeModel: TreeModel) {
    treeModel.filterNodes((node: TreeNode) => {
      return node.data.name.toLowerCase().indexOf(value.toLowerCase()) !== 1;
    } /* fuzzysearch(value, node.data.name) */);
  }
  contextMenuOptionSelected() {
    this.contextMenu.node.setIsActive(true);
    this.closeMenu();
  }
}
function fuzzysearch(needle: string, haystack: string) {
  const haystackLC = haystack.toLowerCase();
  const needleLC = needle.toLowerCase();

  const hlen = haystack.length;
  const nlen = needleLC.length;

  if (nlen > hlen) {
    return false;
  }
  if (nlen === hlen) {
    return needleLC === haystackLC;
  }
  /*   outer: for (let i = 0, j = 0; i < nlen; i++) {
      const nch = needleLC.charCodeAt(i);
      while (j < hlen) {
        if (haystackLC.charCodeAt(j++) === nch) {
          continue outer;
        }
      }
      return false;
    } */
  if (haystackLC.indexOf(needleLC) === -1) { return false; }
  return true;
}
