import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { TimeEditor } from './time-editor/time-editor';
import * as moment from 'moment';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, Validator, NG_VALIDATORS, FormControl } from '@angular/forms';


@Component({
  selector: 'app-date-time-editor',
  templateUrl: './date-time-editor.component.html',
  styleUrls: ['./date-time-editor.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateTimeEditorComponent),
    multi: true
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => DateTimeEditorComponent),
    multi: true,
  }]
})

export class DateTimeEditorComponent implements OnInit, ControlValueAccessor, Validator {

  private date: any;
  private time: TimeEditor;
  @Input() datePlaceholder = 'date';
  @Input() timePlaceholder = 'time';
  @Input() showMilliseconds = false;
  @Output() valueChange = new EventEmitter();
  onChange: any = () => { };
  onTouched: any = () => { };
  onValidatorChange: any = () => { };
  @Input()
  get value(): any | null {


    if (!!this.date && !!this.time) {
      return moment().year(this.date.year()).month(this.date.month()).date(this.date.date()).hours(this.time.hours).minutes(this.time.minutes).seconds(this.time.seconds).milliseconds(this.time.milliseconds);
    }

    if (!!this.date) {
      return moment().year(this.date.year()).month(this.date.month()).date(this.date.date());
    }

    return null;
  }

  set value(date: any | null) {
    const mDate = moment(date);

    this.date = mDate;
    this.time = new TimeEditor(mDate.hours(), mDate.minutes(), mDate.seconds(), mDate.milliseconds());
    this.onChange(mDate);
    this.onTouched();
  }

  constructor() {
  }

  ngOnInit() { }

  dateChange(datevalue) {
    this.date = moment(datevalue.value);
    this.valueChange.emit(this.value);
  }

  timeChanged(timevalue) {
    this.time = timevalue;
    this.valueChange.emit(this.value);
  }

  writeValue(obj: any): void {
    if (obj) {
      this.value = obj;
    }
  }
  registerOnChange(fn: any): void {
    this.onChange(fn);
  }
  registerOnTouched(fn: any): void {
    this.onTouched(fn);
  }
  setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }

  registerOnValidatorChange?(fn: () => void): void {
    this.onValidatorChange(fn);
  }

  public validate(c: FormControl) {
    return !!this.value ? null : {
      datetimeError: {
        valid: false
      }
    };

  }
}
