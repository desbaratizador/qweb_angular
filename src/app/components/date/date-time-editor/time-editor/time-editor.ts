export class TimeEditor {
    constructor(public hours: number, public minutes: number, public seconds: number, public milliseconds: number) { }
  }