import { Component, OnInit, OnDestroy, HostBinding, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { MatFormFieldControl } from '@angular/material';
import { TimeEditor } from './time-editor';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { FocusMonitor } from '@angular/cdk/a11y';


@Component({
  selector: 'app-time-editor',
  templateUrl: './time-editor.component.html',
  styleUrls: ['./time-editor.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: TimeEditorComponent }]
})


export class TimeEditorComponent implements MatFormFieldControl<TimeEditor>, OnDestroy, OnInit {
  static nextId = 0;

  @Input() showMilliseconds = false;
  parts: FormGroup;
  stateChanges = new Subject<void>();
  focused = false;
  ngControl = null;
  errorState = false;
  controlType = 'app-time-editor';
  @HostBinding('id') id = `time-editor-${TimeEditorComponent.nextId++}`;

  @HostBinding('attr.aria-describedby') describedBy = '';

  @HostBinding('class.time-editor')
  get shouldLabelFloat() { return true/* this.focused || !this.empty */; }

  get empty() {
    const { value: { hours, minutes, seconds, milliseconds } } = this.parts;

    return this.showMilliseconds ? !hours && !minutes && !seconds && !milliseconds : !hours && !minutes && !seconds;
  }
  @Output() valueChange = new EventEmitter();
  @Input()
  get placeholder(): string { return this._placeholder; }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }
  private _placeholder: string;

  @Input()
  get required(): boolean { return this._required; }
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }
  private _required = false;

  @Input()
  get disabled(): boolean { return this._disabled; }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }
  private _disabled = false;

  @Input()
  get value(): TimeEditor | null {
    const { value: { hours, minutes, seconds, milliseconds } } = this.parts;
    if ((hours >= 0 && hours <= 23) && (minutes >= 0 && minutes <= 59) && (seconds >= 0 && seconds <= 59) && (milliseconds >= 0 && milliseconds <= 999)) {
      return new TimeEditor(hours, minutes, seconds, milliseconds);
    }
    return null;
  }

  set value(time: TimeEditor | null) {
    const { hours, minutes, seconds, milliseconds } = time || new TimeEditor(0, 0, 0, 0);
    this.parts.setValue({ hours, minutes, seconds, milliseconds });
    this.stateChanges.next();
  }

  constructor(fb: FormBuilder, private fm: FocusMonitor, private elRef: ElementRef<HTMLElement>) {
    this.parts = fb.group({
      hours: 0,
      minutes: 0,
      seconds: 0,
      milliseconds: 0
    });

    fm.monitor(elRef, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef);
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      if (this.elRef.nativeElement.querySelector('input')) {
        this.elRef.nativeElement.querySelector('input').focus();
      }
    }
  }

  ngOnInit() {
  }

  hoursChanged() {
    this.valueChange.emit(this.value);
  }
  minutesChanged() {
    this.valueChange.emit(this.value);
  }
  secondsChanged() {
    this.valueChange.emit(this.value);
  }
  millisecondsChanged() {
    this.valueChange.emit(this.value);
  }
}
