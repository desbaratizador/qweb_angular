import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DatasourceIdModalComponent } from './datasource-id-modal/datasource-id-modal.component';


@Component({
    selector: 'app-datasource-id',
    templateUrl: './datasource-id.component.html',
    styleUrls: ['./datasource-id.component.scss']
})
export class DatasourceIdComponent implements OnInit {
    @Input() data: any;

    constructor(
        private dialog: MatDialog
    ) { }

    ngOnInit() {
    }
    openDatasourceSearch() {
        const dialogRef = this.dialog.open(DatasourceIdModalComponent, {
            width: '20vw',
            height: '40vh',
            data: this.data
        });
    }
}
