import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasourceIdModalComponent } from './datasource-id-modal.component';

describe('DatasourceIdModalComponent', () => {
  let component: DatasourceIdModalComponent;
  let fixture: ComponentFixture<DatasourceIdModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasourceIdModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasourceIdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
