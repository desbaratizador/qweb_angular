import { Component, OnInit, Output, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EventEmitter } from 'events';
import { DataSource } from 'src/app/models/data-source';
import { DatasourceService } from 'src/app/services/datasource.service';
import { Device } from 'src/app/models/device';

@Component({
    selector: 'app-datasource-id-modal',
    templateUrl: './datasource-id-modal.component.html',
    styleUrls: ['./datasource-id-modal.component.scss']
})
export class DatasourceIdModalComponent implements OnInit {

    private searchCriteria: string;
    private sources: Device[];
    private selectedValue;
    constructor(
        public dialogRef: MatDialogRef<DatasourceIdModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private datasourceService: DatasourceService
    ) { }

    ngOnInit() {
    }
    cancel() {
        this.dialogRef.close();
    }
    acceptChanges() {
        this.data.deviceDataSourceId = this.selectedValue;
    }
    find() {
        console.log(this.searchCriteria);
        this.datasourceService.findDevice(this.searchCriteria, this.data.dataSource).subscribe((result: any) => {
            this.sources = result;
        });
    }

    selectSource(evt, sourceId) {
        const listItems = Array.from(document.getElementsByClassName('list-item'));

        listItems.forEach((item, idx) => item.classList.remove('active'));
        evt.currentTarget.classList.add('active');
        this.selectedValue = parseInt(sourceId, 10);
    }
}
