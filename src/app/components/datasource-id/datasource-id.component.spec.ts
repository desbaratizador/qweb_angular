import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasourceIdComponent } from './datasource-id.component';

describe('DatasourceIdComponent', () => {
  let component: DatasourceIdComponent;
  let fixture: ComponentFixture<DatasourceIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasourceIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasourceIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
