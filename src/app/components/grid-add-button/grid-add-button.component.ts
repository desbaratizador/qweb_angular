import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-grid-add-button',
    templateUrl: './grid-add-button.component.html',
    styleUrls: ['./grid-add-button.component.scss']
})
export class GridAddButtonComponent {

    @Output() click = new EventEmitter();

    clicked() {
       // this.click.emit();
    }
}
