import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormToolbarComponent } from './form-toolbar/form-toolbar.component';
import { GridAddButtonComponent } from './grid-add-button/grid-add-button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FormToolbarComponent,
    GridAddButtonComponent
  ],
  exports: [
    FormToolbarComponent,
    GridAddButtonComponent
  ]
})
export class AppComponentsModule { }
