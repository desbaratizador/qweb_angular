import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LimitsDefinitionService } from 'src/app/services/limits-definition.service';
import { LimitsDefinition } from 'src/app/models/limits-definition';
import appConstants from 'src/app/code/app-constants';
import { forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { LimitsEditModalComponent } from './modal/limits-edit-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-limits-edit',
  templateUrl: './limits-edit.component.html',
  styleUrls: ['./limits-edit.component.scss']
})

export class LimitsEditComponent implements OnInit {

  private limitsDefinition: LimitsDefinition;
  private translations: any;

  public fgLimitsDefinition: FormGroup;

  gridDatasource: any[];

  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'variable' },
    { field: 'tensionLevel' },
    { field: 'min100' },
    { field: 'max100' },
    { field: 'min95' },
    { field: 'max95' },
    { field: 'min99' },
    { field: 'max99' },
    { field: 'min995' },
    { field: 'max995' }
  ];

  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.openRecordModal(row);
    }

  }];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private limitsService: LimitsDefinitionService,
    private formBuilder: FormBuilder,
    private appTranslate: AppTranslationService,
    private dialog: MatDialog
  ) {
    appTranslate.translateMultiple([
      'name',
      'type',
      'level',
      'minimum',
      'maximum',
      'percentil5',
      'percentil95',
      'percentil1',
      'percentil99',
      'percentil05',
      'percentil995',
      'edit'
    ]).subscribe((t: any) => {
      this.translations = t;
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
  }

  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.type;
    this.columns[1].template = (v) => v.name;
    this.columns[2].title = t.level;
    this.columns[2].template = (tl) => tl.description;
    this.columns[3].title = t.minimum;
    this.columns[4].title = t.maximum;
    this.columns[5].title = t.percentil5;
    this.columns[6].title = t.percentil95;
    this.columns[7].title = t.percentil1;
    this.columns[8].title = t.percentil99;
    this.columns[9].title = t.percentil05;
    this.columns[10].title = t.percentil995;
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.limitsService.get(id).subscribe((data: any) => {
      this.limitsDefinition = data;
      this.gridDatasource = this.limitsDefinition.limits;
      this.fgLimitsDefinition = this.formBuilder.group({
        'description': [this.limitsDefinition.description, Validators.compose([Validators.required])]
      });
    });
  }

  backToList() {
    this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_LIMITS]);
  }

  save() {
    /*para aparecerem os erros caso os campos não tenham sido preenchidos */
    // this.inputControls.firstname.markAsTouched();

    /* */

    if (!this.fgLimitsDefinition.valid) { return; }

    Object.assign(this.limitsDefinition, this.fgLimitsDefinition.value);
    this.limitsService.save(this.limitsDefinition).subscribe((_: any) => {
      this.backToList();
    });

  }

  add() {
    this.limitsDefinition = new LimitsDefinition();
    this.fgLimitsDefinition.reset();
  }

  get inputControls() {
    return this.fgLimitsDefinition.controls;
  }

  openRecordModal(data) {
    const dialogRef = this.dialog.open(LimitsEditModalComponent, {
        width: '80vw',
        height: '50vh',
        data: data
    });
  }
}
