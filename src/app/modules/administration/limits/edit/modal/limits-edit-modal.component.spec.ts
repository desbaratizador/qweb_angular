import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LimitsEditModalComponent } from './limits-edit-modal.component';

describe('LimitsEditModalComponent', () => {
  let component: LimitsEditModalComponent;
  let fixture: ComponentFixture<LimitsEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LimitsEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LimitsEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
