import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Limits } from 'src/app/models/limits';
import { Description } from 'src/app/models/description';
import { Variable } from 'src/app/models/variable';
import { TensionLevelService } from 'src/app/services/tension-level.service';
import { VariableService } from 'src/app/services/variable.service';
import { compareOption } from 'src/app/code/utils';

@Component({
    selector: 'app-limits-edit-modal',
    templateUrl: './limits-edit-modal.component.html',
    styleUrls: ['./limits-edit-modal.component.scss']
})

export class LimitsEditModalComponent implements OnInit, OnDestroy {
    private compareOption = compareOption;
    private variablesSubscription: Subscription;
    private tensionsSubscription: Subscription;
    private variables: Variable[];
    private tensions: Description[];
    private fgLimits: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<LimitsEditModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Limits,
        private formBuilder: FormBuilder,
        private tensionLevelService: TensionLevelService,
        private variableService: VariableService
    ) {
        this.variablesSubscription = variableService.getVariables().subscribe((variables: Variable[]) => this.variables = variables);

        this.tensionsSubscription = tensionLevelService.getList().subscribe((tensions: Description[]) => this.tensions = tensions);
    }

    ngOnInit() {
        this.fgLimits = this.formBuilder.group({
            'variable': [{value: this.data.variable, disabled: true}, Validators.compose([Validators.required])],
            'tensionLevel': [{value: this.data.tensionLevel, disabled: true}, Validators.compose([Validators.required])],
            'min100': [this.data.min100, Validators.compose([Validators.required])],
            'max100': [this.data.max100, Validators.compose([Validators.required])],
            'min95': [this.data.min95, Validators.compose([Validators.required])],
            'max95': [this.data.max95, Validators.compose([Validators.required])],
            'min99': [this.data.min99, Validators.compose([Validators.required])],
            'max99': [this.data.max99, Validators.compose([Validators.required])],
            'min995': [this.data.min995, Validators.compose([Validators.required])],
            'max995': [this.data.max995, Validators.compose([Validators.required])]
        });
    }

    ngOnDestroy(): void {
        this.variablesSubscription.unsubscribe();
        this.tensionsSubscription.unsubscribe();
    }

    cancel() {
        this.dialogRef.close();
    }

    acceptChanges() {
        if (!this.fgLimits.valid) { return; }

        Object.assign(this.data, this.fgLimits.value);
        this.dialogRef.close(this.data);
    }

    get inputControls() {
        return this.fgLimits.controls;
    }
}
