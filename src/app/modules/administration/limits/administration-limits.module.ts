import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiGridModule } from '@ng-plus/grid';

import { LimitsListComponent } from './list/limits-list.component';
import { LimitsEditComponent } from './edit/limits-edit.component';
import { AppMaterialModule } from 'src/app/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponentsModule } from 'src/app/components/app-components.module';
import { LimitsEditModalComponent } from './edit/modal/limits-edit-modal.component';


@NgModule({
  imports: [
    CommonModule,
    TiGridModule,
    AppMaterialModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AppComponentsModule
  ],
  declarations: [
    LimitsListComponent,
    LimitsEditComponent,
    LimitsEditModalComponent
  ],
  entryComponents: [
    LimitsListComponent,
    LimitsEditComponent,
    LimitsEditModalComponent
  ]
})
export class AdministrationLimitsModule { }
