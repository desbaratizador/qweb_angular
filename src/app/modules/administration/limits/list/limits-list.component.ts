import { Component, OnInit } from '@angular/core';
import { LimitsDefinitionService } from 'src/app/services/limits-definition.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';

@Component({
  selector: 'app-limits-list',
  templateUrl: './limits-list.component.html',
  styleUrls: ['./limits-list.component.scss']
})

export class LimitsListComponent implements OnInit {
  gridDatasource: any[];
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'description' }
  ];

  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_LIMITS, row.id]);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }
  }];

  constructor(private limitsService: LimitsDefinitionService, private appTranslate: AppTranslationService, private router: Router) {
    appTranslate.translateMultiple([
      'description',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }

  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.description;
  }

  ngOnInit() {
    this.limitsService.getList().subscribe((limitsDefinitionList: any[]) => {
      this.gridDatasource = limitsDefinitionList;
    });
  }
}
