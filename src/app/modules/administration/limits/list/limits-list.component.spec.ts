import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LimitsListComponent } from './limits-list.component';

describe('UserListComponent', () => {
  let component: LimitsListComponent;
  let fixture: ComponentFixture<LimitsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LimitsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LimitsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});