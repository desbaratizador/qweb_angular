import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';
import { forkJoin } from 'rxjs';
import { ProfileService } from 'src/app/services/profile.service';
import { Profile } from 'src/app/models/profile';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { compareOption } from 'src/app/code/utils';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})

export class ProfileEditComponent implements OnInit {
  private compareOption = compareOption;
  private profile: Profile;
  private translations: any;
  
  public fgProfile: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private appTranslate: AppTranslationService
  ) {
    appTranslate.translateMultiple([
      'new',
      'userNameLabel'
    ]).subscribe((t: any) => this.translations = t);

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    forkJoin([this.profileService.get(id)])
      .subscribe((resp: any) => {
        this.profile = resp[0];
        this.fgProfile = this.formBuilder.group({
          'name': [this.profile.name, Validators.compose([Validators.required])]
        });

      });
  }

  backToList() {
    this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_PROFILES]);
  }

  save() {
    /*para aparecerem os erros caso os campos não tenham sido preenchidos */
    this.inputControls.name.markAsTouched();
    
    /* */

    if (!this.fgProfile.valid) { return; }

    Object.assign(this.profile, this.fgProfile.value);
    this.profileService.save(this.profile).subscribe((_: any) => {
      this.backToList();
    });

  }

  add() {
    this.profile = new Profile();
    this.fgProfile.reset();
  }

  get inputControls() {
    return this.fgProfile.controls;
  }
}
