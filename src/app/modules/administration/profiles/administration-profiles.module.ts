import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiGridModule } from '@ng-plus/grid';

import { ProfileListComponent } from './list/profile-list.component';
import { ProfileEditComponent } from './edit/profile-edit.component';
import { AppMaterialModule } from 'src/app/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponentsModule } from 'src/app/components/app-components.module';

@NgModule({
  imports: [
    CommonModule,
    TiGridModule,
    AppMaterialModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AppComponentsModule
  ],
  declarations: [
    ProfileListComponent,
    ProfileEditComponent
  ],
  entryComponents: [
    ProfileListComponent,
    ProfileEditComponent
  ]
})
export class AdministrationProfilesModule { }
