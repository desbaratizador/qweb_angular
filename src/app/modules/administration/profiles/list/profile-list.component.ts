import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})

export class ProfileListComponent implements OnInit {
  gridDatasource: any[];
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'name' }
  ];

  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_PROFILES, row.id]);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }
  }];

  constructor(private profileService: ProfileService, private appTranslate: AppTranslationService, private router: Router) {
    appTranslate.translateMultiple([
      'name',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }
  
  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.name;
  }

  ngOnInit() {
    this.profileService.getList().subscribe((profileList: any[]) => {
      this.gridDatasource = profileList;
    });
  }
}