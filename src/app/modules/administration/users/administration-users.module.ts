import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiGridModule } from '@ng-plus/grid';

import { UserListComponent } from './list/user-list.component';
import { UserEditComponent } from './edit/user-edit.component';
import { AppMaterialModule } from 'src/app/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponentsModule } from 'src/app/components/app-components.module';


@NgModule({
  imports: [
    CommonModule,
    TiGridModule,
    AppMaterialModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AppComponentsModule
  ],
  declarations: [
    UserListComponent,
    UserEditComponent
  ],
  entryComponents: [
    UserListComponent,
    UserEditComponent
  ]
})
export class AdministrationUsersModule { }
