import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  gridDatasource: any[];
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'login' },
    { field: 'firstname' },
    { field: 'lastname' },
    { field: 'language', align: 'center' },
    { title: 'Email', field: 'email' },
    { field: 'departement' },
    { field: 'notify', align: 'center' }
  ];

  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_USERS, row.id]);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }
  }];

  constructor(private userService: UserService, private appTranslate: AppTranslationService, private router: Router) {
    appTranslate.translateMultiple([
      'userNameLabel',
      'firstNameUser',
      'lastNameUser',
      'language',
      'emailUser',
      'departmentUser',
      'notify',
      'yes',
      'no',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }

  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.userNameLabel;
    this.columns[2].title = t.firstNameUser;
    this.columns[3].title = t.lastNameUser;
    this.columns[4].title = t.language;
    this.columns[4].template = (v) => `<img class="language-flag" src="../assets/flags/${v.toLowerCase()}.png"/>`;
    this.columns[5].title = t.emailUser;
    this.columns[6].title = t.departmentUser;
    this.columns[7].title = t.notify;
    this.columns[7].template = (v) => v ? t.yes : t.no;
  }

  ngOnInit() {
    this.userService.getList().subscribe((userList: any[]) => {
      this.gridDatasource = userList;
    });
  }

}
