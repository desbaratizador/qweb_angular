import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import appConstants from 'src/app/code/app-constants';
import { ProfileService } from 'src/app/services/profile.service';
import { Profile } from 'src/app/models/profile';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { compareOption } from 'src/app/code/utils';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})

export class UserEditComponent implements OnInit {
  private compareOption = compareOption;
  private user: User;
  private translations: any;
  private profiles: Profile[];

  public fgUser: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private appTranslate: AppTranslationService
  ) {
    appTranslate.translateMultiple([
      'new',
      'userNameLabel'
    ]).subscribe((t: any) => this.translations = t);

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    forkJoin([this.userService.get(id), this.profileService.getList()])
      .subscribe((resp: any) => {
        this.user = resp[0];
        this.profiles = resp[1];
        this.fgUser = this.formBuilder.group({
          'firstname': [this.user.firstname, Validators.compose([Validators.required])],
          'lastname': [this.user.lastname, Validators.compose([Validators.required])],
          'language': [this.user.language, Validators.compose([Validators.required])],
          'email': [this.user.email],
          'department': [this.user.department],
          'profile': [this.user.profile, Validators.compose([Validators.required])],
          'notify': [this.user.notify],
          'login': [this.user.login, Validators.compose([Validators.required])],
          'pwd': [this.user.pwd, Validators.compose([Validators.required])]
        });

      });
  }

  backToList() {
    this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_USERS]);
  }

  save() {
    /*para aparecerem os erros caso os campos não tenham sido preenchidos */
    this.inputControls.firstname.markAsTouched();
    this.inputControls.lastname.markAsTouched();
    this.inputControls.language.markAsTouched();
    this.inputControls.profile.markAsTouched();
    this.inputControls.login.markAsTouched();
    this.inputControls.pwd.markAsTouched();
    /* */

    if (!this.fgUser.valid) { return; }

    Object.assign(this.user, this.fgUser.value);
    this.userService.save(this.user).subscribe((_: any) => {
      this.backToList();
    });

  }

  add() {
    this.user = new User({ firstname: this.translations.new, lastname: this.translations.userNameLabel });
    this.fgUser.reset();
  }

  get inputControls() {
    return this.fgUser.controls;
  }
}
