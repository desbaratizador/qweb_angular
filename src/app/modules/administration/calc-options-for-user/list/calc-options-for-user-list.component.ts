import { Component, OnInit } from '@angular/core';
import { CalcOptionsForUserService } from 'src/app/services/calc-options-for-user.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';

@Component({
	selector: 'app-user-list',
	templateUrl: './calc-options-for-user-list.component.html',
	styleUrls: ['./calc-options-for-user-list.component.scss']
})
export class CalcOptionsForUserListComponent implements OnInit {
	gridDatasource: any[];
	columns: any[] = [
		{ title: 'Id', field: 'id' },
		{ field: 'description' },
		{ field: 'language' }
	];


	rowActions = [{
		title: '',
		classes: '',
		icon: 'fal fa-edit',
		onClick: (row) => {
			this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_REPORT_OPTIONS, row.id]);
		}
	}, {
		title: '',
		classes: 'text-danger',
		icon: 'fal fa-trash',
		onClick: (row) => {
			row.isDeleted = true;
		}
	}
	];

	constructor(private calcOptionsForUserService: CalcOptionsForUserService, private appTranslate: AppTranslationService, private router: Router) {
		appTranslate.translateMultiple([
			'description',
			'language',
			'deleteRecord',
			'edit'
		]).subscribe((t: any) => {
			this.setGridColumnsOptions(t);
			this.setGridActionsOptions(t);
		});
	}

	setGridActionsOptions(t: any) {
		this.rowActions[0].title = t.edit;
		this.rowActions[1].title = t.deleteRecord;
	}
	setGridColumnsOptions(t: any) {
		this.columns[1].title = t.description;
		this.columns[2].title = t.language;
	}

	ngOnInit() {
		this.calcOptionsForUserService.getList().subscribe((calcOptionsForUserList: any[]) => {
			this.gridDatasource = calcOptionsForUserList;
		});
	}
}
