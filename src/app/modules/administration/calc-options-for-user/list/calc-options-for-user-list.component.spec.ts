import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcOptionsForUserListComponent } from './calc-options-for-user-list.component';

describe('CalcOptionsForUserListComponent', () => {
  let component: CalcOptionsForUserListComponent;
  let fixture: ComponentFixture<CalcOptionsForUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcOptionsForUserListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcOptionsForUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
