import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiGridModule } from '@ng-plus/grid';

import { CalcOptionsForUserListComponent } from './list/calc-options-for-user-list.component';
import { CalcOptionsForUserEditComponent } from './edit/calc-options-for-user-edit.component';
import { AppMaterialModule } from 'src/app/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponentsModule } from 'src/app/components/app-components.module';


@NgModule({
  imports: [
    CommonModule,
    TiGridModule,
    AppMaterialModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AppComponentsModule
  ],
  declarations: [
    CalcOptionsForUserListComponent,
    CalcOptionsForUserEditComponent
  ],
  entryComponents: [
    CalcOptionsForUserListComponent,
    CalcOptionsForUserEditComponent
  ]
})
export class AdministrationCalcOptionsForUserModule { }
