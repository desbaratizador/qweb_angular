import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CalcOptionsForUserService } from 'src/app/services/calc-options-for-user.service';
import { DescriptionList } from 'src/app/models/description-list';
import { DescriptionListService } from 'src/app/services/description-list.service';
import { CalcOptionsForUser } from 'src/app/models/calc-options-for-user';
import appConstants from 'src/app/code/app-constants';
import { forkJoin } from 'rxjs';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { compareOption } from 'src/app/code/utils';

@Component({
  selector: 'app-user-edit',
  templateUrl: './calc-options-for-user-edit.component.html',
  styleUrls: ['./calc-options-for-user-edit.component.scss']
})
export class CalcOptionsForUserEditComponent implements OnInit {
  private calcOptionsForUser: CalcOptionsForUser;

  public fgCalcOptionsForUser: FormGroup;

  private compareOption = compareOption;

  private excelExportTypes: DescriptionList[];
  private languages: DescriptionList[];

  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private calcOptionsForUserService: CalcOptionsForUserService,
    private descriptionListService: DescriptionListService,
    private formBuilder: FormBuilder
  ) { }


  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    forkJoin([this.calcOptionsForUserService.get(id), this.descriptionListService.getDataFromServer()])
      .subscribe((resp: any) => {
        this.calcOptionsForUser = resp[0];
        this.excelExportTypes = resp[1].filter((dl: DescriptionList) => dl.context === appConstants.DESCRIPTION_LIST_CONTEXT.EXCEL_EXPORT_TYPE);
        this.languages = resp[1].filter((dl: DescriptionList) => dl.context === 'LANGUAGE');
        this.fgCalcOptionsForUser = this.formBuilder.group({
          'description': [this.calcOptionsForUser.description, Validators.compose([Validators.required])],
          'excelExportType': [this.calcOptionsForUser.excelExportType, Validators.compose([Validators.required])],
          'language': [this.calcOptionsForUser.language, Validators.compose([Validators.required])],
          'harmonicsFirst': [this.calcOptionsForUser.harmonicsFirst, Validators.compose([Validators.required])],
          'harmonicsSecond': [this.calcOptionsForUser.harmonicsSecond, Validators.compose([Validators.required])],
          'harmonicsThird': [this.calcOptionsForUser.harmonicsThird, Validators.compose([Validators.required])],
          'selMostPositiveWeek': [this.calcOptionsForUser.selMostPositiveWeek],
          'selRepresentativeWeek': [this.calcOptionsForUser.selRepresentativeWeek],
          'selMostNegativeWeek': [this.calcOptionsForUser.selMostNegativeWeek]
        });
      });
  }

  backToList() {
    this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_REPORT_OPTIONS]);
  }

  save() {

    if (!this.fgCalcOptionsForUser.valid) { return; }

    Object.assign(this.calcOptionsForUser, this.fgCalcOptionsForUser.value);
    this.calcOptionsForUserService.save(this.calcOptionsForUser).subscribe((_: any) => {
      this.backToList();
    });

  }

  add() {
    this.calcOptionsForUser = new CalcOptionsForUser();
    this.fgCalcOptionsForUser.reset();
  }

  get inputControls() {
    return this.fgCalcOptionsForUser.controls;
  }

}
