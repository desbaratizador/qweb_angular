import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcOptionsForUserEditComponent } from './calc-options-for-user-edit.component';

describe('CalcOptionsForUserEditComponent', () => {
  let component: CalcOptionsForUserEditComponent;
  let fixture: ComponentFixture<CalcOptionsForUserEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcOptionsForUserEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcOptionsForUserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
