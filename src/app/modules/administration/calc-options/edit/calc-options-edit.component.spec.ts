import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcOptionsEditComponent } from './calc-options-edit.component';

describe('CalcOptionsEditComponent', () => {
  let component: CalcOptionsEditComponent;
  let fixture: ComponentFixture<CalcOptionsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcOptionsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcOptionsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});