import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';
import { forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalcOptions } from 'src/app/models/calc-options';
import { Description } from 'src/app/models/description';
import { AggrEventMethod } from 'src/app/models/aggr-event-method';
import { CalcOptionsService } from 'src/app/services/calc-options.service';
import { AggrEventsRefDataService } from 'src/app/services/aggr-events-refdata.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { compareOption } from 'src/app/code/utils';

@Component({
	selector: 'calc-options-edit',
	templateUrl: './calc-options-edit.component.html',
	styleUrls: ['./calc-options-edit.component.scss']
})

export class CalcOptionsEditComponent implements OnInit {
	private compareOption = compareOption;
	private calcOptions: CalcOptions;
	private currentMacroMethod: Description;
	private macroMethods: Description[];
	private methods: AggrEventMethod[];
	private selectableMethods: AggrEventMethod[];
	private durations: Description[];
	private profounds: Description[];
	private translations: any;

	public fgCalcOptions: FormGroup;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private calcOptionsService: CalcOptionsService,
		private aggrEventsRefDataService: AggrEventsRefDataService,
		private formBuilder: FormBuilder,
		private appTranslate: AppTranslationService
	) {
		appTranslate.translateMultiple([
			'new',
			'userNameLabel'
		]).subscribe((t: any) => this.translations = t);

	}

	ngOnInit() {
		const id = this.route.snapshot.paramMap.get('id');

		forkJoin([this.calcOptionsService.get(id), this.aggrEventsRefDataService.getMacroMethods(),
			this.aggrEventsRefDataService.getMethods(), this.aggrEventsRefDataService.getDurations(), this.aggrEventsRefDataService.getProfounds()])
			.subscribe((resp: any) => {
				this.calcOptions = resp[0];
				this.macroMethods = resp[1];
				this.methods = resp[2];
				this.durations = resp[3];
				this.profounds = resp[4];

				this.currentMacroMethod = new Description();
				this.currentMacroMethod.id = this.calcOptions.aggrEventMethod.aggrEventMacroMethodId;
				this.filterMethods(this.currentMacroMethod.id);

				this.fgCalcOptions = this.formBuilder.group({
					'description': [this.calcOptions.description, Validators.compose([Validators.required])],
					'aggrEventMacroMethod': [this.currentMacroMethod, Validators.compose([Validators.required])],
					'aggrEventMethod': [this.calcOptions.aggrEventMethod, Validators.compose([Validators.required])],
					'aggregationTime': [this.calcOptions.aggregationTime, Validators.compose([Validators.required])],
					'aggrEventDuration': [this.calcOptions.aggrEventDuration, Validators.compose([Validators.required])],
					'aggrEventProfound': [this.calcOptions.aggrEventProfound, Validators.compose([Validators.required])],
					'aggrLimitMinimumDipTension': [this.calcOptions.aggrLimitMinimumDipTension, Validators.compose([Validators.required])],
					'aggrLimitMaximumDipTension': [this.calcOptions.aggrLimitMaximumDipTension, Validators.compose([Validators.required])],
					'aggrLimitMinimumDipTime': [this.calcOptions.aggrLimitMinimumDipTime, Validators.compose([Validators.required])],
					'aggrLimitMaximumDipTime': [this.calcOptions.aggrLimitMaximumDipTime, Validators.compose([Validators.required])],
					'aggrLimitMinimumSwellTension': [this.calcOptions.aggrLimitMinimumSwellTension, Validators.compose([Validators.required])],
					'aggrLimitMaximumSwellTension': [this.calcOptions.aggrLimitMaximumSwellTension, Validators.compose([Validators.required])],
					'aggrLimitMinimumSwellTime': [this.calcOptions.aggrLimitMinimumSwellTime, Validators.compose([Validators.required])],
					'aggrLimitMaximumSwellTime': [this.calcOptions.aggrLimitMaximumSwellTime, Validators.compose([Validators.required])],
					'aggrLimitMinimumInterruptionTension': [this.calcOptions.aggrLimitMinimumInterruptionTension, Validators.compose([Validators.required])],
					'aggrLimitMaximumInterruptionTension': [this.calcOptions.aggrLimitMaximumInterruptionTension, Validators.compose([Validators.required])],
					'aggrLimitMinimumInterruptionTime': [this.calcOptions.aggrLimitMinimumInterruptionTime, Validators.compose([Validators.required])],
					'aggrLimitMaximumInterruptionTime': [this.calcOptions.aggrLimitMaximumInterruptionTime, Validators.compose([Validators.required])],
					'voltageLimitToRMS': [this.calcOptions.voltageLimitToRMS, Validators.compose([Validators.required])],
					'voltageLimitToOther': [this.calcOptions.voltageLimitToOther, Validators.compose([Validators.required])],
					'voltageLimitToRMSAtLeastOnePhase': [this.calcOptions.voltageLimitToRMSAtLeastOnePhase, Validators.compose([Validators.required])],
					'voltageLimitToRMSMax': [this.calcOptions.voltageLimitToRMSMax, Validators.compose([Validators.required])],
					'voltageLimitToOtherMax': [this.calcOptions.voltageLimitToOtherMax, Validators.compose([Validators.required])],
					'voltageLimitToOtherAtLeastOnePhase': [this.calcOptions.voltageLimitToOtherAtLeastOnePhase, Validators.compose([Validators.required])]
				});

			});
	}

	backToList() {
		this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_CALC_OPTIONS]);
	}

	save() {
		/*para aparecerem os erros caso os campos não tenham sido preenchidos */
		//this.inputControls.firstname.markAsTouched();
		//this.inputControls.lastname.markAsTouched();
		//this.inputControls.language.markAsTouched();
		//this.inputControls.profile.markAsTouched();
		//this.inputControls.login.markAsTouched();
		//this.inputControls.pwd.markAsTouched();
		/* */

		if (!this.fgCalcOptions.valid) { return; }

		Object.assign(this.calcOptions, this.fgCalcOptions.value);
		this.calcOptionsService.save(this.calcOptions).subscribe((_: any) => {
			this.backToList();
		});

	}

	add() {
		this.calcOptions = new CalcOptions({});
		this.fgCalcOptions.reset();
	}

	get inputControls() {
		return this.fgCalcOptions.controls;
	}

	filterMethods(macroMethodId: number){
		if(macroMethodId == 0){
			this.selectableMethods = this.methods;	
			return;
		}

		this.selectableMethods = this.methods.filter(x => x.aggrEventMacroMethodId == macroMethodId);
	}

	macroMehodChange(eventParam: any) {
		debugger;
		this.filterMethods(eventParam.value.id);
	}
}
