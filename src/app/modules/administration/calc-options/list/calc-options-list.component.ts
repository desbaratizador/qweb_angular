import { Component, OnInit } from '@angular/core';
import { CalcOptionsService } from 'src/app/services/calc-options.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { Router } from '@angular/router';
import appConstants from 'src/app/code/app-constants';

@Component({
  selector: 'app-user-list',
  templateUrl: './calc-options-list.component.html',
  styleUrls: ['./calc-options-list.component.scss']
})
export class CalcOptionsListComponent implements OnInit {
  gridDatasource: any[];
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'description' }
  ];


  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_CALC_OPTIONS, row.id]);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }
  }
  ];

  constructor(private calcOptionsService: CalcOptionsService, private appTranslate: AppTranslationService, private router: Router) {
    appTranslate.translateMultiple([
      'description',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }
  
  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.description;
  }

  ngOnInit() {
    this.calcOptionsService.getList().subscribe((calcOptionsList: any[]) => {
      this.gridDatasource = calcOptionsList;
    });
  }
}
