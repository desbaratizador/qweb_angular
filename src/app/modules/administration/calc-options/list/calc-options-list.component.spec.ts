import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcOptionsListComponent } from './calc-options-list.component';

describe('CalcOptionsListComponent', () => {
  let component: CalcOptionsListComponent;
  let fixture: ComponentFixture<CalcOptionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcOptionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcOptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});