import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import appConstants from 'src/app/code/app-constants';
import { DatasourceService } from 'src/app/services/datasource.service';
import { DatasourceTypeService } from 'src/app/services/datasource-type.service';
import { DataSource } from 'src/app/models/data-source';
import { DataSourceType } from 'src/app/models/data-source-type';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { compareOption } from 'src/app/code/utils';

@Component({
    selector: 'app-data-source-edit',
    templateUrl: './data-source-edit.component.html',
    styleUrls: ['./data-source-edit.component.scss']
})

export class DataSourceEditComponent implements OnInit {
    private compareOption = compareOption;
    private translations: any;
    private dataSource: DataSource;
    private dataSourceTypes: DataSourceType[];
    private fgDataSource: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private datasourceService: DatasourceService,
        private datasourceTypeService: DatasourceTypeService,
        private formBuilder: FormBuilder,
        private appTranslate: AppTranslationService
    ) {
        appTranslate.translateMultiple([
            'new',
            'userNameLabel'
        ]).subscribe((t: any) => this.translations = t);
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        forkJoin([this.datasourceService.get(id), this.datasourceTypeService.getList()])
            .subscribe((resp: any) => {
                this.dataSource = resp[0];
                this.dataSourceTypes = resp[1];
                this.fgDataSource = this.formBuilder.group({
                    'dataSourceType': [this.dataSource.dataSourceType, Validators.compose([Validators.required])],
                    'description': [this.dataSource.description, Validators.compose([Validators.required])],
                    'connectionString': [this.dataSource.connectionString, Validators.compose([Validators.required])],
                    'userId': [this.dataSource.userId, Validators.compose([Validators.required])],
                    'pwd': [this.dataSource.pwd, Validators.compose([Validators.required])]
                });
            });
    }


    backToList() {
        this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_DATASOURCES]);
    }

    save() {

        if (!this.fgDataSource.valid) { return; }

        Object.assign(this.dataSource, this.fgDataSource.value);
        this.datasourceService.save(this.dataSource).subscribe((_: any) => {
            this.backToList();
        });

    }

    add() {
        this.dataSource = new DataSource();
        this.fgDataSource.reset();
    }

    get inputControls() {
        return this.fgDataSource.controls;
    }
}
