import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiGridModule } from '@ng-plus/grid';

import { DataSourceListComponent } from './list/data-source-list.component';
import { DataSourceEditComponent } from './edit/data-source-edit.component';
import { AppMaterialModule } from 'src/app/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponentsModule } from 'src/app/components/app-components.module';

@NgModule({
  imports: [
    CommonModule,
    TiGridModule,
    AppMaterialModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AppComponentsModule
  ],
  declarations: [
    DataSourceListComponent,
    DataSourceEditComponent
  ],
  entryComponents: [
    DataSourceListComponent,
    DataSourceEditComponent
  ]
})
export class AdministrationDataSourcesModule { }