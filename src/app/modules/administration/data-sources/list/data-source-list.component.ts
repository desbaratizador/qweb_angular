import { Component, OnInit } from '@angular/core';
import { DatasourceService } from 'src/app/services/datasource.service';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import appConstants from 'src/app/code/app-constants';

@Component({
  selector: 'app-data-source-list',
  templateUrl: './data-source-list.component.html',
  styleUrls: ['./data-source-list.component.scss']
})

export class DataSourceListComponent implements OnInit {
  gridDatasource: any[];
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'dataSourceType' },
    { field: 'description' },
    { field: 'connectionString' }
  ];


  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.router.navigate([appConstants.ROUTES.ROUTE.ADMINISTRATION_DATASOURCES, row.id]);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }
  }
  ];

  constructor(private datasourceService: DatasourceService, private appTranslate: AppTranslationService, private router: Router,
    private dialog: MatDialog) {
    appTranslate.translateMultiple([
      'datasourceType',
      'description',
      'connectionString',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }

  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.datasourceType;
    this.columns[1].template = (dst) => dst.name;
    this.columns[2].title = t.description;
    this.columns[3].title = t.connectionString;    
  }

  ngOnInit() {
    this.datasourceService.getList().subscribe((dataSourceList: any[]) => {
      this.gridDatasource = dataSourceList;
    });
  }
  
}
