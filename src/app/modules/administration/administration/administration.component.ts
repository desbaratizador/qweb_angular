import { Component, OnInit } from '@angular/core';
import { AppTranslationService } from 'src/app/services/app-translation.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {

  constructor(translate: AppTranslationService) { }

  ngOnInit() {
  }

}
