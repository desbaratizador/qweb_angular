import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppReportService } from 'src/app/services/app-report.service';
import { Device } from 'src/app/models/device';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { compareOption, buildTimestamp } from 'src/app/code/utils';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';
import { CalcOptionsService } from 'src/app/services/calc-options.service';
import { LimitsDefinitionService } from 'src/app/services/limits-definition.service';
import { CalcOptions } from 'src/app/models/calc-options';
import { LimitsDefinition } from 'src/app/models/limits-definition';
import { CalcOptionsForUserService } from 'src/app/services/calc-options-for-user.service';
import { CalcOptionsForUser } from 'src/app/models/calc-options-for-user';
import { ReportService } from 'src/app/services/report.service';
import { Report } from 'src/app/models/report';
import { ToastrService } from 'ngx-toastr';
import { VariableService } from 'src/app/services/variable.service';
import { Variable } from 'src/app/models/variable';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit, OnDestroy {
    private selectedQualityComparisonYear: number[];
    private QUALITY_COMPARISON_START_YEAR = 1970;
    private QUALITY_COMPARISON_END_YEAR = new Date().getFullYear() + 10;
    private qualityComparisonYearRange: number[] = [];
    private compareOption: any = compareOption;
    private currentUser: User;
    private fgReport: FormGroup;
    private appReportServiceSubscription: Subscription;
    private authenticationServiceSubscription: Subscription;
    private limitsDefinitionServiceSubscription: Subscription;
    private calcOptionsServiceSubscription: Subscription;
    private calcOptionsForUserServiceSubscription: Subscription;
    private variableSubscription: Subscription;

    selectable = false;
    removable = false;
    private selectedDevices: Device[] = [];
    private controlsToToggle: string[] = ['userCalcOptions', 'limitsDefinitionReport', 'calcOptionsFromGlobal', 'limitsDefinitionFromGlobal'];
    private variables: Variable[];
    private globalCalcOptions: CalcOptions[] = [];
    private globalLimitsDefinition: LimitsDefinition[] = [];
    private calcOptionsForUser: CalcOptionsForUser[] = [];
    private reports: any = {
        erse: [],
        base: [],
        asset: [],
        eventCons: []
    };
    private selectedReport: Report;
    private errorTranslations: any = {};
    private reportRequest: any[] = [];
    private buildReportOptions: any = {};

    private selectedConformityVariables: Variable[] = [];

    private reportListResult: any[] = [];

    private qualityComparisonColumns: any[] = [
        { field: 'weekData', align: 'center' },
        { field: 'variable', align: 'center' },
        { field: 'statisticData', align: 'center' },
        { field: 'percent', align: 'center' },
    ];

    private qualityComparisonGridDatasource: any[] = [];

    constructor(
        private appReportService: AppReportService,
        private calcOptionsService: CalcOptionsService,
        private limitsDefinitionService: LimitsDefinitionService,
        private calcOptionsForUserService: CalcOptionsForUserService,
        private reportService: ReportService,
        private authenticationService: AuthenticationService,
        private appTranslationService: AppTranslationService,
        private formBuilder: FormBuilder,
        private toastr: ToastrService,
        private authservice: AuthenticationService,
        private variableService: VariableService
    ) {
        this.authservice.currentUser.subscribe(user => {
            if (user) {
                this.currentUser = user;
            }
        });

        this.appTranslationService.translateMultiple([
            'weekData',
            'variable',
            'statisticData',
            'percent'
        ]).subscribe((t: any) => {
            this.setGridColumnsOptions(t);
        });

        for (let sy = this.QUALITY_COMPARISON_START_YEAR, ey = this.QUALITY_COMPARISON_END_YEAR; sy <= ey; sy++) {
            this.qualityComparisonYearRange.push(sy);
        }
    }

    private setGridColumnsOptions(t: any) {
        this.qualityComparisonColumns[0].title = t.weekData;
        this.qualityComparisonColumns[1].title = t.variable;
        this.qualityComparisonColumns[2].title = t.statisticData;
        this.qualityComparisonColumns[3].title = t.percent;
    }

    ngOnInit() {
        this.appTranslationService.translateMultiple([
            'noDeviceSelected',
            'devices',
            'invalidDateSelection',
            'datesError',
            'noReport',
            'report',
        ]).subscribe((t: any) => {
            this.errorTranslations = t;
        });

        this.appReportServiceSubscription = this.appReportService.selectedDevices.subscribe((devices: Device[]) => this.selectedDevices = devices);


        this.authenticationServiceSubscription = this.authenticationService.currentUser.subscribe((user: User) => {
            this.currentUser = user;
        });

        this.calcOptionsServiceSubscription = this.calcOptionsService.getGlobal().subscribe((res: CalcOptions[]) => this.globalCalcOptions = res);

        this.limitsDefinitionServiceSubscription =
            this.limitsDefinitionService.getGlobal().subscribe((res: LimitsDefinition[]) => this.globalLimitsDefinition = res);

        this.calcOptionsForUserServiceSubscription = this.calcOptionsForUserService.getList().subscribe((res: CalcOptionsForUser[]) => this.calcOptionsForUser = res);

        this.variableSubscription = this.variableService.getVariables().subscribe((variables: Variable[]) => this.variables = variables);

        this.reportService.erseReports.subscribe((erseReports: Report[]) => this.reports.erse = erseReports);
        this.reportService.baseReports.subscribe((baseReports: Report[]) => this.reports.base = baseReports);
        this.reportService.assetReports.subscribe((assetReports: Report[]) => this.reports.asset = assetReports);
        this.reportService.eventConsReports.subscribe((eventReports: Report[]) => this.reports.eventCons = eventReports);

        this.fgReport = this.formBuilder.group({
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],
            applyDeviceConfig: [''],
            userCalcOptions: [{ value: '', disabled: true }],
            limitsDefinitionReport: [{ value: '', disabled: true }],
            calcOptionsFromGlobal: [{ value: '', disabled: true }],
            limitsDefinitionFromGlobal: [{ value: '', disabled: true }],
            calcOptionsForUser: [''],
            includeTransientsCharts: false,
            firstHarm: [''],
            secondHarm: [''],
            thirdHarm: [''],
            includeBigCharts: false,
            includeBigChartsNormalized: false,
            erseReportAllWeek: false

        });
        this.fgReport.patchValue({
            applyDeviceConfig: true,
            firstHarm: 3,
            secondHarm: 5,
            thirdHarm: 7
        });
    }

    ngOnDestroy() {
        this.appReportServiceSubscription.unsubscribe();
        this.authenticationServiceSubscription.unsubscribe();
        this.calcOptionsForUserServiceSubscription.unsubscribe();
        this.calcOptionsServiceSubscription.unsubscribe();
        this.limitsDefinitionServiceSubscription.unsubscribe();
        this.variableSubscription.unsubscribe();
    }

    remove(device: Device): void {
        this.appReportService.removeDevice(device, 'reportComponent');
    }

    startDateChanged(startDate: Date): void {
        this.fgReport.controls['startDate'].setValue(startDate);
    }

    endDateChanged(endDate: Date): void {
        this.fgReport.controls['endDate'].setValue(endDate);
    }

    applyDeviceConfigChange(event: any): void {
        if (event.checked) {
            this.controlsToToggle.forEach((ctrlName: string) => {
                this.fgReport.controls[ctrlName].disable();
            });
        } else {
            this.controlsToToggle.forEach((ctrlName: string) => {
                this.fgReport.controls[ctrlName].enable();
            });
            this.fgReport.patchValue({
                userCalcOptions: null,
                limitsDefinitionReport: null,
                calcOptionsFromGlobal: null,
                limitsDefinitionFromGlobal: null
            });
        }
    }


    reportSelect(report: Report) {
        this.selectedReport = report;
        this.selectedQualityComparisonYear = [new Date().getFullYear()];
    }

    reportList() {

    }

    executeReport() {

        let executeRequest = true;

        if (!this.fgReport.valid) {
            this.toastr.error(this.errorTranslations.invalidDateSelection, this.errorTranslations.datesError, {
                timeOut: 3000
            });
            executeRequest = false;
        }

        if (!this.selectedDevices.length) {
            this.toastr.error(this.errorTranslations.noDeviceSelected, this.errorTranslations.devices, {
                timeOut: 3000
            });
            executeRequest = false;
        }

        if (!this.selectedReport) {
            this.toastr.error(this.errorTranslations.noReport, this.errorTranslations.report, {
                timeOut: 3000
            });
            executeRequest = false;
        }

        if (!executeRequest) { return; }

        Object.assign(this.buildReportOptions, this.fgReport.value);
        switch (this.selectedReport.code) {
            case 'summaryReportPT':
            case 'summaryReportAdvancedPT':
            case 'deviceEventReportPT':
            case 'voltageReportPT':
            case 'voltageMaxMinPT':
            case 'flickerReportPT':
            case 'imbalReportPT':
            case 'freqReportPT':
            case 'frequencyMaxMinPT':
            case 'thdReportPT':
                {
                    executeRequest = this._buildGeneralRequest();
                    break;
                }
            case 'customerReportPT':
                {
                    executeRequest = this._buildCustomerReportRequest();
                    break;
                }
            case 'harmReportPT':
                {
                    executeRequest = this._buildHarmonicsReportRequest();

                    break;
                }
            case 'eventReportPT2013':
            case 'eventReportPT':
                {
                    //  acRepAggrEventsReq.addItem(oDevice);
                    break;
                }


            case 'deviceIgnoreDataPT':
            case 'deviceIgnoreEventPT':
            case 'voltageEvolutionPT':
            case 'qotREN':
            case 'nconfLastXYearsPT':
            case 'internetEDP':
            case 'ordbtEDP':
            case 'deviceMaintenancePT':
            case 'assetsREN':
                {
                    executeRequest = this._buildRequestMultiDevice();
                    break;
                }
        }

        if (!executeRequest) { return; }

        this.reportService.generateReports({
            reportRequests: this.reportRequest,
            user: this.currentUser
        })
            .subscribe((data: any) => {
                if (!data) { return; }

                this.reportListResult.unshift({
                    parentId: this.reportListResult.length,
                    parent: { report: data.reportRequests[0], zipfile: data.zipFilename },
                    children: data.reportRequests
                });
                this.reportRequest = [];
                /*   this.viewingReportList = true; */
            });
    }

    private _buildReportRequest(device) {
        this.reportRequest.push({
            '@class': 'ReportRequest',
            device: device,
            report: this.selectedReport,
            datetimeInterval: this.selectedDateTimeInterval,
            applyDeviceConfig: this.buildReportOptions.applyDeviceConfig

        });
    }

    _buildRequestMultiDevice() {
        const reportRequestMultipleDevices = {
            '@class': 'ReportRequestMultipleDevices',
            devices: this.selectedDevices,
            report: this.selectedReport,
            datetimeInterval: this.selectedDateTimeInterval
        };
        this.reportRequest.push(reportRequestMultipleDevices);
        return true;
    }

    _buildGeneralRequest() {
        this.selectedDevices.forEach((device: Device, idx: number) => {
            this._buildReportRequest(device);
        });

        return true;
    }

    _buildCustomerReportRequest(): boolean {
        let hasErrors = false;
        this.selectedDevices.forEach((device: Device, idx) => {
            if (!device/*.hasSelectedCustomers*/) {
                hasErrors = true;
                return;
            }

            this._buildReportRequest(device);
        });

        return !hasErrors;
    }

    _buildHarmonicsReportRequest(): boolean {
        let hasErrors = false;
        hasErrors = false;
        return !hasErrors;
    }

    private get selectedDateTimeInterval() {
        return {
            startDate: buildTimestamp(this.buildReportOptions.startDate),
            endDate: buildTimestamp(this.buildReportOptions.endDate),
        };
    }

    includeTransientsChartsChange(event: any) {
        this.fgReport.patchValue({
            includeTransientsCharts: event.checked
        });
    }

    includeBigChartsChange(event: any) {
        this.fgReport.patchValue({
            includeBigCharts: event.checked
        });
    }

    includeBigChartsNormalizedChange(event: any) {
        this.fgReport.patchValue({
            includeBigChartsNormalized: event.checked
        });
    }

    conformityVariableChange(event: any, variable: Variable) {
        if (event.checked) {
            this.selectedConformityVariables.push(variable);
        } else {
            const idx = this.selectedConformityVariables.indexOf(variable);
            this.selectedConformityVariables.splice(idx, 1);
        }
    }

    erseReportAllWeekChange(event: any) {
        this.fgReport.patchValue({
            erseReportAllWeek: event.checked
        });
    }

    qualityComparisonYearSelected(year: number, event: MouseEvent) {
        if (event.shiftKey || event.ctrlKey) {
            this.selectedQualityComparisonYear.push(year);
        } else {
            this.selectedQualityComparisonYear = [];
            this.selectedQualityComparisonYear.push(year);
        }
    }

    gridAddRecord() {

    }
}
