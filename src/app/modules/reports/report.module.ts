import { AppComponentsModule } from 'src/app/components/app-components.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AppMaterialModule } from 'src/app/material.module';
import { TiGridModule } from '@ng-plus/grid';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReportComponent } from './report/report.component';

@NgModule({
    imports: [
        CommonModule,
        TiGridModule,
        AppMaterialModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        AppComponentsModule
    ],
    declarations: [


    ],
    entryComponents: [
        ReportComponent
    ]
})
export class ReportModule { }
