import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { AppReportService } from 'src/app/services/app-report.service';
import appConstants from 'src/app/code/app-constants';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    title = 'Qweb';
    _opened = true;
    currentUser: User;
    constructor(
        private authservice: AuthenticationService,
        private appReportService: AppReportService,
        private route: ActivatedRoute,
        private router: Router,
        private translate: AppTranslationService) {
        this.appReportService.onDeviceRemoved.subscribe((event: any) => {
            console.log(event);
        });
        this.authservice.currentUser.subscribe(user => {
            if (user) {
                this.currentUser = user;
            } else {
                this.router.navigate(['/login']);
            }
        });
    }

    ngOnInit() {
    }
    logout() {
        this.authservice.logout();
        this.router.navigate(['/login']);
    }
    _toggleSideBar(): void {
        this._opened = !this._opened;
    }

    treeNodeToggle(event: any) {
        if (event.selected) {
            this.appReportService.addDevice(event.device);
        } else {
            this.appReportService.removeDevice(event.device, 'tree');
        }
        if (this.route.snapshot.firstChild && this.route.snapshot.firstChild.url[0].path === appConstants.ROUTES.ROUTE.REPORTS) {
            return;
        }
        this.router.navigate([appConstants.ROUTES.ROUTE.REPORTS]);
    }
}
