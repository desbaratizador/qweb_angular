import appConstants from 'src/app/code/app-constants';
import { DescriptionListService } from 'src/app/services/description-list.service';

export class DescriptionLists {
    _descriptionLists;

    busbarLineMeasures;
    busbarTypes;
    excelExportTypes;
    gestincFaultNatures;
    ignoreDataApplies;
    maintenanceCauses;
    measureLocTypes;
    transformerTypes;
    reportTypes;

    constructor(descriptionListService: DescriptionListService) {
        // this.descriptionListsInit(data);
        descriptionListService.getDataFromServer().subscribe((descriptionLists: any[]) => {
            this.descriptionListsInit(descriptionLists) ;
        });
    }

    descriptionListsInit(data) {
        this._descriptionLists = data;

        this.busbarLineMeasures = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.BUSBAR_LINE_MEASURE);
        this.busbarTypes = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.BUSBAR_TYPE);
        this.excelExportTypes = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.EXCEL_EXPORT_TYPE);
        this.gestincFaultNatures = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.GESTINC_FAULT_TYPE);
        this.ignoreDataApplies = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.IGNORE_DATA_APPLIES);
        this.maintenanceCauses = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.MAINTENANCE_CAUSE);
        this.measureLocTypes = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.MEASURE_LOC_TYPE);
        this.transformerTypes = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.TRANSFORMER_TYPE);
        this.reportTypes = this.getContextData(appConstants.DESCRIPTION_LIST_CONTEXT.REPORT_TYPE);
    }

    getContextData(context) {
        return this._descriptionLists.filter(function (dl) {
            return (dl[1].context === context);
        }).map(function (item) {
            return item[1];
        });
    }
}
