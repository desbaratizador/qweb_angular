import { Component, OnInit } from '@angular/core';
import { AppTranslationService } from 'src/app/services/app-translation.service';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {

  constructor(translate: AppTranslationService) { }

  ngOnInit() {
  }

}
