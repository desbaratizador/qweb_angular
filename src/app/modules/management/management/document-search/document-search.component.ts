import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import appConstants from 'src/app/code/app-constants';
import { forkJoin } from 'rxjs';
import { DocumentService } from 'src/app/services/document.service';
import { Document } from 'src/app/models/document';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { compareOption } from 'src/app/code/utils';

@Component({
  selector: 'app-document-search-edit',
  templateUrl: './document-search.component.html',
  styleUrls: ['./document-search.component.scss']
})

export class DocumentSearchComponent implements OnInit {
  private compareOption = compareOption;
  
  private translations: any;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private documentService: DocumentService,
    private formBuilder: FormBuilder,
    private appTranslate: AppTranslationService
  ) {
    appTranslate.translateMultiple([
      'new',
      'userNameLabel'
    ]).subscribe((t: any) => this.translations = t);

  }

  ngOnInit() {
    
  }

  

  save() {
   

  }

  add() {
    
  }

  get inputControls() {
    return null;
  }
}
