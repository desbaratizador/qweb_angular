import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DeviceConfig } from 'src/app/models/device-config';
import { TimeEditor } from 'src/app/components/date/date-time-editor/time-editor/time-editor';
import { compareOption } from 'src/app/code/utils';
import { CalcOptionsService } from 'src/app/services/calc-options.service';
import { EquipmentService } from 'src/app/services/equipment.service';
import { Subscription } from 'rxjs';
import { Equipment } from 'src/app/models/equipment';
import { CalcOptions } from 'src/app/models/calc-options';
import { LimitsDefinitionService } from 'src/app/services/limits-definition.service';
import { LimitsDefinition } from 'src/app/models/limits-definition';


@Component({
    selector: 'app-device-config-modal',
    templateUrl: './device-config-modal.component.html',
    styleUrls: ['./device-config-modal.component.scss']
})
export class DeviceConfigModalComponent implements OnInit, OnDestroy {
    private compareOption = compareOption;
    private workingData: any = {};
    private equipments: Equipment[];
    private calcOptions: CalcOptions[];
    private limitsDefinitions: LimitsDefinition[];

    private equipmentSubscription: Subscription;
    private calcOptionsSubscription: Subscription;
    private limitDefinitionsSubscription: Subscription;

    constructor(
        public dialogRef: MatDialogRef<DeviceConfigModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DeviceConfig,
        private calcOptionsService: CalcOptionsService,
        private equipmentService: EquipmentService,
        private limitDefinitionsService: LimitsDefinitionService
    ) {
        Object.assign(this.workingData, data);
        this.equipmentSubscription = equipmentService.getList().subscribe((equipments: Equipment[]) => this.equipments = equipments);

        this.calcOptionsSubscription = calcOptionsService.getList().subscribe((calcOptions: CalcOptions[]) => this.calcOptions = calcOptions);

        this.limitDefinitionsSubscription = limitDefinitionsService.getList().subscribe((limitDefinitions: LimitsDefinition[]) => this.limitsDefinitions = limitDefinitions);
    }

    ngOnInit() {

        this.workingData.declaredVoltage = this.workingData.declaredVoltage || 230;
    }
    ngOnDestroy() {
        this.equipmentSubscription.unsubscribe();
        this.calcOptionsSubscription.unsubscribe();
        this.limitDefinitionsSubscription.unsubscribe();
    }
    cancel() {
        this.dialogRef.close();
    }
    acceptChanges() {
        Object.assign(this.data, this.workingData);
    }

    startDateChanged(startDate) {
        this.workingData.startDate = startDate;
    }
    simpleCheckChange(evt) {
        this.workingData.simple = evt.checked;
    }

    simpleCalcCheckChange(evt) {
        this.workingData.simpleCalc = evt.checked;
    }

    slidingDSICheckChange(evt) {
        this.workingData.slidingDSI = evt.checked;
    }

    fixedCheckChange(evt) {
        this.workingData.fixed = evt.checked;
    }

    ignoreRecordValidityCheckChange(evt) {
        this.workingData.ignoreRecordValidity = evt.checked;
    }

    validateRecordsByTensionExtremesCheckChange(evt) {
        this.workingData.validateRecordsByTensionExtremes = evt.checked;
    }

    validateRecordsByEventsCheckChange(evt) {
        this.workingData.validateRecordsByEvents = evt.checked;
    }

    equipmentChange(evt) {
        this.workingData.equipment = evt.value;
    }

    calcOptionsChange(evt) {
        this.workingData.calcOptions = evt.value;
    }

    limitsDefinitionChange(evt) {
        this.workingData.limitDefinitions = evt.value;
    }

    declaredVoltageChange(evt) {
        this.workingData.declaredVoltage = evt.currentTarget.value;
    }
}
