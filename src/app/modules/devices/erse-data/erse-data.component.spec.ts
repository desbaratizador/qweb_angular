import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErseDataComponent } from './erse-data.component';

describe('ErseDataComponent', () => {
  let component: ErseDataComponent;
  let fixture: ComponentFixture<ErseDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErseDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErseDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
