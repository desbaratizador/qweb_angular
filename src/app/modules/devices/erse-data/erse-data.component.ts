import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-erse-data',
  templateUrl: './erse-data.component.html',
  styleUrls: ['./erse-data.component.scss']
})
export class ErseDataComponent implements OnInit {
  @Input() dataForm: FormGroup;
  constructor() { }

  ngOnInit() {
  }

}
