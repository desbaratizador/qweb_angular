import { Component, OnInit, Input } from '@angular/core';
import appConstants from 'src/app/code/app-constants';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { MatDialog } from '@angular/material';
import { DeviceConfigModalComponent } from '../device-config-modal/device-config-modal.component';

@Component({
  selector: 'app-device-config',
  templateUrl: './device-config.component.html',
  styleUrls: ['./device-config.component.scss']
})
export class DeviceConfigComponent implements OnInit {
  @Input() gridDatasource: any[];
  @Input() deviceId: number;
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'startDate', align: 'center' },
    { field: 'endDate', align: 'center' },
    { field: 'simple', align: 'center' },
    { field: 'simpleCalc', align: 'center' },
    { field: 'dataSource', align: 'center' },
    { field: 'deviceDataSourceId', align: 'center' },
    { field: 'calcOptions', align: 'center' },
    { field: 'declaredVoltage', align: 'center' },
    { field: 'limitsDefinition', align: 'center' },
    { field: 'equipment', align: 'center' },
    { field: 'equipmentSerialNumber', align: 'center' },
    { field: 'validateRecordsByEvents', align: 'center' },
    { field: 'validateRecordsByTensionExtremes', align: 'center' },
    { field: 'ignoreRecordValidity', align: 'center' },
    { field: 'fixed', align: 'center' },
    { field: 'slidingDSI', align: 'center' },
    { field: 'indirectMeasureAt', align: 'center' },

  ];


  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.openRecordModal(row);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }

  }
  ];


  constructor(
    private appTranslate: AppTranslationService,
    private dialog: MatDialog
  ) {
    appTranslate.translateMultiple([
      'startDate',
      'endDate',
      'starMeasure',
      'starCalc',
      'datasource',
      'deviceDataSource',
      'calcOptions',
      'declaredVoltage',
      'limitsDefinition',
      'equipment',
      'equipmentSerialNumber',
      'validateRecordsByEvents',
      'validateRecordsByTensionExtremes',
      'ignoreRecordValidation',
      'fixed',
      'slidingDSI',
      'indirectMeasureAt',
      'yes',
      'no',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }
  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.startDate;
    this.columns[1].template = (v) => new Date(v).toLocaleDateString('pt-PT') + ' ' + new Date(v).toLocaleTimeString('pt-PT');
    this.columns[2].title = t.endDate;
    this.columns[2].template = (v) => v ? new Date(v).toLocaleDateString('pt-PT') : null;
    this.columns[3].title = t.starMeasure;
    this.columns[3].template = (v) => v ? t.yes : t.no;
    this.columns[4].title = t.starCalc;
    this.columns[4].template = (v) => v ? t.yes : t.no;
    this.columns[5].title = t.datasource;
    this.columns[5].template = (v) => v ? v.description : '';
    this.columns[6].title = t.deviceDataSource;
    this.columns[7].title = t.calcOptions;
    this.columns[7].template = (v) => v ? v.description : '';
    this.columns[8].title = t.declaredVoltage;
    this.columns[9].title = t.limitsDefinition;
    this.columns[9].template = (v) => v ? v.description : '';
    this.columns[10].title = t.equipment;
    this.columns[10].template = (v) => v ? v.name : '';
    this.columns[11].title = t.equipmentSerialNumber;
    this.columns[12].title = t.validateRecordsByEvents;
    this.columns[12].template = (v) => v ? t.yes : t.no;
    this.columns[13].title = t.validateRecordsByTensionExtremes;
    this.columns[13].template = (v) => v ? t.yes : t.no;
    this.columns[14].title = t.ignoreRecordValidation;
    this.columns[14].template = (v) => v ? t.yes : t.no;
    this.columns[15].title = t.fixed;
    this.columns[15].template = (v) => v ? t.yes : t.no;
    this.columns[16].title = t.slidingDSI;
    this.columns[16].template = (v) => v ? t.yes : t.no;
    this.columns[17].title = t.indirectMeasureAt;
  }

  ngOnInit() {
  }
  private gridAddRecord(evt) {
    this.openRecordModal({
      id: 0,
      device: { id: this.deviceId },
      startDate: null,
      endDate: null,
      user: { id: 24 },
      ignoreDataCause: null,
      obs: null
    });
  }
  openRecordModal(data) {
    const dialogRef = this.dialog.open(DeviceConfigModalComponent, {
      width: '80vw',
      height: '55vh',
      data: data
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result && result.id === 0) {
        this.gridDatasource.push(result);
      }
    });
  }
}
