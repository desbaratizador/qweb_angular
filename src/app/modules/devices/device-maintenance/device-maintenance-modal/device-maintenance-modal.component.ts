import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DeviceMaintenance } from 'src/app/models/device-maintenance';
import { compareOption } from 'src/app/code/utils';
import { DescriptionListService } from 'src/app/services/description-list.service';
import { Subscription } from 'rxjs';
import { DescriptionList } from 'src/app/models/description-list';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-device-maintenance-modal',
    templateUrl: './device-maintenance-modal.component.html',
    styleUrls: ['./device-maintenance-modal.component.scss']
})
export class DeviceMaintenanceModalComponent implements OnInit, OnDestroy {
    private compareOption = compareOption;
    private workingData: any = {};
    private descriptionListSubscription: Subscription;
    private maintenanceCauses: DescriptionList[];
    private fgMaintenanceCause: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<DeviceMaintenanceModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DeviceMaintenance,
        descriptionListService: DescriptionListService,
        private formBuilder: FormBuilder
    ) {
        Object.assign(this.workingData, data);
        this.descriptionListSubscription = descriptionListService.getMaintenanceCauses().subscribe((maintenanceCauses: DescriptionList[]) => {
            this.maintenanceCauses = maintenanceCauses;
        });
    }

    ngOnInit() {
        this.fgMaintenanceCause = this.formBuilder.group({
            'startDate': [this.workingData.startDate, Validators.compose([Validators.required])],
            'endDate': [this.workingData.endDate],
            'maintenanceCause': [this.workingData.maintenanceCause, Validators.compose([Validators.required])],
            'serviceOrder': [this.workingData.serviceOrder],
            'validity': [this.workingData.validity],
            'document': [this.workingData.document],
            'obs': [this.workingData.obs],
        });
    }

    ngOnDestroy(): void {
        this.descriptionListSubscription.unsubscribe();
    }

    cancel() {
        this.dialogRef.close();
    }

    acceptChanges() {

        //  this.inputControls.startDate.markAsTouched();
        if (!this.fgMaintenanceCause.valid) { return; }
        Object.assign(this.workingData, this.fgMaintenanceCause.value);
        Object.assign(this.data, this.workingData);
        this.dialogRef.close(this.data);
    }

    get inputControls() {
        return this.fgMaintenanceCause.controls;
    }

    startDateChanged(startDate) {
        this.fgMaintenanceCause.controls['startDate'].setValue(startDate);
    }

    endDateChanged(endDate) {
        this.fgMaintenanceCause.controls['endDate'].setValue(endDate);
    }
}
