import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceMaintenanceModalComponent } from './device-maintenance-modal.component';

describe('DeviceMaintenanceModalComponent', () => {
  let component: DeviceMaintenanceModalComponent;
  let fixture: ComponentFixture<DeviceMaintenanceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceMaintenanceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceMaintenanceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
