import { Component, OnInit, Input, AfterViewInit, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { DeviceMaintenanceModalComponent } from './device-maintenance-modal/device-maintenance-modal.component';
import { DeviceMaintenance } from 'src/app/models/device-maintenance';

@Component({
    selector: 'app-device-maintenance',
    templateUrl: './device-maintenance.component.html',
    styleUrls: ['./device-maintenance.component.scss']
})
export class DeviceMaintenanceComponent implements OnInit {

    @Input() gridDatasource: any[];
    @Input() deviceId: number;
    columns: any[] = [
        { title: 'Id', field: 'id' },
        { field: 'startDate', align: 'center' },
        { field: 'endDate', align: 'center' },
        { field: 'maintenanceCause', align: 'center' },
        { field: 'validity', align: 'center' },
        { field: 'serviceOrder', align: 'center' },
        { field: 'document', align: 'center' },
        { field: 'obs', align: 'center' },
    ];


    rowActions = [{
        title: '',
        classes: '',
        icon: 'fal fa-edit',
        onClick: (row) => {
            this.openRecordModal(row);
        }
    }, {
        title: '',
        classes: 'text-danger',
        icon: 'fal fa-trash',
        onClick: (row) => {
            row.isDeleted = true;
        }

    }
    ];
    constructor(
        private appTranslate: AppTranslationService,
        private dialog: MatDialog,
        private elementRef: ElementRef
    ) {
        appTranslate.translateMultiple([
            'startDate',
            'endDate',
            'maintenanceCause',
            'validity',
            'serviceOrder',
            'document',
            'obs',
            'yes',
            'no',
            'deleteRecord',
            'edit'
        ]).subscribe((t: any) => {
            this.setGridColumnsOptions(t);
            this.setGridActionsOptions(t);
        });
    }

    setGridActionsOptions(t: any) {
        this.rowActions[0].title = t.edit;
        this.rowActions[1].title = t.deleteRecord;
    }
    setGridColumnsOptions(t: any) {
        this.columns[1].title = t.startDate;
        this.columns[1].template = (v) => new Date(v).toLocaleDateString('pt-PT') + ' ' + new Date(v).toLocaleTimeString('pt-PT');
        this.columns[2].title = t.endDate;
        this.columns[2].template = (v) => v ? new Date(v).toLocaleDateString('pt-PT') : null;
        this.columns[3].title = t.maintenanceCause;
        this.columns[3].template = (v) => v ? v.description : '';
        this.columns[4].title = t.validity;
        this.columns[4].template = (v) => v ? new Date(v).toLocaleDateString('pt-PT') : null;
        this.columns[5].title = t.serviceOrder;
        this.columns[6].title = t.document;
        this.columns[7].title = t.obs;
    }
    ngOnInit() {
    }

    private gridAddRecord(evt) {
        this.openRecordModal({ id: 0, startDate: null, endDate: null, obs: null, document: null, maintenanceCause: null, validity: null });
    }

    private openRecordModal(data) {
        const dialogRef = this.dialog.open(DeviceMaintenanceModalComponent, {
            width: '80vw',
            height: '55vh',
            data: data
        });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result && result.id === 0) {
                result.device = { id: this.deviceId };
                result.user = { id: 41 };
                this.gridDatasource.push(result);
            }
        });
    }
}
