import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceIgnoreEventsModalComponent } from './device-ignore-events-modal.component';

describe('DeviceIgnoreEventsModalComponent', () => {
  let component: DeviceIgnoreEventsModalComponent;
  let fixture: ComponentFixture<DeviceIgnoreEventsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceIgnoreEventsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceIgnoreEventsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
