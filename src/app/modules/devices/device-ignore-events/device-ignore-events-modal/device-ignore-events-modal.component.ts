import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { compareOption } from 'src/app/code/utils';
import { Subscription } from 'rxjs';
import { IgnoreDataCause } from 'src/app/models/ignore-data-cause';
import { IgnoreDataCauseService } from 'src/app/services/ignore-data-cause.service';
import { DeviceIgnoreEvent } from 'src/app/models/device-ignore-event';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
    selector: 'app-device-ignore-events-modal',
    templateUrl: './device-ignore-events-modal.component.html',
    styleUrls: ['./device-ignore-events-modal.component.scss']
})
export class DeviceIgnoreEventsModalComponent implements OnInit, OnDestroy {
    private compareOption = compareOption;
    private ignoreDataCausesSubscription: Subscription;
    private ignoreDataCauses: IgnoreDataCause[];
    private fgIgnoreEvents: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<DeviceIgnoreEventsModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DeviceIgnoreEvent,
        ignoreDataCauseService: IgnoreDataCauseService,
        private formBuilder: FormBuilder
    ) {
        this.ignoreDataCausesSubscription = ignoreDataCauseService.getIgnoreDataCauses().subscribe((dataCauses: IgnoreDataCause[]) => this.ignoreDataCauses = dataCauses);
    }

    ngOnInit() {
        this.fgIgnoreEvents = this.formBuilder.group({
            'startDate': [this.data.startDate, Validators.compose([Validators.required])],
            'endDate': [this.data.endDate, Validators.compose([Validators.required])],
            'ignoreDataCause': [this.data.ignoreDataCause, Validators.compose([Validators.required])],
            'obs': [this.data.obs, Validators.compose([Validators.required])]
        });
    }

    ngOnDestroy(): void {
        this.ignoreDataCausesSubscription.unsubscribe();
    }

    cancel() {
        this.dialogRef.close();
    }

    acceptChanges() {
        if (!this.fgIgnoreEvents.valid) { return; }

        Object.assign(this.data, this.fgIgnoreEvents.value);
        this.dialogRef.close(this.data);
    }

    get inputControls() {
        return this.fgIgnoreEvents.controls;
    }

    startDateChanged(startDate) {
        this.fgIgnoreEvents.controls['startDate'].setValue(startDate);
    }

    endDateChanged(endDate) {
        this.fgIgnoreEvents.controls['endDate'].setValue(endDate);
    }
}
