import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { DeviceIgnoreEventsModalComponent } from './device-ignore-events-modal/device-ignore-events-modal.component';

@Component({
  selector: 'app-device-ignore-events',
  templateUrl: './device-ignore-events.component.html',
  styleUrls: ['./device-ignore-events.component.scss']
})
export class DeviceIgnoreEventsComponent implements OnInit {
  @Input() gridDatasource: any[];
  @Input() deviceId: number;
  columns: any[] = [
    { title: 'Id', field: 'id' },
    { field: 'startDate', align: 'center' },
    { field: 'endDate', align: 'center' },
    { field: 'ignoreDataCause', align: 'center' },
    { field: 'obs', align: 'center' },
  ];


  rowActions = [{
    title: '',
    classes: '',
    icon: 'fal fa-edit',
    onClick: (row) => {
      this.openRecordModal(row);
    }
  }, {
    title: '',
    classes: 'text-danger',
    icon: 'fal fa-trash',
    onClick: (row) => {
      row.isDeleted = true;
    }

  }
  ];
  constructor(
    private appTranslate: AppTranslationService,
    private dialog: MatDialog
  ) {
    appTranslate.translateMultiple([
      'startDate',
      'endDate',
      'ignoreDataCause',
      'observations',
      'yes',
      'no',
      'deleteRecord',
      'edit'
    ]).subscribe((t: any) => {
      this.setGridColumnsOptions(t);
      this.setGridActionsOptions(t);
    });
  }

  setGridActionsOptions(t: any) {
    this.rowActions[0].title = t.edit;
    this.rowActions[1].title = t.deleteRecord;
  }
  setGridColumnsOptions(t: any) {
    this.columns[1].title = t.startDate;
    this.columns[1].template = (v) => new Date(v).toLocaleDateString('pt-PT') + ' ' + new Date(v).toLocaleTimeString('pt-PT');
    this.columns[2].title = t.endDate;
    this.columns[2].template = (v) => v ? new Date(v).toLocaleDateString('pt-PT') : null;
    this.columns[3].title = t.ignoreDataCause;
    this.columns[3].template = (v) => v ? v.description : '';
    this.columns[4].title = t.observations;
  }

  ngOnInit() {
  }

  private gridAddRecord(evt) {
    this.openRecordModal({
      id: 0,
      device: { id: this.deviceId },
      startDate: null,
      endDate: null,
      user: { id: 24 },
      ignoreDataCause: null,
      obs: null
    });
  }

  openRecordModal(data) {
    const dialogRef = this.dialog.open(DeviceIgnoreEventsModalComponent, {
      width: '60vw',
      height: '50vh',
      data: data
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result && result.id === 0) {
        this.gridDatasource.push(result);
      }
    });
  }
}
