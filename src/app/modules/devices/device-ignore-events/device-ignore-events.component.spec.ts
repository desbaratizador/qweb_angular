import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceIgnoreEventsComponent } from './device-ignore-events.component';

describe('DeviceIgnoreEventsComponent', () => {
  let component: DeviceIgnoreEventsComponent;
  let fixture: ComponentFixture<DeviceIgnoreEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceIgnoreEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceIgnoreEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
