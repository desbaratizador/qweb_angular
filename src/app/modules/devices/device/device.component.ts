import { Component, OnInit } from '@angular/core';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceService } from 'src/app/services/device.service';
import { forkJoin } from 'rxjs';
import { Device } from 'src/app/models/device';
import { Location } from 'src/app/models/location';
import { LocationService } from 'src/app/services/location.service';


@Component({
    selector: 'app-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {

    private translations: any;
    private device: Device;
    private locations: Location[];
    private fgDevice: FormGroup;
    private fgDeviceAdditionalData: FormGroup;
    private fgErseData: FormGroup;
    private faTabForms: FormArray;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private appTranslate: AppTranslationService,
        private locationService
            : LocationService,
        private deviceService: DeviceService,
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        forkJoin([
            this.deviceService.get(id),
            this.locationService.getList()]).subscribe((result: any) => {
                this.device = result[0];
                this.setDeviceFormGroup();
                this.locations = result[1];
            });
        /*     this.deviceService.get(id).subscribe((d: Device) => {
              this.device = d;
              this.setDeviceFormGroup();
            }); */
    }

    setDeviceFormGroup() {
        this.fgDevice = this.formBuilder.group({
            'name': [this.device.name, Validators.compose([Validators.required])],
            'code': [this.device.code, Validators.compose([Validators.required])],
            'nominalVoltage': [this.device.nominalVoltage, Validators.compose([Validators.required])],
            'description': [this.device.description, Validators.compose([Validators.maxLength(300)])],
            'locationId': [this.device.locationId],
            'weekProcess': [this.device.weekProcess, Validators.compose([Validators.required])],
            'weekProcessAutomatic': [this.device.weekProcessAutomatic, Validators.compose([Validators.required])],
            'deliveryPoint': [this.device.deliveryPoint, Validators.compose([Validators.required])],
            'eventProcess': [this.device.eventProcess, Validators.compose([Validators.required])],
            'measurePointDescription': [this.device.measurePointDescription, Validators.compose([Validators.maxLength(50)])],
        });
        this.fgDeviceAdditionalData = this.formBuilder.group({
            'customerMeasured': [this.device.customerMeasured],
            'measureLocationType': [this.device.measureLocationType],
            'faultLevel': [this.device.faultLevel],
            'busbarLineMeasurement': [this.device.busbarLineMeasurement],
            'busbarType': [this.device.busbarType],
            'latitude': [this.device.latitude],
            'longitude': [this.device.longitude],
            'transformerTypeUpstream': [this.device.transformerTypeUpstream],
            'transformerTypeDownstream': [this.device.transformerTypeDownstream],
        });
        this.fgErseData = this.formBuilder.group({
            'deviceZoneA': [this.device.deviceZoneA],
            'deviceZoneB': [this.device.deviceZoneB],
            'deviceZoneC': [this.device.deviceZoneC],
            'drc': [this.device.drc],
            'ao': [this.device.ao],
            'injector': [this.device.injector],
            'substation': [this.device.substation],
            'barring': [this.device.barring],
            'output': [this.device.output],
            'county': [this.device.county],
            'instalation': [this.device.instalation],
            'district': [this.device.district],
        });
    }

    get inputControls() {
        return this.fgDevice.controls;
    }

    save() {
        /*para aparecerem os erros caso os campos não tenham sido preenchidos */
        /*  this.inputControls.firstname.markAsTouched();
         this.inputControls.lastname.markAsTouched();
         this.inputControls.language.markAsTouched();
         this.inputControls.profile.markAsTouched();
         this.inputControls.login.markAsTouched();
         this.inputControls.pwd.markAsTouched(); */
        /* */

        /*   if (!this.fgDevice.valid) { return; } */

        Object.assign(this.device, this.fgDevice.value, this.fgDeviceAdditionalData.value, this.fgErseData.value);

        this.deviceService.save(this.device).subscribe((_: any) => {
            console.log('saved successfully');
        });
    }
}
