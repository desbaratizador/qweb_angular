import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { DevicePropertiesModalComponent } from './device-properties-modal/device-properties-modal.component';

@Component({
    selector: 'app-device-properties',
    templateUrl: './device-properties.component.html',
    styleUrls: ['./device-properties.component.scss']
})
export class DevicePropertiesComponent implements OnInit {
    @Input() gridDatasource: any[];
    @Input() deviceId: number;
    columns: any[] = [
        { title: 'Id', field: 'id' },
        { field: 'propertyName', align: 'center' },
        { field: 'propertyValue', align: 'center' },
    ];


    rowActions = [{
        title: '',
        classes: '',
        icon: 'fal fa-edit',
        onClick: (row) => {
            this.openRecordModal(row);
        }
    }, {
        title: '',
        classes: 'text-danger',
        icon: 'fal fa-trash',
        onClick: (row) => {
            row.isDeleted = true;
        }

    }
    ];
    constructor(
        private appTranslate: AppTranslationService,
        private dialog: MatDialog
    ) {
        appTranslate.translateMultiple([
            'deviceTComponentPropertyKey',
            'deviceTComponentPropertyValue',
            'yes',
            'no',
            'deleteRecord',
            'edit'
        ]).subscribe((t: any) => {
            this.setGridColumnsOptions(t);
            this.setGridActionsOptions(t);
        });
    }
    setGridActionsOptions(t: any) {
        this.rowActions[0].title = t.edit;
        this.rowActions[1].title = t.deleteRecord;
    }
    setGridColumnsOptions(t: any) {
        this.columns[1].title = t.deviceTComponentPropertyKey;
        this.columns[2].title = t.deviceTComponentPropertyValue;
    }
    ngOnInit() {
    }

    private gridAddRecord(evt) {
        this.openRecordModal({ id: 0, parent: { id: this.deviceId }, parentType: { id: 1 }, propertyName: null, propertyValue: null });
    }

    openRecordModal(data) {
        const dialogRef = this.dialog.open(DevicePropertiesModalComponent, {
            width: '40vw',
            height: '22vh',
            data: data
        });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result && result.id === 0) {
                this.gridDatasource.push(result);
            }
        });
    }
}
