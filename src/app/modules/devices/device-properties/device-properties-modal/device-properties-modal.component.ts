import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TreeComponentProperty } from 'src/app/models/tree-component-property';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-device-properties-modal',
    templateUrl: './device-properties-modal.component.html',
    styleUrls: ['./device-properties-modal.component.scss']
})
export class DevicePropertiesModalComponent implements OnInit, OnDestroy {
    private fgProperties: FormGroup;
    // private workingData: any = {};

    ngOnDestroy(): void {

    }

    constructor(
        public dialogRef: MatDialogRef<DevicePropertiesModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: TreeComponentProperty,
        private formBuilder: FormBuilder
    ) {
        //  Object.assign(this.workingData, data);
    }

    ngOnInit() {
        this.fgProperties = this.formBuilder.group({
            'propertyName': [this.data.propertyName, Validators.compose([Validators.required])],
            'propertyValue': [this.data.propertyValue, Validators.compose([Validators.required])]
        });
    }

    cancel() {
        this.dialogRef.close();
    }

    get inputControls() {
        return this.fgProperties.controls;
    }

    acceptChanges() {
        if (!this.fgProperties.valid) { return; }
        Object.assign(this.data, this.fgProperties.value);
        this.dialogRef.close(this.data);
    }

    valueChanged(evt) {
        // this.workingData.propertyValue = (event.target as any).value;
    }

    keyChanged(evt) {
        // this.workingData.propertyName = (event.target as any).value;
    }
}
