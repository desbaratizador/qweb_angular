import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicePropertiesModalComponent } from './device-properties-modal.component';

describe('DevicePropertiesModalComponent', () => {
  let component: DevicePropertiesModalComponent;
  let fixture: ComponentFixture<DevicePropertiesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicePropertiesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicePropertiesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
