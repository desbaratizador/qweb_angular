import { Component, OnInit, Input, AfterContentInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Device } from 'src/app/models/device';
import { DescriptionList } from 'src/app/models/description-list';
import { DescriptionListService } from 'src/app/services/description-list.service';
import appConstants from 'src/app/code/app-constants';
import { compareOption } from 'src/app/code/utils';

@Component({
  selector: 'app-device-additional-data',
  templateUrl: './device-additional-data.component.html',
  styleUrls: ['./device-additional-data.component.scss']
})
export class DeviceAdditionalDataComponent implements OnInit, AfterContentInit {
  constants = appConstants;
  // @Input() device: Device;
  @Input() additionalDataForm: FormGroup;
  // private descriptionlist: DescriptionList[];
  private measureLocTypes: DescriptionList[];
  private busbarLineMeasures: DescriptionList[];
  private busBarTypes: DescriptionList[];
  private transformerTypes: DescriptionList[];

  private compareOption = compareOption;
  constructor(
    private descriptionListService: DescriptionListService
  ) { }

  ngOnInit() {
    const _self = this;
    this.setForm();
    this.descriptionListService.getDataFromServer().subscribe((data: any) => {

      _self.measureLocTypes = data.filter((dl: DescriptionList) => dl.context === appConstants.DESCRIPTION_LIST_CONTEXT.MEASURE_LOC_TYPE);

      _self.busbarLineMeasures = data.filter((dl: DescriptionList) => dl.context === appConstants.DESCRIPTION_LIST_CONTEXT.BUSBAR_LINE_MEASURE);

      _self.busBarTypes = data.filter((dl: DescriptionList) => dl.context === appConstants.DESCRIPTION_LIST_CONTEXT.BUSBAR_TYPE);

      _self.transformerTypes = data.filter((dl: DescriptionList) => dl.context === appConstants.DESCRIPTION_LIST_CONTEXT.TRANSFORMER_TYPE);
    });

  }
  ngAfterContentInit(): void {

  }

  setForm() {

  }
}
