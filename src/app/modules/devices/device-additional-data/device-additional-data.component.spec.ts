import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceAdditionalDataComponent } from './device-additional-data.component';

describe('DeviceAdditionalDataComponent', () => {
  let component: DeviceAdditionalDataComponent;
  let fixture: ComponentFixture<DeviceAdditionalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceAdditionalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceAdditionalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
