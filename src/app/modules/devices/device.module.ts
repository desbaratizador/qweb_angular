import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiGridModule } from '@ng-plus/grid';


import { AppMaterialModule } from 'src/app/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeviceComponent } from './device/device.component';
import { DeviceAdditionalDataComponent } from './device-additional-data/device-additional-data.component';
import { DescriptionListFilterPipe } from 'src/app/pipes/description-list-filter.pipe';
import { DeviceConfigComponent } from './device-config/device-config.component';
import { DeviceConfigModalComponent } from './device-config-modal/device-config-modal.component';
import { ErseDataComponent } from './erse-data/erse-data.component';
import { DatasourceIdModalComponent } from 'src/app/components/datasource-id/datasource-id-modal/datasource-id-modal.component';
import { DeviceMaintenanceComponent } from './device-maintenance/device-maintenance.component';
import { DevicePropertiesComponent } from './device-properties/device-properties.component';
import { DeviceIgnoreDataComponent } from './device-ignore-data/device-ignore-data.component';
import { DeviceIgnoreEventsComponent } from './device-ignore-events/device-ignore-events.component';
import { DevicePropertiesModalComponent } from './device-properties/device-properties-modal/device-properties-modal.component';
import { DeviceMaintenanceModalComponent } from './device-maintenance/device-maintenance-modal/device-maintenance-modal.component';
/* import { GridAddButtonComponent } from 'src/app/components/grid-add-button/grid-add-button.component'; */
import { DeviceIgnoreDataModalComponent } from './device-ignore-data/device-ignore-data-modal/device-ignore-data-modal.component';
import { DeviceIgnoreEventsModalComponent } from './device-ignore-events/device-ignore-events-modal/device-ignore-events-modal.component';
import { AppComponentsModule } from 'src/app/components/app-components.module';


@NgModule({
    imports: [
        CommonModule,
        TiGridModule,
        AppMaterialModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        AppComponentsModule
    ],
    declarations: [
        DeviceComponent,
        DeviceAdditionalDataComponent,
        DescriptionListFilterPipe,
        DeviceConfigComponent,
        ErseDataComponent,
        DeviceMaintenanceComponent,
        DevicePropertiesComponent,
        DeviceIgnoreDataComponent,
        DeviceIgnoreEventsComponent,
        DevicePropertiesModalComponent,
    /*     GridAddButtonComponent, */
    ],
    entryComponents: [
        DeviceComponent,
        DeviceConfigModalComponent,
        DatasourceIdModalComponent,
        DevicePropertiesModalComponent,
        DeviceMaintenanceModalComponent,
        DeviceIgnoreDataModalComponent,
        DeviceIgnoreEventsModalComponent,
    ]
})
export class DeviceModule { }
