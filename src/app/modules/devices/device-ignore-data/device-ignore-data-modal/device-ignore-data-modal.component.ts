import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DeviceIgnoreData } from 'src/app/models/device-ignore-data';
import { VariableService } from 'src/app/services/variable.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { compareOption } from 'src/app/code/utils';
import { Variable } from 'src/app/models/variable';
import { Subscription } from 'rxjs';
import { IgnoreDataCause } from 'src/app/models/ignore-data-cause';
import { IgnoreDataCauseService } from 'src/app/services/ignore-data-cause.service';

@Component({
    selector: 'app-device-ignore-data-modal',
    templateUrl: './device-ignore-data-modal.component.html',
    styleUrls: ['./device-ignore-data-modal.component.scss']
})
export class DeviceIgnoreDataModalComponent implements OnInit, OnDestroy {

    private compareOption = compareOption;
    private variables: Variable[];
    private ignoreDataCauses: IgnoreDataCause[];
    private variablesSubscription: Subscription;
    private ignoreDataCausesSubscription: Subscription;
    private fgIgnoreData: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<DeviceIgnoreDataModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DeviceIgnoreData,
        variableService: VariableService,
        ignoreDataCauseService: IgnoreDataCauseService,
        private formBuilder: FormBuilder
    ) {
        this.variablesSubscription = variableService.getVariables().subscribe((variables: Variable[]) => this.variables = variables);

        this.ignoreDataCausesSubscription = ignoreDataCauseService.getIgnoreDataCauses().subscribe((dataCauses: IgnoreDataCause[]) => this.ignoreDataCauses = dataCauses);
    }

    ngOnInit() {
        this.fgIgnoreData = this.formBuilder.group({
            'variable': [this.data.variable, Validators.compose([Validators.required])],
            'startDate': [this.data.startDate, Validators.compose([Validators.required])],
            'endDate': [this.data.endDate, Validators.compose([Validators.required])],
            'ignoreDataCause': [this.data.ignoreDataCause, Validators.compose([Validators.required])],
            'obs': [this.data.obs]
        });
    }
    ngOnDestroy(): void {
        this.variablesSubscription.unsubscribe();
        this.ignoreDataCausesSubscription.unsubscribe();
    }

    cancel() {
        this.dialogRef.close();
    }

    acceptChanges() {
        if (!this.fgIgnoreData.valid) { return; }

        Object.assign(this.data, this.fgIgnoreData.value);
        this.dialogRef.close(this.data);
    }

    get inputControls() {
        return this.fgIgnoreData.controls;
    }

    startDateChanged(startDate) {
        this.fgIgnoreData.controls['startDate'].setValue(startDate);
    }

    endDateChanged(endDate) {
        this.fgIgnoreData.controls['endDate'].setValue(endDate);
    }
}
