import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceIgnoreDataModalComponent } from './device-ignore-data-modal.component';

describe('DeviceIgnoreDataModalComponent', () => {
  let component: DeviceIgnoreDataModalComponent;
  let fixture: ComponentFixture<DeviceIgnoreDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceIgnoreDataModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceIgnoreDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
