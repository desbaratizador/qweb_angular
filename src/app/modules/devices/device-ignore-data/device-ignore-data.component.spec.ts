import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceIgnoreDataComponent } from './device-ignore-data.component';

describe('DeviceIgnoreDataComponent', () => {
  let component: DeviceIgnoreDataComponent;
  let fixture: ComponentFixture<DeviceIgnoreDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceIgnoreDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceIgnoreDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
