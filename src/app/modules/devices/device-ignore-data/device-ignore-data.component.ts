import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppTranslationService } from 'src/app/services/app-translation.service';
import { DeviceIgnoreDataModalComponent } from './device-ignore-data-modal/device-ignore-data-modal.component';

@Component({
    selector: 'app-device-ignore-data',
    templateUrl: './device-ignore-data.component.html',
    styleUrls: ['./device-ignore-data.component.scss']
})
export class DeviceIgnoreDataComponent implements OnInit {
    @Input() gridDatasource: any[];
    @Input() deviceId: number;
    columns: any[] = [
        { title: 'Id', field: 'id' },
        { field: 'startDate', align: 'center' },
        { field: 'endDate', align: 'center' },
        { field: 'variable', align: 'center' },
        { field: 'ignoreDataCause', align: 'center' },
        { field: 'obs', align: 'center' },
    ];


    rowActions = [{
        title: '',
        classes: '',
        icon: 'fal fa-edit',
        onClick: (row) => {
            this.openRecordModal(row);
        }
    }, {
        title: '',
        classes: 'text-danger',
        icon: 'fal fa-trash',
        onClick: (row) => {
            row.isDeleted = true;
        }

    }
    ];
    constructor(
        private appTranslate: AppTranslationService,
        private dialog: MatDialog
    ) {
        appTranslate.translateMultiple([
            'startDate',
            'endDate',
            'variable',
            'ignoreDataCause',
            'observations',
            'yes',
            'no',
            'deleteRecord',
            'edit'
        ]).subscribe((t: any) => {
            this.setGridColumnsOptions(t);
            this.setGridActionsOptions(t);
        });

    }

    setGridActionsOptions(t: any) {
        this.rowActions[0].title = t.edit;
        this.rowActions[1].title = t.deleteRecord;
    }

    setGridColumnsOptions(t: any) {
        this.columns[1].title = t.startDate;
        this.columns[1].template = (v) => new Date(v).toLocaleDateString('pt-PT') + ' ' + new Date(v).toLocaleTimeString('pt-PT');
        this.columns[2].title = t.endDate;
        this.columns[2].template = (v) => v ? new Date(v).toLocaleDateString('pt-PT') : null;
        this.columns[3].title = t.variable;
        this.columns[3].template = (v) => v.name;
        this.columns[4].title = t.ignoreDataCause;
        this.columns[4].template = (v) => v.description;
        this.columns[5].title = t.observations;
    }
    ngOnInit() {
    }

    private gridAddRecord(evt) {
        this.openRecordModal(
            {
                id: 0,
                device: { id: this.deviceId },
                variable: null,
                startDate: null,
                endDate: null,
                user: { id: 24 },
                ignoreDataCause: null,
                obs: null
            });
    }

    openRecordModal(data) {
        const dialogRef = this.dialog.open(DeviceIgnoreDataModalComponent, {
            width: '60vw',
            height: '50vh',
            data: data
        });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result && result.id === 0) {
                this.gridDatasource.push(result);
            }
        });
    }
}
