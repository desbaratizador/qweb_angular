import { Component, OnInit } from '@angular/core';
import { AppTranslationService } from 'src/app/services/app-translation.service';

@Component({
  selector: 'app-users-menu',
  templateUrl: './users-menu.component.html',
  styleUrls: ['./users-menu.component.scss']
})
export class UsersMenuComponent implements OnInit {

  constructor(translate: AppTranslationService) { }

  ngOnInit() {
  }

}
