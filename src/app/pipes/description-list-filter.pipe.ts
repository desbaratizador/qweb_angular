import { Pipe, PipeTransform } from '@angular/core';
import { DescriptionList } from '../models/description-list';

@Pipe({
  name: 'descriptionListFilter'
})
export class DescriptionListFilterPipe implements PipeTransform {

  transform(descriptionList: DescriptionList[], context: string): any {
    if (!descriptionList) { return []; }

    if (!context) { return descriptionList; }

    return descriptionList.filter((dl: DescriptionList) => dl.context === context);
  }


}
