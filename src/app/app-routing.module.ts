import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import appConstants from './code/app-constants';
import { CalcOptionsListComponent } from './modules/administration/calc-options/list/calc-options-list.component';
import { CalcOptionsEditComponent } from './modules/administration/calc-options/edit/calc-options-edit.component';
import { CalcOptionsForUserListComponent } from './modules/administration/calc-options-for-user/list/calc-options-for-user-list.component';
import { CalcOptionsForUserEditComponent } from './modules/administration/calc-options-for-user/edit/calc-options-for-user-edit.component';
import { DataSourceListComponent } from './modules/administration/data-sources/list/data-source-list.component';
import { DataSourceEditComponent } from './modules/administration/data-sources/edit/data-source-edit.component';
import { LimitsListComponent } from './modules/administration/limits/list/limits-list.component';
import { LimitsEditComponent } from './modules/administration/limits/edit/limits-edit.component';
import { ProfileListComponent } from './modules/administration/profiles/list/profile-list.component';
import { ProfileEditComponent } from './modules/administration/profiles/edit/profile-edit.component';
import { UserListComponent } from './modules/administration/users/list/user-list.component';
import { UserEditComponent } from './modules/administration/users/edit/user-edit.component';

import { DeviceComponent } from './modules/devices/device/device.component';
import { LoginComponent } from './modules/login/login.component';
import { HomeComponent } from './modules/home/home.component';
import { AdministrationComponent } from './modules/administration/administration/administration.component';
import { ManagementComponent } from './modules/management/management/management.component';
import { UsersMenuComponent } from './modules/users/users-menu/users-menu.component';
import { ReportComponent } from './modules/reports/report/report.component';


const routes: Routes = [


  { path: appConstants.ROUTES.ROUTE.LOGIN, component: LoginComponent },
  {
    path: '', component: HomeComponent, children: [
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION, component: AdministrationComponent },
      { path: appConstants.ROUTES.ROUTE.MANAGEMENT, component: ManagementComponent },
      { path: appConstants.ROUTES.ROUTE.USERS, component: UsersMenuComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_CALC_OPTIONS, component: CalcOptionsListComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_CALC_OPTIONS_SHEET, component: CalcOptionsEditComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_DATASOURCES, component: DataSourceListComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_DATASOURCES_SHEET, component: DataSourceEditComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_REPORT_OPTIONS, component: CalcOptionsForUserListComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_REPORT_OPTIONS_SHEET, component: CalcOptionsForUserEditComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_LIMITS, component: LimitsListComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_LIMITS_SHEET, component: LimitsEditComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_PROFILES, component: ProfileListComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_PROFILE_SHEET, component: ProfileEditComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_USERS, component: UserListComponent },
      { path: appConstants.ROUTES.ROUTE.ADMINISTRATION_USER_SHEET, component: UserEditComponent },
      { path: appConstants.ROUTES.ROUTE.DEVICES, component: DeviceComponent },
      { path: appConstants.ROUTES.ROUTE.DEVICE_SHEET, component: DeviceComponent },
      { path:  appConstants.ROUTES.ROUTE.REPORTS, component: ReportComponent }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
